﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataExchange
{
    [Serializable]
    public class DataExchangeConfig
    {
        string posOffLineDataIndex = string.Empty;
        public string PosOffLineDataIndex
        {
            get { return posOffLineDataIndex; }
            set { posOffLineDataIndex = value; }
        }

        int timeDataExchange = 30;
        public int DataExchangeTime
        {
            get
            {
                if (timeDataExchange < 10)
                    timeDataExchange = 30;

                return timeDataExchange;
            }
            set { timeDataExchange = value; }
        }

        bool backupRemoteDataExchange = false;
        public bool DataExchangeBackupRemote
        {
            get { return backupRemoteDataExchange; }
            set { backupRemoteDataExchange = value; }
        }

        int timeoutDataExchange = 30;
        /// <summary>
        /// 超时(毫秒)
        /// </summary>
        public int DataExchangeTimeOut
        {
            get
            {
                if (timeoutDataExchange <= 0)//小于等于0都会验证为无法连接数据库
                    timeoutDataExchange = 500;

                return timeoutDataExchange;
            }
            set { timeoutDataExchange = value; }
        }

        string naDataExchange = FCNS.Data.DbDefine.dbFile;
        public string DataExchangeDataName
        {
            get { return naDataExchange; }
            set { naDataExchange = value.Trim(); }
        }

        string miDataExchange = string.Empty;
        public string DataExchangeDataAddress
        {
            get { return miDataExchange; }
            set { miDataExchange = value.Trim(); }
        }

        string muDataExchange = string.Empty;
        public string DataExchangeDataUser
        {
            get { return muDataExchange; }
            set { muDataExchange = value.Trim(); }
        }

        string mpDataExchange = string.Empty;
        public string DataExchangeDataPassword
        {
            get { return mpDataExchange; }
            set { mpDataExchange = value; }
        }

        int portDataExchange = 1433;
        public int DataExchangePort { get { return portDataExchange; } set { portDataExchange = value; } }
    }
}