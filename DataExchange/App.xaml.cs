﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace DataExchange
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        /*
         * 先上传数据（由于离线数据需要总部的后台运算：例如-》库存）
         * 再下载数据
         * 
         * 上传数据先确保除 PosOrder 外的数据都上传成功了
         * 然后上传 PosOrder 并运算
         */
    }
}
