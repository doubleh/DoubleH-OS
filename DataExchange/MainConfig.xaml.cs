﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DataExchange
{
    /// <summary>
    /// MainConfig.xaml 的交互逻辑
    /// </summary>
    public partial class MainConfig : Window
    {
        public MainConfig()
        {
            InitializeComponent();

            InitEvent();
        }

        DataExchangeConfig config = null;
        public void Init(DataExchangeConfig conf )
        {
            config = conf ;
            numericUpDownDataTime.Value = config.DataExchangeTime;
            checkBoxBackRemote.IsChecked = config.DataExchangeBackupRemote;
            textBoxAddress.Text = config.DataExchangeDataAddress;
            textBoxData.Text = config.DataExchangeDataName;
            textBoxName.Text = config.DataExchangeDataUser;
            textBoxPwd.Password = config.DataExchangeDataPassword;
            numericUpDownPort.Value = config.DataExchangePort;
            numericUpDownTimeOut.Value = config.DataExchangeTimeOut;
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => SaveConfig();
        }

        private void SaveConfig()
        {
            config.DataExchangeTime = (int)numericUpDownDataTime.Value;
            config.DataExchangeBackupRemote = checkBoxBackRemote.IsChecked.Value;
            config.DataExchangeDataAddress = textBoxAddress.Text.Trim();
            config.DataExchangeDataName = textBoxData.Text.Trim();
            config.DataExchangeDataUser = textBoxName.Text.Trim();
            config.DataExchangeDataPassword = textBoxPwd.Password;
            config.DataExchangePort = (int)numericUpDownPort.Value;
            config.DataExchangeTimeOut = (int)numericUpDownTimeOut.Value;

            DoubleH.Utility.MessageWindow.Show("重启后生效");
            this.Close();
        }
    }
}