﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoubleH.Utility.Configuration
{
    public sealed class AppDefine
    {
        public static readonly string baseDir = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string extensionDir = baseDir + "Extension\\";
        public static readonly string printDir = baseDir + "Print\\";
        public static readonly string updateDir = baseDir + "Update\\";
        public static readonly string dataDir = baseDir + "Data\\";
        public static readonly string helpDir = baseDir + "Help\\";

        //public static readonly string updateDir = AppDomain.CurrentDomain.BaseDirectory + "Update\\";

        /// <summary>
        /// 插件验证名称
        /// </summary>
        public const string pluginProductName = "PluginForDoubleH";
        public const string extensionProductName = "ExtensionForDoubleH";
        public const string posExtensionProductName = "ExtensionForDoubleHpos";
        public const string updateUrl = "http://www.fcnsoft.com/download/doubleh.xml";
        public static readonly string readMe = FCNS.Data.DbDefine.appConfigDir + "readme.txt";
        public static readonly string appConfigFile = FCNS.Data.DbDefine.appConfigDir + "doubleH.xml";

    }
}
