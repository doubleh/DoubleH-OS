﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using DoubleH.Utility.Configuration;

namespace DoubleH.Utility
{
    #region MenuItemBinding
    public class MenuItemBinding
    {
        bool mustRefreshSubItem = false;
        /// <summary>
        /// 不知道有什么用吗？看看‘公寓租赁’中的公寓栏目就知道用途了
        /// </summary>
        public bool MustRefreshSubItem
        {
            get { return mustRefreshSubItem; }
            set { mustRefreshSubItem = value; }
        }

        public List<System.Windows.Controls.MenuItem> ContextMenuItems { get; set; }

        UC.PagePanel.PageListView.EnumModeTemplete mode1 = UC.PagePanel.PageListView.EnumModeTemplete.Null;
        /// <summary>
        /// 缩略图模式下使用的模板名称，如果为空表示不支持缩略图模式
        /// </summary>
        public UC.PagePanel.PageListView.EnumModeTemplete DataTempleteName
        {
            get { return mode1; }
            set { mode1 = value; }
        }

        string tableName = string.Empty;
        /// <summary>
        /// 当前菜单调去的数据表名称,优先使用,如果为空,则使用 DllFile 名称
        /// </summary>
        public string TableName
        {
            get
            {
                if (string.IsNullOrEmpty(tableName))
                    tableName = DllFile.Split('.')[0];

                return tableName;
            }
            set { tableName = value; }
        }

        public override string ToString()
        {
            return Name + " - " + TableText.ToString();
        }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// dll 文件名称
        /// </summary>
        public string DllFile { get; set; }

        bool print = false;
        public bool CanPrint { get { return print; } set { print = value; } }

        bool add = true;
        public bool CanNew { get { return add; } set { add = value; } }

        public Table.UserS.EnumAuthority Authority
        {
            get { return Table.UserS.LoginUser.GetAuthority(TableText); }
        }

        DataTableText tt = DataTableText.Null;
        /// <summary>
        /// 数据表标识
        /// </summary>
        public DataTableText TableText
        {
            get { return tt; }
            set
            {
                tt = value;
                Name = TableText.ToString();

                UserUIparams binding = DoubleHConfig.UserConfig.DataGridBinding.Find(f => { return f.TableText == TableText; });
                if (binding == null)
                    DoubleHConfig.UserConfig.DataGridBinding.Add(binding = new UserUIparams() { TableText = this.TableText });//必须这样,因为初始化的用户配置文件是空的导致返回值为null,会抛出异常

                column = binding;
            }
        }

        FCNS.Data.Table.DataTableS dataTable = null;
        public FCNS.Data.Table.DataTableS DataTable
        {
            get { return dataTable; }
            set { dataTable = value; }
        }

        object tag = null;
        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        List<MenuItemBinding> tb = new List<MenuItemBinding>();
        public List<MenuItemBinding> SubItems
        {
            get { return tb; }
            set { tb = value; }
        }

        UserUIparams column = null;
        public UserUIparams UserParams { get { return column; } }
    }
    #endregion

    #region DataGridColumnHeaderBinding
    public class DataGridColumnHeaderBinding
    {
        string columnType = string.Empty;
        public string ColumnType
        {
            get { return columnType; }
            set { columnType = value; }
        }

        //bool canFilter = false;
        ///// <summary>
        ///// 
        ///// </summary>
        //public bool CanFilter { get { return canFilter; } set { canFilter = value; } }
        bool canSearch = false;
        /// <summary>
        /// 是否添加到默认搜索中
        /// </summary>
        public bool CanSearch
        {
            get { return canSearch; }
            set { canSearch = value; }
        }
        bool isReadOnly = true;
        /// <summary>
        /// 是否只读
        /// </summary>
        public bool IsReadOnly { get { return isReadOnly; } set { isReadOnly = value; } }
        /// <summary>
        /// 获取或设置绑定到的数据库列名
        /// </summary>
        public string BindingName { get; set; }
        public string ContentBindingName { get; set; }
        /// <summary>
        /// 获取或设置要显示的标题文本
        /// </summary>
        public string Header { get; set; }
        /// <summary>
        /// 获取或设置Column的宽度-列表模式
        /// </summary>
        public double Width { get; set; }
        ///// <summary>
        ///// 缩略图模式的宽度与高度（正方形）
        ///// </summary>
        //public double WidthAndHeight { get { return Width; } }
        /// <summary>
        /// 获取或设置ListView中列的排序
        /// </summary>
        public int Index { get; set; }

        string styleKey = "居中";
        /// <summary>
        /// 样式名称,默认左对齐
        /// </summary>
        public string StyleKey { get { return styleKey; } set { styleKey = value; } }

        bool visible = true;
        /// <summary>
        /// 获取或设置此列是否可见
        /// </summary>
        public bool Visible { get { return visible; } set { visible = value; } }
    }
    #endregion

    #region UserUIparams
    /// <summary>
    /// 用户的自定义参数，保存到用户配置文件中
    /// </summary>
    public class UserUIparams
    {
        public DataTableText TableText { get; set; }

        public Utility.UC.UCPagePanel.EnumMode Mode
        {
            get { return (Utility.UC.UCPagePanel.EnumMode)ViewMode; }
            set { ViewMode = (int)value; }
        }

        int viewMode = 0;
        public int ViewMode
        {
            get { return viewMode; }
            set { viewMode = value; }
        }
        //图表
        double chartSplitLineHeight = 300;
        /// <summary>
        /// 报表中 图表和明细的分隔条位置
        /// </summary>
        public double ChartSplitLineHeight
        {
            get { return chartSplitLineHeight; }
            set { chartSplitLineHeight = value; }
        }

        string chartColumnSeriesName = "ColumnSeries";
        /// <summary>
        /// 图表类型名
        /// </summary>
        public string ChartColumnSeriesName
        {
            get { return chartColumnSeriesName; }
            set { chartColumnSeriesName = value; }
        }

        //列表视图
        List<DataGridColumnHeaderBinding> items = null;
        public List<DataGridColumnHeaderBinding> Items
        {
            get
            {
                if (items == null)
                    items = GetListViewBinding();

                return items;
            }
            set { items = value; }
        }

        string sortName = string.Empty;
        /// <summary>
        /// 排序的列名
        /// </summary>
        public string SortName { get { return sortName; } set { sortName = value; } }

        System.ComponentModel.ListSortDirection lsd = System.ComponentModel.ListSortDirection.Ascending;
        /// <summary>
        /// 排序的方向
        /// </summary>
        public System.ComponentModel.ListSortDirection SortDirection { get { return lsd; } set { lsd = value; } }

        private List<DataGridColumnHeaderBinding> GetListViewBinding()
        {
            List<DataGridColumnHeaderBinding> listViewColumn = null;
            switch (TableText)
            {
                #region 促销管理
                case DataTableText.优惠券:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=1,CanSearch=true},
              new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="号码",Width=100,Index=2},
         new DataGridColumnHeaderBinding(){BindingName="StartDateTime",Header="生效时间",Width=120,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="EndDateTime",Header="失效时间",Width=120,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额",Width=80,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=6} ,
         new DataGridColumnHeaderBinding(){BindingName="_ProductSNameList",Header="可兑换商品列表",Width=120,Index=11} ,
                          new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width=120,Index=12} };
                    break;

                case DataTableText.促销方案:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="StartDateTime",Header="生效时间",Width=120,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="EndDateTime",Header="失效时间",Width=120,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="MoneyMin",Header="金额下限",Width=80,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="MoneyMax",Header="金额上限",Width=80,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="_VipList",Header="享受优惠的会员等级",Width=80,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="Flag",Header="优惠方式",Width=80,Index=8}, 
         new DataGridColumnHeaderBinding(){BindingName="FlagValue",Header="优惠方式相关值",Width=80,Index=9},
         new DataGridColumnHeaderBinding(){BindingName="_ProductSNameList",Header="促销商品列表",Width=120,Index=10},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=11} ,
                          new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width=120,Index=12} };
                    break;
                #endregion

                #region 消息中心
                case DataTableText.我的消息:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期", Width=200,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="内容", Width=300,Index=1}};

                    break;

                case DataTableText.待办事项:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="登记日期", Width=160,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="PlannedTasks",Header="计划任务", Width=300,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="ExecuteDateTime",Header="预计时间", Width=160,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态", Width=50,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="Flag",Header="紧急程度", Width=50,Index=4},
            new DataGridColumnHeaderBinding() { BindingName = "_OverdueDateTime", Header = "超期", Width = 60, Index = 5 }};
                    break;
                #endregion

                #region 公寓
                case DataTableText.公寓:
                case DataTableText.楼层:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="房间名称", Width=200,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态", Width=200,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_RoomTypeName",Header="类型", Width=200,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_Configure",Header="配置", Width=200,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="Direction",Header="朝向", Width=200,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="RoomArea",Header="面积", Width=200,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="_FloorName",Header="楼层", Width=200,Index=6},
            //new DataGridColumnHeaderBinding(){BindingName="_HouseSName",Header="楼名", Width=200,Index=7}, 
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=8}};
                    break;
                #endregion

                #region ITdb
                case DataTableText.硬件:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
              new DataGridColumnHeaderBinding(){BindingName="Code",Header="编号",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=1,CanSearch=true},
              new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=2},
         //new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=2},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=80,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="IpV4",Header="IPv4",Width=80,Index=5,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="IpV6",Header="IPv6",Width=80,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="Mac",Header="MAC",Width=100,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="PurchaseDate",Header="购买日期",Width=80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="PurchasePrice",Header="购买价格",Width=80,Index=9},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=11} ,
         new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width=60,Index=11} ,
         new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="使用者",Width=60,Index=11} ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=12,ColumnType="Image"}   };
                    break;

                case DataTableText.软件:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Version",Header="版本",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="数量",Width=100,Index=2},
         new DataGridColumnHeaderBinding(){BindingName="Gpl",Header="许可信息",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="PurchaseDate",Header="购买日期",Width=80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="PurchasePrice",Header="购买价格",Width=80,Index=9},
         new DataGridColumnHeaderBinding(){BindingName="License",Header="授权",Width=80,Index=10},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=11} ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=12,ColumnType="Image"}   };
                    break;
                #endregion

                #region POS
                case DataTableText.连锁门店:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="门店名称", Width=200,Index=0,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="ShopFlag",Header="标识",Width= 200,Index=1,CanSearch=true},
                     //new DataGridColumnHeaderBinding(){BindingName="Tel",Header="电话",Width= 200,Index=2},
                     new DataGridColumnHeaderBinding(){BindingName="Address",Header="地址",Width= 200,Index=3},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=4},
                     new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width= 200,Index=4}};
                    break;

                case DataTableText.POS请货单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
                     new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width= 200,Index=1 },
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号", Width=200,Index=2,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="_ShopName",Header="门店名称", Width=200,Index=3,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="ShopFlag",Header="门店标识",Width= 200,Index=4,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width= 200,Index=5},
                     new DataGridColumnHeaderBinding(){BindingName="_ShopAddress",Header="地址",Width= 200,Index=6},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=7},
                     new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width= 200,Index=8} };
                    break;

                case DataTableText.POS配送单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
                     new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width= 200,Index=8},
                    new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width= 200,Index=1 },
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="配送单号", Width=200,Index=2,CanSearch=true}, 
              new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="请货单号",Width= 200,Index=9},
            new DataGridColumnHeaderBinding(){BindingName="_ShopName",Header="门店名称", Width=200,Index=3,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="ShopFlag",Header="门店标识",Width= 200,Index=4,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="_ShopAddress",Header="地址",Width= 200,Index=6},
                     new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width= 200,Index=5},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=7},
                   };
                    break;

                case DataTableText.交班明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="StartDateTime",Header="上班时间", Width=160,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="EndDateTime",Header="下班时间", Width=100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="StartMoney",Header="初始金额", Width=50,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="EndMoney",Header="取款金额", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="PosNO",Header="机器号", Width=60,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="收银员", Width=60,Index=6}};
                    break;
                case DataTableText.零售统计表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_Name",Header="名称", Width=100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_Money",Header="金额", Width=100,Index=2}};
                    break;
                case DataTableText.零售明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期", Width=160,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="PosNO",Header="机器号", Width=60,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="收银员", Width=60,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号", Width=160,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额", Width=50,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="Flag",Header="单据类型", Width=80,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态", Width=60,Index=7}};
                    break;
                case DataTableText.POS记账明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期", Width=160,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="PosNO",Header="机器号", Width=60,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="收银员", Width=60,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号", Width=160,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额", Width=50,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户", Width=50,Index=8},
            new DataGridColumnHeaderBinding(){BindingName="Flag",Header="单据类型", Width=80,Index=7},
            new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库", Width=100,Index=8} };
                    break;



                case DataTableText.POS机器号:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="Flag",Header="设备类型",Width=80,Index=1,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="PosNO",Header="设备号",Width=80,Index=2,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="_ShopFlag",Header="连锁门店标识",Width=80,Index=3,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=120,Index=4},
                      new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="默认仓库",Width=80,Index=5},
                      new DataGridColumnHeaderBinding(){BindingName="_UserListString",Header="收银员列表",Width=80,Index=6},
                      new DataGridColumnHeaderBinding(){BindingName="LastLoginDateTime",Header="最后登录时间",Width=100,Index=7},
                      new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width=60,Index=8}};

                    break;

                case DataTableText.POS禁售商品:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="PosCanNotUse",Header="禁售设备号",Width=80,Index=7,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=8}   }; break;

                case DataTableText.实时库存:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
         new DataGridColumnHeaderBinding(){BindingName="_QuantityAll",Header="库存数量(包含客存)",Width=80,Index=7,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_QuantityCorSSave",Header="客存数量",Width=80,Index=7,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=8}   }; break;

                case DataTableText.客存明细:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
         new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=80,Index=0},
         new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="票据号码",Width=80,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="数量",Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=3,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=4,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=5,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=7},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=9},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=10}   }; break;

                case DataTableText.商品赠送明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_ProductSName",Header="商品名称", Width=200,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号", Width=150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="数量",Width=50,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="AveragePrice",Header="库存单价",Width= 80,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_ZengSongUserSName",Header="经办人",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=100,Index=6} };
                    break;
                #endregion

                #region 项目
                case DataTableText.项目列表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
                     new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width= 80,Index=1},
                         new DataGridColumnHeaderBinding(){BindingName="_Progress",Header="进度", ColumnType="ProgressBar", Width=100,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期", Width=100,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="项目编号", Width=100,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="项目名称", Width=200,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="Money",Header="项目总价", Width=80,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户", Width=200,Index=7},
            new DataGridColumnHeaderBinding(){BindingName="StartDateTime",Header="施工开始时间", Width=100,Index=8},
            new DataGridColumnHeaderBinding(){BindingName="EndDateTime",Header="施工结束时间", Width=100,Index=9},
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="操作员", Width=100,Index=10},
                     new DataGridColumnHeaderBinding(){ContentBindingName="Url",BindingName="Url",Header="相关链接", Width=100,Index=11,ColumnType="Hyperlink"}};
                    break;

                case DataTableText.任务跟踪:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
                    new DataGridColumnHeaderBinding(){BindingName="Name",Header="任务名称", Width=200,Index=0},
                    new DataGridColumnHeaderBinding(){BindingName="_ProjectSName",Header="项目", Width=200,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="_ScheduleName",Header="类型", Width=100,Index=2},
                    new DataGridColumnHeaderBinding(){BindingName="_ServiceUserSName",Header="经办人", Width=200,Index=3},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注", Width=200,Index=4},
                    new DataGridColumnHeaderBinding(){BindingName="_PlanDateTime",Header="计划花费时间", Width=100,Index=5},
                    new DataGridColumnHeaderBinding(){BindingName="_CostDateTime",Header="已消耗时间", Width=100,Index=6},
                             new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width= 80,Index=7},
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="操作员", Width=100,Index=10}};
                    break;

                case DataTableText.问题反馈:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
                    new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期", Width=200,Index=0},
                    new DataGridColumnHeaderBinding(){BindingName="Name",Header="问题", Width=200,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="_ProjectSName",Header="项目", Width=200,Index=2},
                    new DataGridColumnHeaderBinding(){BindingName="_CorSUserSName",Header="联系人", Width=100,Index=3},
                    new DataGridColumnHeaderBinding(){BindingName="Level",Header="优先级", Width=200,Index=4},
                    new DataGridColumnHeaderBinding(){BindingName="_ServiceUserSName",Header="经办人", Width=200,Index=5},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注", Width=200,Index=6},
                             new DataGridColumnHeaderBinding(){BindingName="Enable",Header="状态",Width= 80,Index=7},
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="操作员", Width=100,Index=10}};
                    break;
                #endregion

                #region 物流
                case DataTableText.仓库物流跟踪:
                case DataTableText.采购物流跟踪:
                case DataTableText.批发物流跟踪:
                case DataTableText.物流明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期", Width=200,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="_Company",Header="物流公司", Width=200,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号", Width=100,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_Money",Header="运费", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="关联单号", Width=100,Index=4},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=5},
                     //new DataGridColumnHeaderBinding(){BindingName="_Http",ContentBindingName="_ContentBindingName",Header="状态",Width= 200,Index=6,ColumnType="Hyperlink"}};
                     new DataGridColumnHeaderBinding(){BindingName="_ContentBindingName",Header="状态",Width= 200,Index=6}};
                    break;

                case DataTableText.物流公司:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称", Width=200,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Tel",Header="电话", Width=200,Index=1},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=2}};
                    break;
                #endregion

                #region 财务
                case DataTableText.银行存取款:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=4}  };
                    break;

                case DataTableText.其它费用收入单:
                case DataTableText.其它费用支出单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="RealMoney",Header="金额",Width=100,Index=3},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=4}  };
                    break;

                case DataTableText.收款单:
                case DataTableText.付款单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额",Width=100,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="结算日期",Width=100,Index=2},
                    new DataGridColumnHeaderBinding(){BindingName="RealMoney",Header="实付金额",Width=100,Index=3},
                    new DataGridColumnHeaderBinding(){BindingName="_UseInvoice",Header="开具发票",Width=100,Index=4},
                    new DataGridColumnHeaderBinding(){BindingName="Invoice",Header="发票号码",Width=100,Index=5,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="RelatedOrderDateTime",Header="关联单号开单日期",Width=100,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="关联单号",Width= 100,Index=7,CanSearch=true},
                      new DataGridColumnHeaderBinding(){BindingName=" _CorSName",Header="付款对象",Width=100,Index=8,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="_CandSProperty",Header="客户性质",Width= 100,Index=13} ,
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=14}  };
                    break;

                case DataTableText.未开具发票:
                case DataTableText.未收取发票:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额",Width=100,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="结算日期",Width=100,Index=2},
                    new DataGridColumnHeaderBinding(){BindingName="RealMoney",Header="实付金额",Width=100,Index=3},
                    //new DataGridColumnHeaderBinding(){BindingName="UseInvoice",Header="开具发票",Width=100,Index=4},
                    //new DataGridColumnHeaderBinding(){BindingName="Invoice",Header="发票号码",Width=100,Index=5},
            //        new DataGridColumnHeaderBinding(){BindingName="RelatedOrderDateTime",Header="开单日期",Width=100,Index=6},
            //new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="关联单号",Width= 100,Index=7},
                      new DataGridColumnHeaderBinding(){BindingName=" _CorSName",Header="付款对象",Width=100,Index=8,CanSearch=true},
                   // new DataGridColumnHeaderBinding(){BindingName="_CorSTel",Header="联系电话",Width= 100,Index=9},
                   //new DataGridColumnHeaderBinding(){BindingName="_CorSContacts",Header="联系人",Width= 100,Index=10},
                   // new DataGridColumnHeaderBinding(){BindingName="_CorSPhone",Header="手机",Width= 100,Index=11},
                   //  new DataGridColumnHeaderBinding(){BindingName="_CorSAddress",Header="地址",Width= 100,Index=12},
                    new DataGridColumnHeaderBinding(){BindingName="_CandSProperty",Header="客户性质",Width= 100,Index=13} ,
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=14}  };
                    break;

                //    case DataTableText.可收支单据:
                //        listViewColumn = new List<DataGridColumnHeaderBinding>(){
                //new DataGridColumnHeaderBinding(){BindingName="_Pay",Header="",Width= 20,Index=0, ColumnType="CheckBox",IsReadOnly=false},
                //new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=1},
                //        new DataGridColumnHeaderBinding(){BindingName="RelatedOrderDateTime",Header="关联单号开单日期",Width=150,Index=2},
                //        new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="关联单号",Width=100,Index=3},
                //        new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额",Width=50,Index=4},
                //        new DataGridColumnHeaderBinding(){BindingName="RealMoney",Header="实付金额",Width=50,Index=5},
                //        new DataGridColumnHeaderBinding(){BindingName="_UseInvoice",Header="开具发票",Width=60,Index=6, ColumnType=""},
                //        new DataGridColumnHeaderBinding(){BindingName="_InvoiceType",Header="发票类型",Width=100,Index=7},
                //new DataGridColumnHeaderBinding(){BindingName="Invoice",Header="发票号码",Width= 100,Index=8},
                //        new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=9}  };
                //        break;

                case DataTableText.支付方式:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称", Width=200,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="StringValue",Header="账号", Width=200,Index=1,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="DoubleValue",Header="余额", Width=100,Index=2},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=3}};
                    break;

                case DataTableText.会计科目:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="StringValue",Header="科目代码", Width=100,Index=1,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="科目名称", Width=100,Index=0,CanSearch=true}};
                    break;


                case DataTableText.客商余额表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="客商名称", Width=150,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="MoneyZ",Header="己方欠款", Width=80,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="MoneyF",Header="客商欠款", Width=80,Index=1}};
                    break;
                case DataTableText.账户余额表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="账户", Width=150,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="DoubleValue",Header="金额", Width=80,Index=1}};
                    break;
                case DataTableText.收支明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="收支对象", Width=80,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="记账日", Width=150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="Money",Header="应收支", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="MoneyOut",Header="实际支出", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="MoneyIn",Header="实际收入", Width=80,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="_BankName",Header="资金账户", Width=80,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="Source",Header="资金动向", Width=80,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=7}};
                    break;
                case DataTableText.发票明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="InvoiceDateTime",Header="开票日期", Width=50,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="InvoiceType",Header="发票类型", Width=50,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="InvoiceNO",Header="发票号码", Width=50,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额", Width=50,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="Tax",Header="税额", Width=50,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="MoneyTax",Header="金额(含税)", Width=50,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="CorSName",Header="客商", Width=150,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="Own",Header="我方开具", Width=50,Index=7}};
                    break;
                case DataTableText.应收支明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="收支对象", Width=80,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="记账日", Width=150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="MoneyOut",Header="应支出", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="MoneyIn",Header="应收入", Width=80,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="Source",Header="资金动向", Width=80,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=7}};
                    break;
                case DataTableText.损益表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_KeMu",Header="科目代码", Width=80,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="_Name",Header="科目名称", Width=150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_Money",Header="本期数", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="_MoneyAll",Header="本年累计数", Width=80,Index=4}};
                    break;
                #endregion

                #region 车辆
                case DataTableText.车辆成本:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="CarName",Header="车辆", Width=80,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期", Width=80,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="Flag",Header="类型", Width=80,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注", Width=150,Index=5}};
                    break;


                case DataTableText.车辆档案:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="车牌",Width= 100,Index=0,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="Model",Header="车型",Width= 150,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="_EnableText",Header="状态",Width= 100,Index=2},
                    new DataGridColumnHeaderBinding(){BindingName="Owner",Header="车主",Width= 100,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="InsuranceDate",Header="保险日期",Width= 150,Index=4},
                   new DataGridColumnHeaderBinding(){BindingName="ExamineDate",Header="年审日期",Width= 150,Index=5},
                   new DataGridColumnHeaderBinding(){BindingName="RegisterDate",Header="注册登记日期",Width= 150,Index=6},
                   new DataGridColumnHeaderBinding(){BindingName="Vin",Header="车辆识别代号",Width= 200,Index=7},
                     new DataGridColumnHeaderBinding(){BindingName="EngineNO",Header="发动机号码",Width= 200,Index=8},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=10}};
                    if (Table.SysConfig.SysConfigParams.UseCarRentS)
                    {
                        listViewColumn.Add(new DataGridColumnHeaderBinding() { BindingName = "RentHour", Header = "时租金", Width = 100, Index = 11 });
                        listViewColumn.Add(new DataGridColumnHeaderBinding() { BindingName = "RentDay", Header = "日租金", Width = 100, Index = 12 });
                        listViewColumn.Add(new DataGridColumnHeaderBinding() { BindingName = "RentMonth", Header = "月租金", Width = 100, Index = 13 });
                    }
                    break;

                case DataTableText.预订车辆:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_CarName",Header="车牌",Width= 100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="_CarIdEnable",Header="车辆状态",Width= 100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="DateStart",Header="出车日期",Width= 100,Index=2},
                   new DataGridColumnHeaderBinding(){BindingName="DatePlan",Header="预计还车日期",Width= 100,Index=4},
                 new DataGridColumnHeaderBinding() { BindingName = "Deposit", Header = "定金", Width = 100, Index = 8 },
                   new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人",Width= 100,Index=9},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=10}};
                    listViewColumn.Add(new DataGridColumnHeaderBinding() { BindingName = "_FlagText", Header = "租赁方式", Width = 100, Index = 11 });
                    break;

                case DataTableText.行车记录:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_CarName",Header="车牌",Width= 100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="_EnableText",Header="状态",Width= 100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="DateStart",Header="出车日期",Width= 100,Index=2},
                   new DataGridColumnHeaderBinding(){BindingName="MileageStart",Header="出车里程",Width= 100,Index=3},
                   new DataGridColumnHeaderBinding(){BindingName="DatePlan",Header="预计还车日期",Width= 100,Index=4},
                   new DataGridColumnHeaderBinding(){BindingName="DateEnd",Header="还车日期",Width= 100,Index=5},
                   new DataGridColumnHeaderBinding(){BindingName="_MileageEnd",Header="还车里程",Width= 100,Index=6},
                   new DataGridColumnHeaderBinding(){BindingName="Mileage",Header="共用里程",Width= 100,Index=7},
                 new DataGridColumnHeaderBinding() { BindingName = "_AllMoney", Header = "费用", Width = 100, Index = 8 },
                   new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人",Width= 100,Index=9},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=10}};

                    if (Table.SysConfig.SysConfigParams.UseCarRentS)
                    {
                        listViewColumn.Add(new DataGridColumnHeaderBinding() { BindingName = "_FlagText", Header = "租赁方式", Width = 100, Index = 11 });
                        listViewColumn.Add(new DataGridColumnHeaderBinding() { BindingName = "Money", Header = "租金", Width = 100, Index = 12 });
                    }
                    break;

                case DataTableText.油票记录:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_CarName",Header="车牌",Width= 100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="油票号码",Width= 100,Index=1,CanSearch=true},
                   new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额",Width= 100,Index=2},
                   new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width= 100,Index=3},
                   new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人",Width= 100,Index=4},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=5}};
                    break;

                case DataTableText.违章处罚:
                case DataTableText.事故赔偿:
                case DataTableText.维修保养:
                case DataTableText.年审记录:
                case DataTableText.投保记录:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_CarName",Header="车牌",Width= 100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="_EnableText",Header="状态",Width= 100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="DateStart",Header="受理日期",Width= 100,Index=2},
                   new DataGridColumnHeaderBinding(){BindingName="DateEnd",Header="处理日期",Width= 100,Index=3},
                   new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="办事处",Width= 100,Index=4},
                   new DataGridColumnHeaderBinding(){BindingName="Money",Header="费用",Width= 100,Index=5},
                   new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="关联单号",Width= 100,Index=6},
                   new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人",Width= 100,Index=7},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=8}};
                    break;
                #endregion

                #region 售后
                case DataTableText.售后统计表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="工程师", Width=80,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客商", Width=80,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="单据数量", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_QuantityZuoFei",Header="作废单据", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="_QuantityNoMoney",Header="免费单据", Width=80,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="_QuantityHaveMoney",Header="收款单据", Width=80,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="_Money",Header="金额", Width=80,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="_MoneyError",Header="回访收款不符", Width=150,Index=7}};
                    break;
                case DataTableText.商品质保期查询:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_ProductSName",Header="商品", Width=120,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户", Width=160,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_CorSTel",Header="电话", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_OrderDateTime",Header="购买日期", Width=80,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="_OrderNO",Header="单号", Width=120,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="开单人", Width=60,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人", Width=60,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="购买数量", Width=80,Index=7},
            new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="购买价格", Width=80,Index=8},
            new DataGridColumnHeaderBinding(){BindingName="_BaoXiu",Header="保修(天)", Width=80,Index=9},
            new DataGridColumnHeaderBinding(){BindingName="_BaoXiuBe",Header="待过期(天)", Width=80,Index=10},
            new DataGridColumnHeaderBinding(){BindingName="_BaoXiuOver",Header="已过期(天)", Width=80,Index=11}};
                    break;
                case DataTableText.故障库:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            //new DataGridColumnHeaderBinding(){BindingName="_WeiXiuDuiXiang",Header="维修对象", Width=80,Index=0},
            //new DataGridColumnHeaderBinding(){BindingName="_WeiXiuLeiXing",Header="维修类型", Width=80,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_GuZhangMiaoShu",Header="故障描述", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_JieJueFangFa",Header="解决方法", Width=80,Index=3}};
                    break;
                case DataTableText.售后区域补贴统计表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_Name",Header="名称", Width=80,Index=0},
            //new DataGridColumnHeaderBinding(){BindingName="_WeiXiuLeiXing",Header="维修类型", Width=80,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="单据数量", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_Money",Header="补贴总额", Width=80,Index=3}};
                    break;


                case DataTableText.客户报修:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width= 80,Index=-2},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=-1,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="GuanLianDanHao",Header="关联单号",Width= 100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="报修日期",Width= 100,Index=1},
                   new DataGridColumnHeaderBinding(){BindingName="_WeiXiuDuiXiangName",Header="维修对象",Width= 100,Index=2},
                   new DataGridColumnHeaderBinding(){BindingName="GuZhangMiaoShu",Header="故障描述",Width= 100,Index=3},
                   new DataGridColumnHeaderBinding(){BindingName="_WeiXiuLeiXingName",Header="维修类型",Width= 100,Index=4},
                     new DataGridColumnHeaderBinding(){BindingName="ArrivalDateTime",Header="预计上门时间",Width= 100,Index=5},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=6},
                     new DataGridColumnHeaderBinding(){BindingName="_CustomerSProperty",Header="客户性质",Width= 100,Index=7},
                    new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width= 100,Index=8,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="_CorSTel",Header="联系电话",Width= 100,Index=9},
                   new DataGridColumnHeaderBinding(){BindingName="CorSContactsName",Header="联系人",Width= 100,Index=10},
                    new DataGridColumnHeaderBinding(){BindingName="CorSContactsPhone",Header="手机",Width= 100,Index=11},
                     new DataGridColumnHeaderBinding(){BindingName="_CorSAddress",Header="地址",Width= 100,Index=12}};
                    break;

                case DataTableText.售后派单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="GuanLianDanHao",Header="关联单号",Width= 100,Index=0,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="PaiDanDateTime",Header="派单日期",Width=100,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="_WeiXiuDuiXiangName",Header="维修对象",Width=100,Index=2},
                      new DataGridColumnHeaderBinding(){BindingName="GuZhangMiaoShu",Header="故障描述",Width= 100,Index=3},
                      new DataGridColumnHeaderBinding(){BindingName="_WeiXiuLeiXingName",Header="维修类型",Width= 100,Index=4},
                       new DataGridColumnHeaderBinding(){BindingName="_ServiceUserSName",Header="执行工程师",Width= 100,Index=5,CanSearch=true},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=6},
                         new DataGridColumnHeaderBinding(){BindingName="_CustomerSProperty",Header="客户性质",Width= 100,Index=7},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width= 100,Index=8,CanSearch=true},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSTel",Header="联系电话",Width=100,Index=9},
                      new DataGridColumnHeaderBinding(){BindingName="CorSContactsName",Header="联系人",Width=100,Index=10},
                      new DataGridColumnHeaderBinding(){BindingName="CorSContactsPhone",Header="手机",Width=100,Index=11},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSAddress",Header="地址",Width=100,Index=12}};
                    break;

                case DataTableText.维护完工:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="GuanLianDanHao",Header="关联单号",Width= 100,Index=0,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="报修日期",Width= 100,Index=1},
                        new DataGridColumnHeaderBinding(){BindingName="PaiDanDateTime",Header="派单日期",Width= 100,Index=2},
                        new DataGridColumnHeaderBinding(){BindingName="DaoDaDateTime",Header="到达日期",Width=100,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="WanChengDateTime",Header="完工日期",Width=100,Index=4},
                      new DataGridColumnHeaderBinding(){BindingName="GuZhangMiaoShu",Header="故障描述",Width=100,Index=5},
                       new DataGridColumnHeaderBinding(){BindingName="_ServiceUserSName",Header="执行工程师",Width=100,Index=6,CanSearch=true},
                       new DataGridColumnHeaderBinding(){BindingName="JieJueFangFa",Header="解决方法",Width= 100,Index=7},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=8},
                      new DataGridColumnHeaderBinding(){BindingName="FuWuFei",Header="服务费",Width= 100,Index=9,StyleKey="DataGridCellRight"},
                      new DataGridColumnHeaderBinding(){BindingName="JiaoTongFei",Header="交通费",Width=100,Index=10,StyleKey="DataGridCellRight"},
                      new DataGridColumnHeaderBinding(){BindingName="_FuWuPingJiaName",Header="客户评价",Width= 100,Index=11},
                         new DataGridColumnHeaderBinding(){BindingName="_CustomerSProperty",Header="客户性质",Width= 100,Index=12},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width= 100,Index=13,CanSearch=true},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSTel",Header="联系电话",Width=100,Index=14},
                      new DataGridColumnHeaderBinding(){BindingName="CorSContactsName",Header="联系人",Width=100,Index=15},
                      new DataGridColumnHeaderBinding(){BindingName="CorSContactsPhone",Header="手机",Width=100,Index=16},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSAddress",Header="地址",Width=100,Index=17}};
                    break;

                case DataTableText.回访反馈:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
               new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="GuanLianDanHao",Header="关联单号",Width= 100,Index=0,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="报修日期",Width=100,Index=1},
                        new DataGridColumnHeaderBinding(){BindingName="PaiDanDateTime",Header="派单日期",Width= 100,Index=2},
                        new DataGridColumnHeaderBinding(){BindingName="DaoDaDateTime",Header="到达日期",Width=100,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="WanChengDateTime",Header="完工日期",Width= 100,Index=4},
                      new DataGridColumnHeaderBinding(){BindingName="GuZhangMiaoShu",Header="故障描述",Width=100,Index=5},
                       new DataGridColumnHeaderBinding(){BindingName="_ServiceUserSName",Header="执行工程师",Width=100,Index=6,CanSearch=true},
                       new DataGridColumnHeaderBinding(){BindingName="JieJueFangFa",Header="解决方法",Width= 100,Index=7},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=8},
                      new DataGridColumnHeaderBinding(){BindingName="_CustomerSProperty",Header="客户性质",Width= 100,Index=9},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width= 100,Index=10,CanSearch=true},
                      new DataGridColumnHeaderBinding(){BindingName="FuWuFei",Header="服务费",Width=100,Index=11,StyleKey="DataGridCellRight"},
                      new DataGridColumnHeaderBinding(){BindingName="JiaoTongFei",Header="交通费",Width=100,Index=12,StyleKey="DataGridCellRight"},
                      new DataGridColumnHeaderBinding(){BindingName="FuWuFeiHeShi",Header="收费核实",Width=100,Index=13},
                      new DataGridColumnHeaderBinding(){BindingName="_FuWuPingJiaName",Header="客户评价",Width= 100,Index=14},
                    new DataGridColumnHeaderBinding(){BindingName="_KeHuPingJiaHeShiName",Header="客户评价核实",Width= 100,Index=15},
                     new DataGridColumnHeaderBinding(){BindingName="KeHuYiJian",Header="客户意见",Width= 100,Index=16}};
                    break;

                case DataTableText.外包合同:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                      new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width= 100,Index=0,CanSearch=true},
                      new DataGridColumnHeaderBinding(){BindingName="_UniqueSName",Header="服务级别",Width= 100,Index=1},
               new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="合同号",Width= 100,Index=2,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="StartDateTime",Header="签约日期",Width= 100,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="EndDateTime",Header="结束日期",Width=100,Index=4},
                      new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额",Width=100,Index=5},
                      new DataGridColumnHeaderBinding(){BindingName="_OperaterName",Header="经办人",Width=100,Index=6},
                      new DataGridColumnHeaderBinding(){BindingName="_EnableText",Header="状态",Width=100,Index=7},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=8} };
                    break;

                case DataTableText.客户坏件送修:
                case DataTableText.供应商坏件送修:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
               new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户名称",Width= 100,Index=1,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="_CorSTel",Header="客户电话",Width= 100,Index=2},
                        new DataGridColumnHeaderBinding(){BindingName="_CorSAddress",Header="客户地址",Width= 100,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="StartDateTime",Header="下单日期",Width=100,Index=5}};
                    break;
                // case DataTableText.客户坏件取回:
                //     listViewColumn = new List<DataGridColumnHeaderBinding>{
                //new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0},
                //         new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户名称",Width= 100,Index=1},
                //         new DataGridColumnHeaderBinding(){BindingName="_CorSTel",Header="客户电话",Width= 100,Index=2},
                //         new DataGridColumnHeaderBinding(){BindingName="_CorSAddress",Header="客户地址",Width= 100,Index=3},
                //         new DataGridColumnHeaderBinding(){BindingName="WeiXiuLeiXingName",Header="维修类型",Width=100,Index=4},
                //         new DataGridColumnHeaderBinding(){BindingName="KeHuFanXiuDateTime",Header="下单日期",Width=100,Index=5},
                //         new DataGridColumnHeaderBinding(){BindingName="GongYingShangFanXiuDateTime",Header="供应商寄出日期",Width= 100,Index=6},
                //        new DataGridColumnHeaderBinding(){BindingName="GongYingShangFanHuiDateTime",Header="供应商返回日期",Width= 100,Index=7},
                //         new DataGridColumnHeaderBinding(){BindingName="KeHuQuHuiDateTime",Header="客户取回日期",Width=100,Index=3}};
                //     break;
                // case DataTableText.供应商坏件返回:
                //     listViewColumn = new List<DataGridColumnHeaderBinding>{
                //new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=0},
                //       new DataGridColumnHeaderBinding(){BindingName="_supplierSName",Header="供应商名称",Width= 100,Index=1},
                //         new DataGridColumnHeaderBinding(){BindingName="_supplierSTel",Header="供应商电话",Width= 100,Index=2},
                //         new DataGridColumnHeaderBinding(){BindingName="_supplierSAddress",Header="供应商地址",Width= 100,Index=3},
                //         new DataGridColumnHeaderBinding(){BindingName="WeiXiuLeiXingName",Header="维修类型",Width=100,Index=4},
                //         new DataGridColumnHeaderBinding(){BindingName="GongYingShangFanXiuDateTime",Header="供应商寄出日期",Width= 100,Index=5},
                //         new DataGridColumnHeaderBinding(){BindingName="GongYingShangFanHuiDateTime",Header="供应商返回日期",Width= 100,Index=6}};
                //     break;
                // case DataTableText.员工评价:
                //     objectListViewColumn = new List<DataGridColumnHeaderBinding>{
                //new DataGridColumnHeaderBinding(){BindingName="KaoHeFenShu",Header="考核分数",Width= ,true,false),
                //new DataGridColumnHeaderBinding(){BindingName="KaoHeYiJian",Header="考核意见",Width= ,true,false),
                //       new DataGridColumnHeaderBinding(){BindingName="GuZhangMiaoShu",Header="故障描述",Width= ,false,true),
                //        new DataGridColumnHeaderBinding(){BindingName="",Header="执行工程师",Width= ,false,true),
                //        new DataGridColumnHeaderBinding(){BindingName="JieJueFangFa",Header="解决方法",Width= ,false,true),
                //       new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= ,true,false),
                //       new DataGridColumnHeaderBinding(){BindingName="CustomerSProperty",Header="客户性质",Width= ,false,true),
                //       new DataGridColumnHeaderBinding(){BindingName="FuWuFei",Header="服务费",Width= ,false,true),
                //       new DataGridColumnHeaderBinding(){BindingName="JiaoTongFei",Header="交通费",Width= ,false,true),
                //       new DataGridColumnHeaderBinding(){BindingName="FuWuFeiHeShi",Header="收费核实",Width= ,false,true),
                //       new DataGridColumnHeaderBinding(){BindingName="KeHuPingJiaName",Header="客户评价",Width= ,false,true),
                //     new DataGridColumnHeaderBinding(){BindingName="KeHuPingJiaHeShiName",Header="客户评价核实",Width= ,false,true)};
                //     break;

                #endregion

                #region 仓库
                case DataTableText.调拨单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 150,Index=0,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=150,Index=1},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSOutName",Header="调出仓库",Width=150,Index=2},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSInName",Header="调入仓库",Width=150,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width=150,Index=4},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=5}};
                    break;

                case DataTableText.库存调价单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 150,Index=1,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=150,Index=2},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=150,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width=150,Index=4},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=5}};
                    break;

                case DataTableText.盘点单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="_EnableText",Header="盘点状态",Width=150,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="盘点批次",Width= 150,Index=1,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="开单日期",Width=150,Index=2},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=150,Index=3},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=4},
     new DataGridColumnHeaderBinding(){BindingName="_UserSName",Header="经办人",Width= 150,Index=5}};
                    break;

                case DataTableText.仓库:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="仓库名称", Width=200,Index=0,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="Address",Header="地址",Width= 200,Index=1},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=2}};
                    break;

                case DataTableText.非采购入库单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="开单日期",Width=150,Index=0},
                        //new DataGridColumnHeaderBinding(){BindingName="ArrivalDateTime",Header="到货日期",Width= 150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="入库单号",Width= 150,Index=2,CanSearch=true},
            //new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="采购单号",Width=150,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="收货仓库",Width=150,Index=4},
                        //new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="供应商",Width=100,Index=5},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=6}};
                    break;
                case DataTableText.非销售出库单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="开单日期",Width=150,Index=0},
                        //new DataGridColumnHeaderBinding(){BindingName="ArrivalDateTime",Header="出货日期",Width= 150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="出库单号",Width=150,Index=2,CanSearch=true},
            //new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="批发单号",Width=150,Index=3},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="发货仓库",Width= 150,Index=4},
                        //new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width=100,Index=5},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=6}};
                    break;

                case DataTableText.仓库单据入库单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="开单日期",Width=150,Index=0},
                        new DataGridColumnHeaderBinding(){BindingName="ArrivalDateTime",Header="到货日期",Width= 150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="入库单号",Width= 150,Index=2,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="采购单号",Width=150,Index=3,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="NoteNO",Header="配送单号",Width=150,Index=4,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="收货仓库",Width=150,Index=5},
                        new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="供应商",Width=100,Index=6},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=7}};
                    break;

                case DataTableText.仓库单据出库单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="开单日期",Width=150,Index=0},
                        new DataGridColumnHeaderBinding(){BindingName="ArrivalDateTime",Header="出货日期",Width= 150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="出库单号",Width=150,Index=2,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="批发单号",Width=150,Index=3,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="NoteNO",Header="配送单号",Width=150,Index=4,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="发货仓库",Width= 150,Index=5},
                        new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width=100,Index=6},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 150,Index=7}};
                    break;

                case DataTableText.仓库商品入库单:
                case DataTableText.仓库商品出库单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="_ProductSName",Header="商品名称",Width=150,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="数量",Width=150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="相关单号",Width= 150,Index=2,CanSearch=true},
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="开单日期",Width=150,Index=3}};
                    break;

                case DataTableText.商品出入库明细表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_ProductSName",Header="商品名称", Width=200,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="RelatedOrderNO",Header="相关单号", Width=150,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="Price",Header="价格", Width=80,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="数量",Width=50,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="AveragePrice",Header="库存单价",Width= 80,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=100,Index=8} };
                    break;
                case DataTableText.库存单价变动情况表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="ProductSName",Header="商品名称", Width=200,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单据类型", Width=200,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="AveragePrice",Header="库存单价",Width= 80,Index=2},
         new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="StoreSName",Header="仓库",Width=100,Index=4} };
                    break;
                case DataTableText.商品实时库存表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_ProductSName",Header="商品名称", Width=200,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="AveragePrice",Header="库存单价",Width= 80,Index=1},
         new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="数量",Width=80,Index=2},
         new DataGridColumnHeaderBinding(){BindingName="_Sum",Header="库存成本",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="QuantityMax",Header="最大库存量",Width=80,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="QuantityMin",Header="最低库存量",Width=80,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=100,Index=6} };
                    break;
                #endregion

                #region 采购销售
                case DataTableText.采购价格变动情况表:
                case DataTableText.销售价格变动情况表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="_ProductSName",Header="商品名称", Width=80,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="_Money",Header="价格", Width=80,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客商", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_OrderDateTime",Header="记账日", Width=150,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人", Width=150,Index=3}};
                    break;
                case DataTableText.采购统计表:
                case DataTableText.销售统计表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Flag",Header="单据类型", Width=100,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="StoreSName",Header="仓库", Width=200,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="CorSName",Header="客商", Width=200,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="单据数量", Width=80,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="Money",Header="金额", Width=80,Index=3}  };
                    break;



                case DataTableText.采购明细表:
                case DataTableText.采购订单:
                case DataTableText.采购退货单:
                case DataTableText.采购询价单:
                case DataTableText.赠品入库单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="Flag",Header="单据类型",Width=100,Index=0},
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width= 100,Index=2,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Money",Header="合计金额",Width=100,Index=3},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="供应商",Width=100,Index=4,CanSearch=true},
                          new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人",Width= 100,Index=5},
                            new DataGridColumnHeaderBinding(){BindingName="_AuditorUserSName",Header="审核人",Width= 100,Index=6},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=100,Index=7},
                        new DataGridColumnHeaderBinding(){BindingName="Address",Header="收货地址",Width=100,Index=8},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 100,Index=9},
                      new DataGridColumnHeaderBinding(){BindingName="_EnableText",Header="状态",Width= 100,Index=10}};
                    break;

                case DataTableText.销售明细表:
                case DataTableText.销售订单:
                case DataTableText.销售退货单:
                case DataTableText.销售询价单:
                case DataTableText.赠品出库单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号",Width=100,Index=1,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Money",Header="合计金额",Width=100,Index=2},
                      new DataGridColumnHeaderBinding(){BindingName="_CorSName",Header="客户",Width=100,Index=3,CanSearch=true},
                          new DataGridColumnHeaderBinding(){BindingName="_OperatorerSName",Header="经办人",Width= 100,Index=4},
                            new DataGridColumnHeaderBinding(){BindingName="_AuditorUserSName",Header="审核人",Width=100,Index=5},
                        new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=100,Index=6},
                        new DataGridColumnHeaderBinding(){BindingName="Address",Header="收货地址",Width=100,Index=7},
                      new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=8},
                      new DataGridColumnHeaderBinding(){BindingName="_EnableText",Header="状态",Width= 100,Index=9}};
                    break;
                #endregion

                #region 档案资料
                case DataTableText.区域:
                case DataTableText.公寓固定费用:
                case DataTableText.公寓仪表费用:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称", Width=200,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="DoubleValue",Header="金额", Width=200,Index=1},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=2}};
                    break;
                case DataTableText.发票定义:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称", Width=200,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="DoubleValue",Header="税率", Width=200,Index=1},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=2}};
                    break;

                case DataTableText.会员分类:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称", Width=200,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="_VipUniqueSName",Header="会员类别", Width=200,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="DoubleValue",Header="折扣", Width=200,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="BoolValue",Header="是否积分", Width=200,Index=3},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=4}};
                    break;

                case DataTableText.房间类型:
                case DataTableText.标记:
                case DataTableText.任务阶段名称:
                case DataTableText.项目相关状态:
                case DataTableText.产地:
                case DataTableText.品牌:
                case DataTableText.货架:
                case DataTableText.行:
                case DataTableText.客户性质:
                case DataTableText.客户提供资料:
                case DataTableText.服务评价:
                case DataTableText.维修对象:
                case DataTableText.维修类型:
                case DataTableText.操作员:
                case DataTableText.计量单位:
                case DataTableText.服务级别定义:
                case DataTableText.任务类型:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称", Width=200,Index=0,CanSearch=true},
                     new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width= 200,Index=1}};
                    break;

                case DataTableText.设备分类:
                case DataTableText.客商分类:
                case DataTableText.商品分类:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
              new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_ParentName",Header="类别",Width=100,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="_Count",Header="记录数",Width=80,Index=3}};
                    break;


                case DataTableText.可采购物料清单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="虚拟库存",Width=100,Index=7},
           new DataGridColumnHeaderBinding(){BindingName="PurchasePrice",Header="进货价",Width=80,Index=8},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=15},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=16},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=17}   ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=18,ColumnType="Image"}   };
                    break;
                case DataTableText.物料清单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="虚拟库存",Width=100,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="PosPrice",Header="零售价",Width=80,Index=9},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=15},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=16},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=17}   ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=18,ColumnType="Image"}   };
                    break;
                case DataTableText.可销售物料清单:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="虚拟库存",Width=100,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="PosPrice",Header="零售价",Width=80,Index=9},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice1",Header="一级批发价",Width=80,Index=10},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice2",Header="二级批发价",Width=80,Index=11},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice3",Header="三级批发价",Width=80,Index=12},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice4",Header="四级批发价",Width=80,Index=13},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice5",Header="五级批发价",Width=80,Index=14},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=15},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=16},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=17}   ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=18,ColumnType="Image"}   };
                    break;
                case DataTableText.仓库商品:
                case DataTableText.商品校验:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="库存",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_AveragePrice",Header="库存单价",Width=100,Index=6},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=13},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=14},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=15} ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=16,ColumnType="Image"} };
                    break;
                case DataTableText.可销售商品:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         //new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="库存",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="PosPrice",Header="零售价",Width=80,Index=7},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice1",Header="一级批发价",Width=80,Index=8},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice2",Header="二级批发价",Width=80,Index=9},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice3",Header="三级批发价",Width=80,Index=10},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice4",Header="四级批发价",Width=80,Index=11},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice5",Header="五级批发价",Width=80,Index=12},
           new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=13},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=14},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=15} ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=16,ColumnType="Image"}   };
                    break;
                case DataTableText.可采购商品:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,CanSearch=true},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,CanSearch=true},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=100,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         //new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="库存",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="PurchasePrice",Header="进货价",Width=80,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=60,Index=13} ,
                    new DataGridColumnHeaderBinding(){BindingName="_ImageFile",Header="图片",Width=60,Index=14,ColumnType="Image"}    };
                    break;

                case DataTableText.供应商:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                    new DataGridColumnHeaderBinding(){BindingName="Code",Header="编号",Width=100,Index=-1,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Name",Header="供应商名称",Width=100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Tel",Header="电话",Width=100,Index=1,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Fax",Header="传真",Width=100,Index=3},
                    new DataGridColumnHeaderBinding(){BindingName="Email",Header="电子邮箱",Width=100,Index=4},
                    new DataGridColumnHeaderBinding(){BindingName="Corporation",Header="法人名称",Width=100,Index=5},
                    new DataGridColumnHeaderBinding(){BindingName="Address",Header="地址",Width=100,Index=7},
                    new DataGridColumnHeaderBinding(){BindingName="Post",Header="邮政编码",Width=100,Index=8},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=9}};
                    break;

                case DataTableText.客户:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                    new DataGridColumnHeaderBinding(){BindingName="Code",Header="编号",Width=100,Index=-1,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Name",Header="客户名称",Width=100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Tel",Header="电话",Width=100,Index=1,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Fax",Header="传真",Width=100,Index=3},
                    new DataGridColumnHeaderBinding(){BindingName="Email",Header="电子邮箱",Width=100,Index=4},
                    new DataGridColumnHeaderBinding(){BindingName="Corporation",Header="法人名称",Width=100,Index=5},
                    new DataGridColumnHeaderBinding(){BindingName="Address",Header="地址",Width=100,Index=7},
                    new DataGridColumnHeaderBinding(){BindingName="Post",Header="邮政编码",Width=100,Index=8},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=9},
                    new DataGridColumnHeaderBinding(){BindingName="_GroupSName",Header="分类",Width=100,Index=9},
                    new DataGridColumnHeaderBinding(){BindingName="_KeHuXingZhi",Header="客户性质",Width=100,Index=10}};
                    break;

                case DataTableText.会员资料:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                    new DataGridColumnHeaderBinding(){BindingName="VipNO",Header="会员号码",Width=100,Index=0,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="_VipUniqueSName",Header="会员类别",Width=100,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="VipMoney",Header="消费总额",Width=50,Index=2},
                    new DataGridColumnHeaderBinding(){BindingName="VipIntegral",Header="累积积分",Width=50,Index=3},
                    new DataGridColumnHeaderBinding(){BindingName="VipRemainIntegral",Header="剩余积分",Width=50,Index=4},
                    new DataGridColumnHeaderBinding(){BindingName="Name",Header="客户名称",Width=100,Index=5,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Tel",Header="电话",Width=100,Index=6,CanSearch=true},
                    new DataGridColumnHeaderBinding(){BindingName="Email",Header="电子邮箱",Width=100,Index=8},
                    new DataGridColumnHeaderBinding(){BindingName="Address",Header="地址",Width=100,Index=10},
                    new DataGridColumnHeaderBinding(){BindingName="Post",Header="邮政编码",Width=100,Index=11},
                    new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=12} };
                    break;
                #endregion

                #region boss报表
                case DataTableText.商品毛利表:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
         new DataGridColumnHeaderBinding(){BindingName="OrderDateTime",Header="日期",Width=100,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_ProductSName",Header="商品名称", Width=200,Index=2},
            new DataGridColumnHeaderBinding(){BindingName="OrderNO",Header="单号", Width=150,Index=3},
            new DataGridColumnHeaderBinding(){BindingName="_OrderTypeName",Header="单据类型", Width=150,Index=4},
            new DataGridColumnHeaderBinding(){BindingName="WholesalePrice",Header="销售价", Width=80,Index=5},
            new DataGridColumnHeaderBinding(){BindingName="Quantity",Header="数量",Width=50,Index=6},
            new DataGridColumnHeaderBinding(){BindingName="AveragePrice",Header="库存单价",Width= 80,Index=7},
            new DataGridColumnHeaderBinding(){BindingName="_GrossProfit",Header="毛利",Width= 80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="_StoreSName",Header="仓库",Width=100,Index=9} };
                    break;
                case DataTableText.营业额:
                    listViewColumn = new List<DataGridColumnHeaderBinding>(){
         new DataGridColumnHeaderBinding(){BindingName="_Name",Header="日期",Width=200,Index=1},
            new DataGridColumnHeaderBinding(){BindingName="_Money",Header="金额", Width=100,Index=2} };
                    break;
                #endregion

                #region 商品编辑控件 DataGridExt 使用
                case DataTableText.请货单商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=5}, 
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 6,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="配送价格",Width=100,Index= 7 }};
                    break;
                case DataTableText.配送单商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=5}, 
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 6,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="配送价格",Width=100,Index= 7,IsReadOnly=false}};
                    break;
                case DataTableText.Pos零售商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="Name",Header="商品名称",Width=250,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width= 55,Index=1},
                    new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=80,Index=2},
                      new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=55,Index=3},
                          new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="售价",Width= 55,Index=4},
                            new DataGridColumnHeaderBinding(){BindingName="_TempSum",Header="金额",Width=80,Index=5},
                        new DataGridColumnHeaderBinding(){BindingName="PosPrice",Header="零售价",Width=80,Index=6},
                        new DataGridColumnHeaderBinding(){BindingName="VipPrice",Header="会员价",Width=80,Index=7},
                      new DataGridColumnHeaderBinding(){BindingName="_Discount",Header="折扣",Width= 55,Index=8},
                      new DataGridColumnHeaderBinding(){BindingName="_DiscountPrice",Header="折扣价",Width= 80,Index=9}};

                    //if (DoubleHConfig.AppConfig.PosShowImg)
                    //    listViewColumn.Add(new DataGridColumnHeaderBinding() { BindingName = "_ImageFile", Header = "图片", Width = 60, Index = 10, ColumnType = "image" });

                    break;

                case DataTableText.Pos零售商品编辑触摸屏简版:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
                        new DataGridColumnHeaderBinding(){BindingName="Name",Header="商品名称",Width=280,Index=0},
                      new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index=3},
                          new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="售价",Width= 60,Index=4}};
                    break;

                case DataTableText.物料清单商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=4},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 11,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="成本(%)",Width=50,Index= 12,IsReadOnly=false}};
                    break;
                case DataTableText.销售询价单商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="PosPrice",Header="零售价",Width=50,Index=6},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="单价",Width=50,Index=7,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 8,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempSum",Header="金额",Width=100,Index= 9},
         new DataGridColumnHeaderBinding(){BindingName="_TempAverageSum",Header="成本",Width=100,Index= 10} ,
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=11},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index= 12}};
                    break;

                case DataTableText.赠品商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 6,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="实时库存",Width=50,Index=8},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=9},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=10},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=11} ,
          new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=12},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index= 13}};
                    break;
                case DataTableText.采购单据商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_SerialNumber",Header="序列号",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="单价",Width=50,Index=5,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 6,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempSum",Header="金额",Width=100,Index= 7},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="实时库存",Width=50,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="PurchasePrice",Header="进货价",Width=50,Index=9},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=11},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=172},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=13} };
                    break;
                case DataTableText.销售单据商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_SerialNumber",Header="序列号",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="单价",Width=50,Index=5,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 6,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_TempSum",Header="金额",Width=100,Index= 7},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="实时库存",Width=50,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="PosPrice",Header="零售价",Width=50,Index=10},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice1",Header="一级批发价",Width=50,Index=11},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice2",Header="二级批发价",Width=50,Index=12},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice3",Header="三级批发价",Width=50,Index=13},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice4",Header="四级批发价",Width=50,Index=14},
          new DataGridColumnHeaderBinding(){BindingName="WholesalePrice5",Header="五级批发价",Width=50,Index=15},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=16},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=17},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=18},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=19},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index= 20}};
                    break;
                case DataTableText.仓库单据商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_SerialNumber",Header="序列号",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="数量",Width=50,Index= 6,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="实时库存",Width=50,Index= 7},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=9},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=10} ,
       new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=11},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index= 12}};
                    break;
                case DataTableText.库存调价单商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=5},
         new DataGridColumnHeaderBinding(){BindingName="_AveragePrice",Header="原库存单价",Width=80,Index= 6},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="新库存单价",Width=80,Index= 7,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=18}    };
                    break;
                case DataTableText.仓库调拨单商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Width=100,Index=0,IsReadOnly=false},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Width= 100,Index=1,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_SerialNumber",Header="序列号",Width=120,Index=2,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Model",Header="型号",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_AveragePrice",Header="库存单价",Width=50,Index= 6},
         new DataGridColumnHeaderBinding(){BindingName="_TempPrice",Header="调入单价",Width=80,Index= 7,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="_Quantity",Header="库存",Width=50,Index= 8},
         new DataGridColumnHeaderBinding(){BindingName="_TempQuantity",Header="调出数量",Width=80,Index= 9,IsReadOnly=false},
         new DataGridColumnHeaderBinding(){BindingName="Note",Header="备注",Width=100,Index=10},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoName",Header="组合名称",Width=100,Index=11},
         new DataGridColumnHeaderBinding(){BindingName="_WuLiaoQuantity",Header="组合数量",Width=100,Index= 12}};
                    break;
                case DataTableText.盘点单据商品编辑:
                    listViewColumn = new List<DataGridColumnHeaderBinding>{
            new DataGridColumnHeaderBinding(){BindingName="_Name",Header="名称",Width=100,Index=0},
            new DataGridColumnHeaderBinding(){BindingName="_Code",Header="编码",Width= 100,Index=1},
         new DataGridColumnHeaderBinding(){BindingName="_Barcode",Header="条码",Width=120,Index=2},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Width=50,Index=3},
         new DataGridColumnHeaderBinding(){BindingName="_Standard",Header="规格",Width=100,Index=4},
         new DataGridColumnHeaderBinding(){BindingName="_Model",Header="型号",Width=100,Index=4},
          new DataGridColumnHeaderBinding(){BindingName="_PlaceName",Header="产地",Width=80,Index=7},
         new DataGridColumnHeaderBinding(){BindingName="_BrandName",Header="品牌",Width=80,Index=8},
         new DataGridColumnHeaderBinding(){BindingName="_Note",Header="备注",Width=100,Index=9},  
         new DataGridColumnHeaderBinding(){BindingName="_QuantityOld",Header="库存",Width=50,Index=10},//防止盘点结束后商品库存变动导致显示错误
         new DataGridColumnHeaderBinding(){BindingName="_QuantityNew",Header="盘点数量",Width=100,Index= 11,IsReadOnly=false}};
                    break;
                #endregion
            }
            return listViewColumn ?? new List<DataGridColumnHeaderBinding>();
        }
    }
    #endregion
}