﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Table=FCNS.Data.Table;

namespace DoubleH.Utility.Login
{
   public class LoginUtility
    {
       /// <summary>
       /// 连接默认的数据库然后返回登录的用户
       /// </summary>
       /// <returns></returns>
       public static Table.UserS MiniLogin()
       {
           DoubleH.Utility.TextInputWindow tiw = new TextInputWindow();
           tiw.Init("格式：账号.密码（例如:001.000000）", null, true);
           tiw.ShowDialog();
           string tiwText = tiw.GetText;
           if (string.IsNullOrEmpty(tiwText) || !tiwText.Contains("."))
               return null;

           int index = tiwText.IndexOf('.', 0);
           return  Table.UserS.GetObject(tiwText.Substring(0, index), tiwText.Substring(index + 1));
       }

       /// <summary>
       /// 显示登录窗体并且是否登录成功
       /// </summary>
       /// <returns></returns>
       public static bool ShowLoginWindowSuccess(Table.UserS.EnumFlag flag)
       {
           Table.UserS.LoginUser = null;
           DoubleH.Utility.Login.LoginWindow lw = new Utility.Login.LoginWindow();
           lw.Init(flag);
           lw.ShowDialog();
           return Table.UserS.LoginUser != null; 
       }
    }
}
