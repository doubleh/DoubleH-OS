﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using DoubleH.Utility;
using System.Collections;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using Table = FCNS.Data.Table;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Markup;
using DoubleH.Utility.Configuration;
using System.Reflection;

namespace DoubleH.Utility.UC
{
    public partial class UCCustomView : UserControl
    {
        public enum EnumMode
        {
            列表 = 0,
            缩略图
        }

        public enum EnumModeTemplete
        {
            /// <summary>
            /// 表示不支持缩略图功能
            /// </summary>
            Null,
            PosProductSItemTemplate,
            PosProductSItemTemplateNoImg,
            PosProductSItemTemplateNoText,

            PurchaseProductSItemTemplate,
            GroupSItemTemplateHaveImage
        }

        public delegate void dChangedMode(EnumMode oldMode, EnumMode newMode);
        public event dChangedMode ModeChanged;
        public delegate void dDoubleClickItem(object item);
        public event dDoubleClickItem ItemDoubleClick;
        IList groups = null;
        IList homeItems = null;
        protected bool m_IsDraging = false;
        protected Point m_DragStartPoint;

        public UCCustomView()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitItemUI();
            InitEvent();
        }
        
        #region 属性
       public Brush BackgroundBrush
       {
           set
           {
               listViewExtGroup.Background = value;
               listViewExtItem.Background = value;
               gridGroup.Background = value;
           }
       }

       public Brush ForegroundBrush
       {
           set
           {
               this.BorderBrush = value;
               listViewExtGroup.Foreground = value;
            listViewExtItem.Foreground =value;
            listViewExtGroup.BorderBrush = value;
           }
       }

        public DataGridExt UCDataGridExt
        {
            get { return dataGridExtItem; }
        }

        public object SelectedItem
        {
            get
            {
                switch (mode)
                {
                    case EnumMode.列表: return dataGridExtItem.SelectedItem;
                    case EnumMode.缩略图: return listViewExtItem.SelectedItem;
                    default: return null;
                }
            }
        }

        public object SelectedGroupItem
        {
            get
            {
                if (mode == EnumMode.缩略图)
                    return listViewExtGroup.SelectedItem;
                else
                    return null;
            }
        }

        public IEnumerable SelectedItems
        {
            get
            {
                switch (mode)
                {
                    case EnumMode.列表: return dataGridExtItem.SelectedItems;
                    case EnumMode.缩略图: return listViewExtItem.SelectedItems;
                    default: return null;
                }
            }
        }

        EnumMode mode = EnumMode.列表;
        public EnumMode Mode
        {
            get { return mode; }
            set
            {
                mode = value;
                InitGroupUI();
                InitItemUI();
                InitItemData(items);
            }
        }

        EnumModeTemplete listViewGroupTempleteName = EnumModeTemplete.GroupSItemTemplateHaveImage;
        /// <summary>
        /// 缩略图模式下顶部分类的模板名称
        /// </summary>
        public EnumModeTemplete ListViewGroupTempleteName
        {
            get { return listViewGroupTempleteName; }
            set
            {
                listViewGroupTempleteName = value;
            }
        }

        EnumModeTemplete listViewBodyTempleteName = EnumModeTemplete.Null;
        /// <summary>
        /// 缩略图模式下内容的模板名称
        /// </summary>
        public EnumModeTemplete ListViewBodyTempleteName
        {
            get { return listViewBodyTempleteName; }
            set
            {
                listViewBodyTempleteName = value;
                InitItemUI();
                InitItemData(items);
            }
        }

        IList items = null;
        public IList Items
        {
            get { return items; }
            //set
            //{
            //    items = value;
            //    InitData();
            //}
        }
        #endregion

        #region public
        public void AddMenuItem(MenuItem menu)
        {
            this.ContextMenu.Items.Add(menu);
        }

        public void Init(EnumMode mode, List<DataGridColumnHeaderBinding> columns, EnumModeTemplete dataTempleteName= EnumModeTemplete.Null)
        {
            //只需要初始化一次的放这里
            this.mode = mode;
            this.listViewBodyTempleteName = dataTempleteName;
            imageSuoLueTu.Visibility = dataTempleteName ==  EnumModeTemplete.Null ? Visibility.Collapsed : Visibility.Visible;
            imageLieBiao.Visibility = columns == null ? Visibility.Collapsed : Visibility.Visible;
            switch (mode)
            {
                case EnumMode.列表: dataGridExtItem.InitColumns(columns); break;
                //case EnumMode.缩略图: listViewExtItem.ChangeView(dataTempleteName); break;
            }
            InitItemUI();
        }

        public void LoadData(IList groups, IList items)
        {
            this.groups = groups;
            this.items = items;

            InitGroupUI();
            InitGroupData();
            InitItemData(items);
        }
        /// <summary>
        /// 定义首页显示的数据
        /// </summary>
        /// <param name="items"></param>
        public void LoadData(IList items)
        {
            this.homeItems = items;
            InitItemData(items);
        }
        #endregion

        #region private
        private void InitEvent()
        {
            imageHome.MouseUp += (ss, ee) => GoHome();
            listViewExtItem.MouseDoubleClick += (ss, ee) => ListViewExtMouseDoubleClick();
            dataGridExtItem.MouseDoubleClick += (ss, ee) => DataGridExtMouseDoubleClick();
            listViewExtGroup.SelectionChanged += (ss, ee) => ListViewExtGroupSelectionChanged();
            imageLieBiao.MouseUp += (ss, ee) => ChangeMode(EnumMode.列表);
            imageSuoLueTu.MouseUp += (ss, ee) => ChangeMode(EnumMode.缩略图);
            
            listViewExtItem.PreviewMouseLeftButtonDown += (ss, ee) => DropStart(ee);
            listViewExtItem.PreviewMouseMove += (ss, ee) => DropMove(ee);
            imageHome.PreviewDrop += (ss, ee) => DropEnd(ee);
        }

        private void GoHome()
        {
            listViewExtGroup.SelectedItem = null;
            InitItemData(homeItems??items);
        }

        private void ListViewExtMouseDoubleClick()
        {
            if (listViewExtItem.SelectedItem == null || ItemDoubleClick == null)
                return;

            ItemDoubleClick(listViewExtItem.SelectedItem);
        }

        private void DataGridExtMouseDoubleClick()
        {
            if (dataGridExtItem.SelectedItem == null || ItemDoubleClick == null)
                return;

            ItemDoubleClick(listViewExtItem.SelectedItem);
        }

        private void ListViewExtGroupSelectionChanged()
        {
            if (listViewExtGroup.SelectedItem == null || items == null)
                return;

            Table.DataTableS table = listViewExtGroup.SelectedItem as Table.DataTableS;
            if (table == null)
                InitItemData(items);
            else
            {
                List<object> obj = new List<object>();
                Type t = items[0].GetType();
                PropertyInfo pi = t.GetProperty("GroupSId");
                if (pi == null)
                    return;

                foreach (object ob in items)
                    if ((long)pi.GetValue(ob, null) == table.Id)
                        obj.Add(ob);

                InitItemData(obj);
            }
        }

        private void ChangeMode(EnumMode newMode)
        {
            if (items == null || mode == newMode)
                return;

            if (ModeChanged != null)
                ModeChanged(mode, newMode);

            Mode = newMode;
        }

        private void InitItemUI()
        {
            switch (mode)
            {
                case EnumMode.列表: InitItemUI0(); break;
                case EnumMode.缩略图: InitItemUI1(); break;
            }
        }

        private void InitGroupUI()
        {
            if (groups == null)
            {
                gridGroup.Height = 0;
                dataGridExtItem.Margin = new Thickness(0, 20, 0, 0);
                listViewExtItem.Margin = new Thickness(0, 20, 0, 0);
            }
            else
            {
                switch (mode)
                {
                    case EnumMode.列表:
                        gridGroup.Height = 30;
                        dataGridExtItem.Margin = new Thickness(0, 50, 0, 0);
                        listViewExtItem.Margin = new Thickness(0, 50, 0, 0);
                        break;
                    case EnumMode.缩略图:
                        gridGroup.Height = 100;
                        dataGridExtItem.Margin = new Thickness(0, 118, 0, 0);
                        listViewExtItem.Margin = new Thickness(0, 118, 0, 0);
                        break;
                }
            }
        }

        private void InitGroupData()
        {
            if (groups != null)
                listViewExtGroup.ItemsSource = groups;
        }

        private void InitItemData(IList obj)
        {
            switch (mode)
            {
                case EnumMode.列表: InitItemData0(obj); break;
                case EnumMode.缩略图: InitItemData1(obj); break;
            }
        }

        #region 列表模式
        private void InitItemUI0()
        {
            dataGridExtItem.Visibility = Visibility.Visible;
            listViewExtItem.Visibility = Visibility.Collapsed;

            listViewExtGroup.ChangeView(listViewGroupTempleteName.ToString(), "myPlainViewDSK2", 18);
        }

        private void InitItemData0(IList obj)
        {
            dataGridExtItem.ItemsSource = obj;
        }
        #endregion

        #region 缩略图模式
        private void InitItemUI1()
        {
            dataGridExtItem.Visibility = Visibility.Collapsed;
            listViewExtItem.Visibility = Visibility.Visible;
            listViewExtGroup.ChangeView(listViewGroupTempleteName.ToString(), "myPlainViewDSK2", 64);
            listViewExtItem.ChangeView(listViewBodyTempleteName.ToString(), "myPlainViewDSK", 120);
        }

        private void InitItemData1(IList obj)
        {
            listViewExtItem.ItemsSource = obj;
        }
        #endregion

        #region 拖放
        private void DropStart(MouseEventArgs ee)
        {
            m_DragStartPoint = ee.GetPosition(null);
        }

        private void DropMove(MouseEventArgs ee)
        {
            if (listViewExtItem.SelectedItem == null)
                return;

            if (ee.LeftButton == MouseButtonState.Pressed && !m_IsDraging)
            {
                Point position = ee.GetPosition(null);

                if (Math.Abs(position.X - m_DragStartPoint.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(position.Y - m_DragStartPoint.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    m_IsDraging = true;
                    DataObject data = new DataObject(typeof(Table.DataTableS), listViewExtItem.SelectedItem);
                    DragDropEffects de = DragDrop.DoDragDrop(listViewExtItem, data, DragDropEffects.Copy);
                    m_IsDraging = false;
                }
            }
        }

        private void DropEnd(DragEventArgs ee)
        {
            if (homeItems == null)
                return;

            var data = ee.Data;
            if (!data.GetDataPresent(typeof(Table.DataTableS)))
                return;

            Table.DataTableS info = data.GetData(typeof(Table.DataTableS)) as Table.DataTableS;
            if (info == null)
                return;

            Table.DataTableS search = homeItems.Cast<Table.DataTableS>().FirstOrDefault(f => f.Id == info.Id);
            if (listViewExtGroup.SelectedItem == null && search != null)//如果是在主页状态下拖动，就等于是移除。
                homeItems.Remove(search);
            else if (listViewExtGroup.SelectedItem != null && search == null)
                homeItems.Add(info);

            StringBuilder sb = new StringBuilder();
            foreach (Table.DataTableS ps in homeItems)
                sb.Append(ps.Id + ",");

            sb.Remove(sb.Length - 1, 1);
            DoubleHConfig.AppConfig.PosCustomProductS = sb.ToString();
            ConfigSerializer.SaveConfig(DoubleHConfig.AppConfig, FCNS.Data.DbDefine.appConfigFile);
        }
        #endregion
        #endregion

    }
}
