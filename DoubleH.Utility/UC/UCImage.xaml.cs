﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoubleH.Utility.IO;
using System.IO;

namespace DoubleH.Utility.UC
{
    /// <summary>
    /// UCImage.xaml 的交互逻辑
    /// </summary>
    public partial class UCImage : UserControl
    {
        public UCImage()
        {
            InitializeComponent();

            InitEvent();
        }

        private void InitEvent()
        {
            buttonImage.Click += (ss, ee) => ImageFile = FileTools.GetFile(true, ".jpg", ".bmp", ".png", ".gif");
            image1.MouseRightButtonUp += (ss, ee) => RemoveImg(ee);
        }

        private void RemoveImg(MouseButtonEventArgs ee)
        {
            DoubleH.Utility.Net.NetUtility.RemoveFile(imageFile);
            ImageFile = string.Empty;
        }
        /// <summary>
        /// 更新上传图片文件到服务器
        /// </summary>
        public void UpdateImage(string remoteDirectoryName)
        {
            if (!File.Exists(imageFile))
                return;

            //这里要发文件给服务器
            DoubleH.Utility.Net.NetUtility.SaveFile(imageFile, FCNS.Data.Table.SysConfig.SysConfigParams.Id + "\\" + remoteDirectoryName, UploadFileName);
        }

        string imageFile = string.Empty;
        public string ImageFile
        {
            get
            {
                return imageFile;
            }
            set
            {
                imageFile = value;

                if (string.IsNullOrEmpty(value))
                    image1.Source = null;
                else
                {
                    image1.Source = new BitmapImage(new Uri(value));
                    uploadFileName = DateTime.Now.ToString(FCNS.Data.DbDefine.dateTimeFormat) + System.IO.Path.GetFileName(value);
                }
            }
        }

        string uploadFileName = string.Empty;
        /// <summary>
        /// 保存到数据库中的文件名
        /// </summary>
        public string UploadFileName
        {
            get
            {
                return uploadFileName;
            }
        }
    }
}