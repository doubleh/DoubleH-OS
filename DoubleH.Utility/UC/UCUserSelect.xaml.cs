﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;

namespace DoubleH.Utility.UC
{
    /// <summary>
    /// UCuserSelect.xaml 的交互逻辑
    /// </summary>
    public partial class UCUserSelect : ComboBox
    {
        public UCUserSelect()
        {
            InitializeComponent();

            this.DisplayMemberPath = "Name";
            this.SelectedValuePath = "Id";
        }

        public void Init(params Table.UserS.EnumFlag[] flag)
        { 
            this.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, flag);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="canNull">true 则包含 Null 项</param>
        /// <param name="flag"></param>
        public void Init(bool canNull,Int64 corSId)
        {
            ObservableCollection<Table.UserS> all = Table.UserS.GetList(Table.UserS.EnumEnable.启用,Table.UserS.EnumFlag.客商);
            if (canNull)
                all.Insert(0, new Table.UserS() { Name = "无" ,CorSId=corSId});

            this.ItemsSource = all.Where(f=>f.CorSId==corSId);
        }
        /// <summary>
        /// 初始化指定客商的联系人列表
        /// </summary>
        /// <param name="corSId"></param>
        public void Init(Int64 corSId, params Table.UserS.EnumEnable[] enable)
        {
            this.ItemsSource = Table.UserS.GetListForCorS(corSId, enable);
        }

        public Table.UserS SelectedObject
        {
            get
            {
                Table.UserS us = SelectedItem as Table.UserS;
                if (us == null || us.Id == -1)
                    return null;
                else
                    return us;
            }
            set
            {
                SelectedItem = value;
            }
        }

        public Int64 SelectedObjectId
        {
            get
            {
                if (SelectedObject == null)
                    return -1;
                else
                    return SelectedObject.Id;
            }
            set
            {
                SelectedValue = value;
            }
        }

        public string SelectedObjectPhone
        {
            get
            {
                if (SelectedObject == null)
                    return string.Empty;
                else
                    return SelectedObject.Phone;
            }
        }
    }
}
