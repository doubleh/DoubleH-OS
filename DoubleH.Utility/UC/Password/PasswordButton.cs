﻿using System.Windows;
using System.ComponentModel;
using System.Windows.Controls;

namespace DoubleH.Utility.UC.Password
{
    [DefaultEvent("CheckIn")]
    public class PasswordButton : ContentControl
    {
        public static readonly DependencyProperty IsCheckedProperty;
        public static readonly DependencyProperty IsSwitchOnProperty;
        public static readonly DependencyProperty PasswordProperty;
        public static readonly RoutedEvent CheckInEvent;

        [Category("Behavior")]
        public event RoutedEventHandler CheckIn
        {
            add
            {
                base.AddHandler(CheckInEvent, value);
            }
            remove
            {
                base.RemoveHandler(CheckInEvent, value);
            }
        }
        public string Password
        {
            get
            {
                return (string)base.GetValue(PasswordProperty);
            }
            set
            {
                base.SetValue(PasswordProperty, value);
            }
        }
        public bool IsChecked
        {
            get
            {
                return (bool)base.GetValue(IsCheckedProperty);
            }
            set
            {
                base.SetValue(IsCheckedProperty, value);
            }
        }
        public bool IsSwitchOn
        {
            get
            {
                return (bool)base.GetValue(IsSwitchOnProperty);
            }
            set
            {
                base.SetValue(IsSwitchOnProperty, value);
            }
        }

        static PasswordButton()
        {
            CheckInEvent = EventManager.RegisterRoutedEvent("CheckIn", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PasswordButton));
            IsCheckedProperty = DependencyProperty.Register("IsChecked", typeof(bool), typeof(PasswordButton), new FrameworkPropertyMetadata(false));
            IsSwitchOnProperty = DependencyProperty.Register("IsSwitchOn", typeof(bool), typeof(PasswordButton), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.None, new PropertyChangedCallback(OnIsSwitchOnChange)));
            PasswordProperty = DependencyProperty.Register("Password", typeof(string), typeof(PasswordButton), new FrameworkPropertyMetadata(string.Empty));
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(PasswordButton), new FrameworkPropertyMetadata(typeof(PasswordButton)));


        }

        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            if (this.IsSwitchOn)
            {
                this.IsChecked = true;
                this.OnCheckIn(new RoutedEventArgs(CheckInEvent));
            }
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeftButtonDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            this.IsSwitchOn = true;
            this.IsChecked = true;
            this.OnCheckIn(new RoutedEventArgs(CheckInEvent));
            base.OnMouseLeftButtonDown(e);
        }

        protected virtual void OnCheckIn(RoutedEventArgs e)
        {
            base.RaiseEvent(e);
        }


        private static void OnIsSwitchOnChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordButton element = (PasswordButton)d;
            bool oldValue = (bool)e.OldValue;
            bool newValue = (bool)e.NewValue;
            if (newValue == false)
            {
                element.IsChecked = false;
            }

        }

    }

}
