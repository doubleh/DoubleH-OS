﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Collections;
using System.ComponentModel;

namespace DoubleH.Utility.UC.PagePanel
{
    public interface IPagePanel
    {
        object SelectedItem { get; }

        IList SelectedItems { get; }

        object SelectedGroupItem { get; }

        Brush BackgroundBrush { set; }

        Brush ForegroundBrush { set; }

        void InitHomeData(IList obj);

        void InitData(IList<FCNS.Data.Table.GroupS> groups, IList items);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userUIparams"></param>
        /// <param name="bodyTempleteName">次序:头部模板,中间模板</param>
        void Init(UserUIparams userUIparams, params PagePanel.PageListView.EnumModeTemplete[] templeteName);

        event Utility.UC.UCPagePanel.dDoubleClickItem ItemDoubleClick;

        event Utility.UC.UCPagePanel.dClickItem ItemClick;

        event Utility.UC.UCPagePanel.dHyperlinkClickItem HyperlinkClick;

        event Utility.UC.UCPagePanel.dScrollMove ScrollMove;
    }
}