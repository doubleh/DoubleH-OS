﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using DoubleH.Utility;
using System.Collections;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using Table = FCNS.Data.Table;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Markup;
using DoubleH.Utility.Configuration;
using System.Reflection;

namespace DoubleH.Utility.UC
{
    /// <summary>
    /// UCPagePanel.xaml 的交互逻辑
    /// </summary>
    public partial class UCPagePanel : UserControl
    {
        public UCPagePanel()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitEvent();
        }

        public enum EnumMode
        {
            列表 = 0,
            缩略图
        }

        public delegate void dChangedMode(EnumMode oldMode, EnumMode newMode);
        public event dChangedMode ModeChanged;
        public delegate void dDoubleClickItem(object item);
        public event dDoubleClickItem ItemDoubleClick;
        public delegate void dClickItem(DataGridCellInfo cell);
        public event dClickItem ItemClick;
        public delegate void dHyperlinkClickItem(object selectedItem);
        public event dHyperlinkClickItem HyperlinkClick;
        public delegate void dScrollMove(object sender, System.Windows.Input.ExecutedRoutedEventArgs e);
        public event dScrollMove ScrollMove;
        //public delegate void dMouseRightClick();
        //public event dMouseRightClick MouseRightClick;

        PagePanel.IPagePanel pagePanel = null;
        IList<FCNS.Data.Table.GroupS> groups = null;
        UserUIparams userUIparams = null;
        List<MenuItem> pluginMenuItem = new List<MenuItem>();
        EnumMode lastMode = EnumMode.列表;

        #region 属性
        public DataGridExt DataGridObj
        {
            get
            {
                PagePanel.PageDataGrid pdg = pagePanel as PagePanel.PageDataGrid;
                if (pdg == null)
                    return null;
                else
                    return pdg.dataGridExt1;
            }
        }
        /// <summary>
        /// 禁用列表
        /// </summary>
        public bool HideLieBiao
        {
            set { imageLieBiao.IsEnabled = false; }
        }

        IList homeData = null;
        /// <summary>
        /// 缩略图模式下主页自定义的内容
        /// </summary>
        public IList HomeData
        {
            get { return homeData; }
            set
            {
                homeData = value;
                LoadHomeData(value);
            }
        }

        public object SelectedItem
        {
            get
            {
                if (pagePanel == null)
                    return null;

                return pagePanel.SelectedItem;
            }
        }

        public object SelectedGroupItem
        {
            get
            {
                if (pagePanel == null)
                    return null;

                return pagePanel.SelectedGroupItem;
            }
        }

        public IList SelectedItems
        {
            get
            {
                if (pagePanel == null)
                    return null;
                
                return pagePanel.SelectedItems;
            }
        }

        EnumMode mode = EnumMode.列表;
        public EnumMode Mode
        {
            get { return mode; }
            set
            {
                lastMode = mode;
                mode = value;
                InitUI();
                pagePanel.InitData(groups, items);
            }
        }

        PagePanel.PageListView.EnumModeTemplete listViewGroupTempleteName = PagePanel.PageListView.EnumModeTemplete.HeaderItemTemplateHaveImgAndText;
        /// <summary>
        /// 缩略图模式下顶部分类的模板名称
        /// </summary>
        public PagePanel.PageListView.EnumModeTemplete ListViewGroupTempleteName
        {
            get { return listViewGroupTempleteName; }
            set
            {
                listViewGroupTempleteName = value;
            }
        }

        PagePanel.PageListView.EnumModeTemplete listViewBodyTempleteName = PagePanel.PageListView.EnumModeTemplete.BodyItemTemplateHaveImgAndText;
        /// <summary>
        /// 缩略图模式下内容的模板名称
        /// </summary>
        public PagePanel.PageListView.EnumModeTemplete ListViewBodyTempleteName
        {
            get { return listViewBodyTempleteName; }
            set
            {
                listViewBodyTempleteName = value;
                if (userUIparams.Mode != EnumMode.缩略图)
                    return;

                InitUI();
                pagePanel.InitData(groups, items);
            }
        }

        IList items = null;
        public IList Items
        {
            get { return items; }
            //set
            //{
            //    items = value;
            //    pagePanel.InitData(items);
            //}
        }

        public Frame Frame
        {
            get { return frame1; }
        }
        #endregion

        public void Init(UserUIparams userUIparams, PagePanel.PageListView.EnumModeTemplete bodyTempleteName = PagePanel.PageListView.EnumModeTemplete.Null)
        {
            //只需要初始化一次的放这里
            this.userUIparams = userUIparams;
            this.mode = userUIparams.Mode;//必须的
            lastMode = userUIparams.Mode;
            listViewBodyTempleteName = bodyTempleteName;
            if (bodyTempleteName == PagePanel.PageListView.EnumModeTemplete.Null)
                imageSuoLueTu.Visibility = Visibility.Collapsed;
            else
            {
                imageSuoLueTu.Visibility = Visibility.Visible;
                this.listViewBodyTempleteName = bodyTempleteName;
            }

            InitUI();
            frame1.Navigate(pagePanel);
        }
        /// <summary>
        /// 仅限插件调用
        /// </summary>
        /// <param name="menu"></param>
        public void AddContextMenuItemForPlugin(MenuItem menu)
        {
            pluginMenuItem.Add(menu);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groups">如果不为空,以默认模板开启缩略图,除非在 Init() 中指定模板.</param>
        /// <param name="items"></param>
        public void LoadData(IList<FCNS.Data.Table.GroupS> groups, IList items)
        {
            if (pagePanel == null)
                return;

            this.groups = groups;
            if (groups != null)
            {
                imageLieBiao.Visibility = Visibility.Visible;
                if (listViewGroupTempleteName == PagePanel.PageListView.EnumModeTemplete.Null || listViewBodyTempleteName == PagePanel.PageListView.EnumModeTemplete.Null)
                {
                    imageSuoLueTu.Visibility = Visibility.Visible;
                    listViewGroupTempleteName = PagePanel.PageListView.EnumModeTemplete.HeaderItemTemplateHaveImgAndText;
                    listViewBodyTempleteName = PagePanel.PageListView.EnumModeTemplete.BodyItemTemplateHaveImgAndText;
                    pagePanel.Init(userUIparams, listViewGroupTempleteName, listViewBodyTempleteName);
                }
            }
            this.items = items;
            NormalUtility.SortColumn(items, userUIparams.SortName, userUIparams.SortDirection);
            pagePanel.InitData(groups, items);
        }

        /// <summary>
        /// 返回上一个视图的显示状态。
        /// </summary>
        public void ReturnLastViewState()
        {
            this.Mode = lastMode;
        }

        private void InitEvent()
        {
            imageLieBiao.MouseUp += (ss, ee) => ChangeMode(EnumMode.列表);
            imageSuoLueTu.MouseUp += (ss, ee) => ChangeMode(EnumMode.缩略图);
            this.ContextMenuOpening += (ss, ee) => ConfigContextMenu();
        }

        private void ConfigContextMenu()
        {
            if (pluginMenuItem.Count == 0)
                return;

            if (this.ContextMenu.Items.Contains(pluginMenuItem[0]))
                return;

            foreach (MenuItem mi in pluginMenuItem)
                this.ContextMenu.Items.Add(mi);
        }

        private void LoadHomeData(IList homeData)
        {
            if (pagePanel == null)
                return;
            if (userUIparams.Mode != EnumMode.缩略图)
                return;

            pagePanel.InitHomeData(homeData);
        }

        private void ChangeMode(EnumMode newMode)
        {
            //MessageBox.Show((items == null).ToString() + "  " + mode.ToString() + "  " + itemBinding.UserParams.Mode.ToString() + "  " + newMode.ToString());
            if (items == null || userUIparams.Mode == newMode)
                return;

            Mode = newMode;

            if (ModeChanged != null)
                ModeChanged(userUIparams.Mode, newMode);
        }

        private void InitUI()
        {
            switch (mode)
            {
                case EnumMode.列表:
                    if (!(pagePanel is PagePanel.PageDataGrid))
                    {
                        pagePanel = new PagePanel.PageDataGrid();
                        pagePanel.ItemDoubleClick += new dDoubleClickItem(pagePanel_ItemDoubleClick);
                        pagePanel.ItemClick += new dClickItem(pagePanel_ItemClick);
                        pagePanel.ScrollMove += new dScrollMove(pagePanel_ScrollMove);
                        pagePanel.HyperlinkClick += new dHyperlinkClickItem(pagePanel_HyperlinkClick);

                        pagePanel.BackgroundBrush = Background;
                        pagePanel.ForegroundBrush = Foreground;
                    }
                    frame1.Navigate(pagePanel);
                    pagePanel.Init(userUIparams, listViewBodyTempleteName);
                    break;

                case EnumMode.缩略图:
                    if (!(pagePanel is PagePanel.PageListView))
                    {
                        pagePanel = new PagePanel.PageListView();
                        pagePanel.ItemDoubleClick += new dDoubleClickItem(pagePanel_ItemDoubleClick);
                        pagePanel.ScrollMove += new dScrollMove(pagePanel_ScrollMove);

                        pagePanel.BackgroundBrush = Background;
                        pagePanel.ForegroundBrush = Foreground;
                    }
                    frame1.Navigate(pagePanel);
                    pagePanel.Init(userUIparams, listViewGroupTempleteName, listViewBodyTempleteName);
                    break;
            }
        }

        private void pagePanel_HyperlinkClick(object cell)
        {
            if (HyperlinkClick != null)
                HyperlinkClick(cell);
        }

        private void pagePanel_ScrollMove(object sender, ExecutedRoutedEventArgs e)
        {
            if (ScrollMove != null)
                ScrollMove(sender, e);
        }

        private void pagePanel_ItemClick(DataGridCellInfo cell)
        {
            if (ItemClick != null)
                ItemClick(cell);
        }

        private void pagePanel_ItemDoubleClick(object item)
        {
            if (ItemDoubleClick != null)
                ItemDoubleClick(item);
        }
    }
}