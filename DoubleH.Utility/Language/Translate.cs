﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml;
using System.Windows.Controls;
using System.Windows;
using System.Diagnostics;

namespace DoubleH.Utility.Language
{
   public  class Translate
    {
       static Dictionary<string, string> lang = new Dictionary<string, string>();
       public static Dictionary<string, string> LangString
       {
           get { return lang; }
       }

       /// <summary>
       /// 翻译窗体和其控件
       /// </summary>
       /// <param name="window"></param>
       public static void SetWindowLang(Window window)
       {
           //Debug.Assert(window != null);
           //window.Title = GetLangString(window.Title);
           //while(window.
       }

       public static void SetControlText( ContentControl control)
       {
           Debug.Assert(control != null);

           control.Content = GetLangString(control.ToString());
       }
       /// <summary>
       /// 加载语言包文件
       /// </summary>
       /// <param name="file"></param>
       public static void LoadLang(string file)
       {
           if (!File.Exists(file))
               return;

           lang.Clear();
           using (XmlReader reader = XmlReader.Create(file))
           {
               while (reader.Read())
                   if (reader.NodeType == XmlNodeType.Element)
                       lang[reader.Name] = reader.Value;

               reader.Close();
           }
           MessageWindow.Show(lang.Count.ToString());
       }

       public static string GetLangString(string text)
       {
           if (lang.Keys.Contains(text))
               return lang[text];
           else
               return text;
       }
    }
}
