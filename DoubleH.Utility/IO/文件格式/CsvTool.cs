﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Data;
using System.Diagnostics;

namespace DoubleH.Utility.IO
{
    public class CsvTool : IFileIO
    {
        public const string ExtName = ".csv";
        private string fileName = null; //文件名
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public DataTable ImportFileToDataTable()
        {
            if (string.IsNullOrEmpty(fileName))
                return null;

            DataTable data = new DataTable();
            try
            {
                StreamReader reader = new StreamReader(fileName, Encoding.UTF8);
                string line = reader.ReadLine();
                if (string.IsNullOrEmpty(line))
                    return null;

                string[] columns = line.Split(',');
                for (int i = 0; i < columns.Length; i++)
                {
                    DataColumn column = new DataColumn(columns[i]);
                    data.Columns.Add(column);
                }

                line = reader.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    DataRow dataRow = data.NewRow();
                    columns = line.Split(',');
                    for (int i = 0; i < columns.Length; i++)
                        dataRow[i] = columns[i];

                    data.Rows.Add(dataRow);
                    line = reader.ReadLine();
                }
                reader.Close();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return null;
            }
        }

        public void ExportData(DataTable dataTable)
        {
        }

        public void ExportDataGridToFile(UC.DataGridExt grid)
        {
            StringBuilder sb = new StringBuilder();
            int columnLenght = grid.Columns.Count;
            //先获取标题
            for (int j = 0; j < columnLenght; j++)
                sb.Append((grid.Columns[j].Header == null ? "" : grid.Columns[j].Header.ToString()) + ",");

            sb.Remove(sb.Length - 1, 1);
            sb.Append("\r\n");
            //内容
            for (int i = 0; i < grid.Items.Count; i++)
            {
                for (int j = 0; j < columnLenght;j++)
                {
                     System.Windows.Controls.DataGridCell cell= grid.GetCell(i, j);
                    if(cell==null)
                        continue;

                    System.Windows.Controls.TextBlock tb =cell.Content as System.Windows.Controls.TextBlock;
                    sb.Append(tb == null ? "," : tb.Text + ",");
                }

                sb.Remove(sb.Length - 1, 1);
                sb.Append("\r\n");
            }

            System.IO.File.WriteAllText(fileName, sb.ToString(), Encoding.UTF8);
        }
    }
}