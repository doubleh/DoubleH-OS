﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Data;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace DoubleH.Utility.IO
{
    /// <summary>
    /// SqlBackup.xaml 的交互逻辑
    /// </summary>
    public partial class SqlRestore : Window
    {
        string[] tableNames=new string[]{"UserS","UniqueS","SysConfig","GroupS","CorS","PosS","SalesOrderS","AfterSaleServiceS",
        "ProductSInRepairS","ProductSInWeiBaoS","RepairS","WeiBaoS","CarDriveS","CarHealthS","CarOilS","CarS","StoreS","StoreOrderS",
        "ProductSIO","ProductS","ProductSInStoreS","AdjustAveragePriceS","PurchaseOrderS","PayS","ShopInfoS",
"ProductSInCheckStoreS","CheckStoreS","PosOrderS"};
       
        string folder = DbDefine.baseDir + "backup\\";

        public SqlRestore()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
        }

        private void InitEvent()
        {
            buttonBrowser.Click += (ss, ee) =>
            {
                using (System.Windows.Forms.OpenFileDialog ofd=new System.Windows.Forms.OpenFileDialog())
                {
                    ofd.Filter = "(*.xml)|*.xml";
                    ofd.InitialDirectory = folder;
                    if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        textBox1.Text = ofd.FileName;
                }
            };

            buttonStart.Click += (ss, ee) =>
                {
                    if (!File.Exists(textBox1.Text))
                    {
                        MessageWindow.Show("数据文件不存在");
                        return;
                    }
                    DataSet dataset = new DataSet();
                    dataset.ReadXml(textBox1.Text,XmlReadMode.ReadSchema);
                    foreach (DataTable dt in dataset.Tables)
                    {
                        FCNS.Data.SQLdata.ExecuteNonQuery("delete from " + dt.TableName+" where Id>-1");

                        StringBuilder sb=new StringBuilder(" (");
                        foreach (DataColumn dc in dt.Columns)
                            sb.Append(dc.ColumnName + ",");

                        sb.Remove(sb.ToString().Length - 1, 1);
                        sb.Append(") values (");
                        foreach (DataRow row in dt.Rows)
                        {
                            StringBuilder sb2 = new StringBuilder();
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                if (dt.Columns[i].DataType == typeof(string))
                                    sb2.Append("'" + row[i].ToString() + "',");
                                else
                                    sb2.Append(row[i].ToString() + ",");
                            }
                            sb2.Remove(sb2.ToString().Length - 1, 1);
                            sb2.Append(")");
                            FCNS.Data.SQLdata.ExecuteNonQuery("insert into " + dt.TableName + sb.ToString()+sb2.ToString());
                        }
                    }
                    MessageWindow.Show("恢复成功,请重新启动软件.");
                    this.Close();
                    //File.Copy(FCNS.Data.DbDefine.dbFile, filePath + DateTime.Now.ToString(FCNS.Data.DbDefine.dateFormat));
                };
        }
    }
}