﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FCNS.Data.Interface;
using System.Collections;
using System.Diagnostics;

namespace DoubleH.Utility.IO
{
    /// <summary>
    /// ExportWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ExportWindow : Window
    {
        IEnumerable dataGridObject = null;
        string fileName = string.Empty;

        public ExportWindow(IEnumerable obj)
        {
            InitializeComponent();

            dataGridObject = obj;

            InitEvent();
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>
            {
                fileName = FileTools.GetFile(false,".csv");
                if (string.IsNullOrEmpty(fileName.Trim()))
                    return;

                if (radioButton1.IsChecked.Value)
                    Export(GetText());
                else
                {
                    if (radioButton2.IsChecked.Value)
                        Export(GetText2(false));
                    else
                        Export(GetText2(true));
                }
                this.Close();
            };
        }

        private void Export(string text)
        {
            Debug.Assert(dataGridObject != null);
            System.IO.File.WriteAllText(fileName, text, Encoding.UTF8);
            if (MessageWindow.Show("", fileName + " 导出成功,是否打开文件?", System.Windows.MessageBoxButton.YesNo) == System.Windows.MessageBoxResult.Yes)
                System.Diagnostics.Process.Start(fileName);
        }

        private string GetText()
        {
            StringBuilder sb = new StringBuilder();
            if (!dataGridObject.HasItems)
                return string.Empty;

            for (int j = 0; j < dataGridObject.Columns.Count; j++)
                sb.Append(dataGridObject.Columns[j].Header + ",");

            sb.Remove(sb.Length - 1, 1);
            sb.Append("\r\n");
            for (int i = 0; i < dataGridObject.Items.Count; i++)
            {
                var vr = dataGridObject.Items[i];
                Type type = vr.GetType();
                for (int j = 0; j < dataGridObject.Columns.Count; j++)
                {
                    DataGridTextColumn gvc = (DataGridTextColumn)dataGridObject.Columns[j];
                    System.Reflection.PropertyInfo vt = type.GetProperty(((Binding)gvc.Binding).Path.Path);
                    object obj = (vt == null) ? "" : vt.GetValue(vr, null);
                    sb.Append((obj==null)?"":obj.ToString() + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append("\r\n");
            }

            return sb.ToString();
        }

        private string GetText2(bool all)
        {
            StringBuilder sb = new StringBuilder();

            if (!dataGridObject.HasItems)
                return string.Empty;

            List<System.Reflection.PropertyInfo> info =new List<System.Reflection.PropertyInfo>();
            foreach (System.Reflection.PropertyInfo i in dataGridObject.Items[0].GetType().GetProperties())
            {
                if (!all && i.Name.StartsWith("_"))
                    continue;

                info.Add(i);
            }
            for(int i=0;i<info.Count;i++)
                sb.Append(info[i].Name + ",");

            sb.Remove(sb.Length - 1, 1);
            sb.Append("\r\n");

            for (int i = 0; i < dataGridObject.Items.Count; i++)
            {
                object vr = dataGridObject.Items[i];
                for (int j = 0; j < info.Count; j++)
                {
                    object obj = info[j].GetValue(vr, null);
                    sb.Append((obj ?? "") + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append("\r\n");
            }

            return sb.ToString();
        }
    }
}