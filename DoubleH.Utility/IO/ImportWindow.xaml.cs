﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace DoubleH.Utility.IO
{
    /// <summary>
    /// ImportWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ImportWindow : Window
    {
        public ImportWindow()
        {
            InitializeComponent();

            InitEvent();
        }

        //FCNS.Data.Interface.IImport obj = null;
        //public void Init(FCNS.Data.Interface.IImport obj)
        //{
        //    if (obj == null)
        //        return;

        //    this.obj = obj;
        //    buttonFile.IsEnabled = true;
        //    //dataGridExtObj.InitColumns(new DataGridColumnBinding(
        //}

        private void InitEvent()
        {
            buttonFile.Click += (ss, ee) => SelectFile();
            listBoxTable.MouseDoubleClick += (ss, ee) => FillData();
            buttonRun.Click += (ss, ee) => SaveData(); 
        }

        private void SelectFile()
        {
            textBlockPath.Text = IO.FileTools.GetFile(true,".xls", ".xlsx", ".txt");
            if (string.IsNullOrEmpty(textBlockPath.Text))
                return;

            listBoxTable.Items.Clear();
            wk = null;
            dataGridExtObj.Columns.Clear();
        
            switch (textBlockPath.Text.Substring(textBlockPath.Text.Length - 4, 4))
            {
                case ".xls":
                    OpenXls();
                    break;
            }
        }

        HSSFWorkbook wk = null;
        private void OpenXls()
        {
            StringBuilder sbr = new StringBuilder();
            using (FileStream fs = File.OpenRead(textBlockPath.Text))   //打开myxls.xls文件
            {

                wk = new HSSFWorkbook(fs);   //把xls文件中的数据写入wk中
                for (int i = 0; i < wk.NumberOfSheets; i++)  //NumberOfSheets是myxls.xls中总共的表数
                    listBoxTable.Items.Add(wk.GetSheetName(i));

                fs.Dispose();
            }
        }

        private void FillData()
        {
            if (wk == null || listBoxTable.SelectedItem == null)
                return;

            ISheet sheet = wk.GetSheetAt(listBoxTable.SelectedIndex);   //读取当前表数据
            if (sheet.LastRowNum <2)
                return;

            //标题
            IRow header = sheet.GetRow(0);
            IRow binding = sheet.GetRow(1);
            for (int h = 0; h < header.LastCellNum; h++)
            {
                ICell cell = header.GetCell(h);
                if (cell == null)
                    continue;

                DataGridTextColumn gc = new DataGridTextColumn();
                gc.Binding = new Binding("[" + binding.GetCell(h).ToString()+"]");
                gc.IsReadOnly = true;
                gc.Header = cell.ToString();
                dataGridExtObj.Columns.Add(gc);
            }
            //内容
            for (int r = 1; r < sheet.LastRowNum; r++)
            {
                IRow row = sheet.GetRow(r);  //读取当前行数据
                if (row == null)
                    continue;

                Dictionary<string, string> item = new Dictionary<string, string>();
                for (int c = 0; c <= row.LastCellNum; c++)  //LastCellNum 是当前行的总列数
                {
                    ICell cell = row.GetCell(c);  //当前表格
                    if (cell != null)
                        item.Add(binding.GetCell(c).ToString(), cell.ToString());
                }
                dataGridExtObj.Items.Add(item);
            }
        }

        private void SaveData()
        {
            //if (obj == null)
            //    return;

        }
    }
}