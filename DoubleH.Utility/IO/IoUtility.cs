﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FSLib.IPMessager;
using FSLib.IPMessager.Entity;
using System.IO;
using DoubleH.Utility.Net;
using System.Diagnostics;

namespace DoubleH.Utility.IO
{
    public class IoUtility
    {
        public static string GetFile(string fileName, string directoryName)
        {
            CheckDirectory(directoryName);

            if (NetUtility.LanClient == null)
                return FromLocal(fileName, directoryName);
            else
                return FromServer(fileName, directoryName);
        }

        public static bool SaveFile(string fullFilePath, string directoryName, string fileName)
        {
            CheckDirectory(directoryName);
            //NetUtility.LanClient.FileTaskManager.SendTaskAdded += new EventHandler<FSLib.IPMessager.Core.FileTaskEventArgs>(FileTaskManager_SendTaskAdded);
            //NetUtility.LanClient.FileTaskManager.SendItemStart += new EventHandler<FSLib.IPMessager.Core.FileTaskEventArgs>(FileTaskManager_SendItemStart);
            if (NetUtility.LanClient == null)
                return ToLocal(fullFilePath, directoryName, fileName);
            else
                return ToServer(fullFilePath, directoryName, fileName);
        }

        //static void FileTaskManager_SendItemStart(object sender, FSLib.IPMessager.Core.FileTaskEventArgs e)
        //{
        //    MessageWindow.Show("vvv");
        //}

        //static void FileTaskManager_SendTaskAdded(object sender, FSLib.IPMessager.Core.FileTaskEventArgs e)
        //{
        //    MessageWindow.Show("az");
        //}

        private static string FromServer(string fileName, string directoryName)
        {
            return string.Empty;
        }

        private static bool ToServer(string fullFilePath, string directoryName, string fileName)
        {
            Debug.Assert(NetUtility.ServerHost!= null);

            FileTaskItem file = new FileTaskItem();
            NetUtility.LanClient.PerformSendFile(new FileTaskItem[] { file }, string.Empty, false, false, NetUtility.ServerHost);
            return true;
        }

        private static string FromLocal(string fileName, string directoryName)
        {
            string file = FCNS.Data.DbDefine.baseDir + directoryName + "\\" + fileName;
            if (File.Exists(file))
                return file;
            else
                return string.Empty;
        }

        private static bool ToLocal(string fullFilePath, string directoryName, string fileName)
        {
            if (File.Exists(fullFilePath))
            {
                try
                {
                    File.Copy(fullFilePath, FCNS.Data.DbDefine.baseDir + directoryName + "\\" + fileName, true);
                    return true;
                }
                catch (Exception ee)
                {
                    MessageWindow.Show(ee.Message);
                }
            }

            return false;
        }

        private static void CheckDirectory(string directoryName)
        {
            try
            {
                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);
            }
            catch (Exception ee)
            {
                MessageWindow.Show(ee.Message);
            }
        }
    }
}