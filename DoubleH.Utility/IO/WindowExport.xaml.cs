﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoubleH.Utility.IO
{
    /// <summary>
    /// WindowExport.xaml 的交互逻辑
    /// </summary>
    public partial class WindowExport : Window
    {
        public WindowExport()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void Init()
        {
        }

        private void InitVar()
        {
        }

        private void InitEvent()
        {
        } 
    }
}
