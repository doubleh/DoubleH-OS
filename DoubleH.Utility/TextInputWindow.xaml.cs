﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoubleH.Utility
{
    /// <summary>
    /// TextInput.xaml 的交互逻辑
    /// </summary>
    public partial class TextInputWindow : Window
    {
        public TextInputWindow()
        {
            InitializeComponent();

            InitEvent();
        }

        bool isPassword = false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="text">输入框默认文本</param>
        /// <param name="isPassword">是否以密码*号的形式显示</param>
        public void Init(string title, string text, bool isPassword = false)
        {
            this.Title = title;
            this.isPassword = isPassword;

            if (isPassword)
            {
                passwordBox1.Password = text;
                passwordBox1.Focus();
            }
            else
            {
                textBox1.Text = text;
                textBox1.Focus();
            }
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) => { GetText = (isPassword ? passwordBox1.Password : textBox1.Text); this.Close(); };
        }

        public string GetText
        {
            get;
            set;
        }
    }
}