﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.IO;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace DoubleH.Utility
{
    /// <summary>
    /// WindowEmail.xaml 的交互逻辑
    /// </summary>
    public partial class WindowEmail : Window
    {
        public WindowEmail()
        {
            InitializeComponent();

            InitEvent();
            ClearHtml();
        }

        string bodyText = string.Empty;
        public void Init(Int64 corS, string bodyHtml)
        {
            bodyText = bodyHtml;
            Table.CorS cs = Table.CorS.GetObject(corS);
            if (cs != null)
            {
                checkComboBoxTarget.DisplayMemberPath = "Name";
                checkComboBoxTarget.SelectedMemberPath = "Email";
                checkComboBoxTarget.ValueMemberPath = "Email";

                checkComboBoxTarget.ItemsSource = cs.AllContact;
            }
            emailTitle.Focus();
        }

        private void ClearHtml()
        {
            foreach (string str in Directory.GetFiles(DbDefine.baseDir, "*.html"))
            {
                try { File.Delete(str); }
                catch { }
            }
        }

        private void InitEvent()
        {
            emailSend.Click += (ss, ee) => SendEmail();
        }

        private void SendEmail()
        {
            if (string.IsNullOrEmpty(checkComboBoxTarget.Text) || string.IsNullOrEmpty(textBoxEmail.Text))
            {
                MessageWindow.Show("收件人地址不能为空");
                return;
            }
            string[] address = checkComboBoxTarget.SelectedValue.Split(',');
            if (!string.IsNullOrEmpty(textBoxEmail.Text))
                ThreadPool.QueueUserWorkItem(Send, new string[]{textBoxEmail.Text, emailTitle.Text});
            else
                foreach (string str in address)
                    ThreadPool.QueueUserWorkItem(Send, new string[]{str, emailTitle.Text});

            this.Close();
        }

        private void Send(object targetEmail)
        {
                string name=((string[])targetEmail)[0];
                System.IO.File.WriteAllText(name+".html", bodyText);
                try
                {
                    MailMessage message = new MailMessage(DoubleHConfig.UserConfig.EmailName, name);
                    message.IsBodyHtml = true;
                    message.Subject = ((string[])targetEmail)[1];
                    message.Body = "请下载附件查看";
                    message.Attachments.Add(new Attachment(name + ".html"));
                    SmtpClient sc = new SmtpClient(DoubleHConfig.UserConfig.EmailSmtp, DoubleHConfig.UserConfig.EmailPort);
                    sc.UseDefaultCredentials = false;
                    sc.Credentials = new NetworkCredential(DoubleHConfig.UserConfig.EmailName.Split('@')[0], DoubleHConfig.UserConfig.EmailPwd);
                    sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                    sc.EnableSsl = DoubleHConfig.UserConfig.EmailSsl;
                    sc.Send(message);
                }
                catch 
                {
                }
        }
    }
}