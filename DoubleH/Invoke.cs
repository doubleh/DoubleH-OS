﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Collections;
using DoubleH.Utility.Configuration;

namespace DoubleH
{
    /// <summary>
    /// WorkArea.xaml 的交互逻辑
    /// </summary>
    public partial class WorkArea : Window
    {
        //delegate void BindDataAsync();//主线程委托
        //delegate object ControlValue(ContentControl control);//次线程委托,用于读取主线程控件的值

        private void BeginBindData()
        {
            if (selectedMenu == null)
                return;

            IList objItems = null;
            switch (selectedMenu.TableText)
            {
                #region 促销管理
                case DataTableText.优惠券: objItems = Table.CouponsS.GetList(null, null, Table.CouponsS.EnumEnable.评估); break;
                case DataTableText.促销方案: objItems = Table.PromotionS.GetList(null, null, Table.PromotionS.EnumEnable.评估); break;
                #endregion

                #region 消息中心
                //case DataTableText.我的消息: objItems = GetMessageS(); break;
                case DataTableText.待办事项: ObservableCollection<Table.ScheduleS> ss = Table.ScheduleS.GetList(Table.UserS.LoginUser.Id);
                    foreach (Table.ScheduleS f in ss)
                    {
                        switch ((int)f.Flag)
                        {
                            case 0: f._Flag = DoubleHConfig.UserConfig.ScheduleImage0; break;
                            case 1: f._Flag = DoubleHConfig.UserConfig.ScheduleImage1; break;
                            case 2: f._Flag = DoubleHConfig.UserConfig.ScheduleImage2; break;
                            case 3: f._Flag = DoubleHConfig.UserConfig.ScheduleImage3; break;
                        }
                    }
                    objItems = ss;
                    break;
                #endregion

                #region 公寓租赁
                case DataTableText.公寓:
                    objItems = Table.RoomS.GetList((Int64)selectedMenu.Tag, null, Table.RoomS.EnumEnable.空置, Table.RoomS.EnumEnable.已租);
                    break;
                case DataTableText.楼层:
                    string[] handf = selectedMenu.Tag.ToString().Split('.');
                    objItems = Table.RoomS.GetList(Int64.Parse(handf[0]), Int64.Parse(handf[1]), Table.RoomS.EnumEnable.空置, Table.RoomS.EnumEnable.已租);
                    break;
                #endregion

                #region ITdb
                case DataTableText.硬件: objItems = Table.ItDbS.GetList(Table.ItDbS.EnumFlag.硬件, Table.ItDbS.EnumEnable.激活, Table.ItDbS.EnumEnable.维修); break;
                case DataTableText.软件: objItems = Table.ItDbS.GetList(Table.ItDbS.EnumFlag.软件, Table.ItDbS.EnumEnable.激活, Table.ItDbS.EnumEnable.维修); break;
                #endregion

                #region POS
                case DataTableText.POS机器号: objItems = Table.PosS.GetList(); break;
                case DataTableText.POS禁售商品: objItems = Table.ProductS.GetListWherePosCanNotUsed(); break;
                case DataTableText.连锁门店: objItems = Table.StoreS.GetList(true,Table.StoreS.EnumEnable.启用); break;
                case DataTableText.POS请货单: objItems = Table.PosRequestS.GetList(Table.PosRequestS.EnumFlag.请货单, null, Table.PosRequestS.EnumEnable.审核); break;
                case DataTableText.POS配送单: objItems = Table.PosRequestS.GetList(Table.PosRequestS.EnumFlag.配送单, null, Table.PosRequestS.EnumEnable.评估, Table.PosRequestS.EnumEnable.审核); break;
                #endregion

                #region 项目
                case DataTableText.项目列表: objItems = Table.ProjectS.GetList(Table.ProjectS.EnumEnable.评估, Table.ProjectS.EnumEnable.实施, Table.ProjectS.EnumEnable.验收); break;
                case DataTableText.任务跟踪: objItems = Table.ScheduleInProjectS.GetList(); break;
                case DataTableText.问题反馈: objItems = Table.FeedbackInProjectS.GetList(); break;
                #endregion

                #region 物流
                case DataTableText.采购物流跟踪:
                case DataTableText.仓库物流跟踪:
                case DataTableText.批发物流跟踪: objItems = Table.WuLiuS.GetList((Table.WuLiuS._EnumFlag)Enum.Parse(typeof(Table.WuLiuS._EnumFlag), selectedMenu.TableText.ToString())); break;
                case DataTableText.物流公司: objItems = Table.CorS.GetWuLiuList(); break;
                #endregion

                #region 财务
                case DataTableText.银行存取款: objItems = Table.BankMoneyS.GetList(DateTime.Now.AddMonths(-1), null); break;

                case DataTableText.其它费用收入单:
                case DataTableText.其它费用支出单:
                    objItems = Table.PayS.GetList(DateTime.Now.AddMonths(-1), null, (Table.PayS.EnumFlag)Enum.Parse(typeof(Table.PayS.EnumFlag), selectedMenu.TableText.ToString()), null); break;

                case DataTableText.收款单:
                case DataTableText.付款单: objItems = Table.PayS.GetList(null, null, (Table.PayS.EnumFlag)Enum.Parse(typeof(Table.PayS.EnumFlag), selectedMenu.TableText.ToString()), new int[] { (int)Table.PayS.EnumEnable.评估 }); break;
                case DataTableText.未收取发票: objItems = Table.PayS.GetListAboutInvoice(null, null, Table.PayS.EnumFlag.付款单, false); break;
                case DataTableText.未开具发票: objItems = Table.PayS.GetListAboutInvoice(null, null, Table.PayS.EnumFlag.收款单, false); break;

                case DataTableText.POS记账明细表: objItems = Table.PosOrderS.GetListForNoPay(null, null, null, null); break;
                #endregion

                #region 车辆
                case DataTableText.车辆档案: objItems = Table.CarS.GetList(); break;
                case DataTableText.预订车辆: objItems = Table.CarDriveS.GetList(DateTime.Now.AddMonths(-1), null, Table.CarDriveS.EnumEnable.预订); break;
                case DataTableText.行车记录: objItems = Table.CarDriveS.GetList(DateTime.Now.AddMonths(-1), null, Table.CarDriveS.EnumEnable.登记受理, Table.CarDriveS.EnumEnable.处理完毕); break;
                case DataTableText.油票记录: objItems = Table.CarOilS.GetList(DateTime.Now.AddMonths(-1), null); break;
                case DataTableText.违章处罚: objItems = Table.CarHealthS.GetList(DateTime.Now.AddMonths(-1), null, null, Table.CarHealthS.EnumFlag.违章处罚); break;
                case DataTableText.事故赔偿: objItems = Table.CarHealthS.GetList(DateTime.Now.AddMonths(-1), null, null, Table.CarHealthS.EnumFlag.事故赔偿); break;
                case DataTableText.维修保养: objItems = Table.CarHealthS.GetList(DateTime.Now.AddMonths(-1), null, null, Table.CarHealthS.EnumFlag.维修保养); break;
                case DataTableText.年审记录: objItems = Table.CarHealthS.GetList(DateTime.Now.AddMonths(-1), null, null, Table.CarHealthS.EnumFlag.年审记录); break;
                case DataTableText.投保记录: objItems = Table.CarHealthS.GetList(DateTime.Now.AddMonths(-1), null, null, Table.CarHealthS.EnumFlag.投保记录); break;
                #endregion

                #region 仓库
                case DataTableText.调拨单: objItems = Table.AllocationS.GetList(null, null, Table.AllocationS.EnumEnable.评估); break;
                case DataTableText.库存调价单: objItems = Table.AdjustAveragePriceS.GetList(null, null, null); break;
                case DataTableText.盘点单: objItems = Table.CheckStoreS.GetList(null, null, new int[] { (int)Table.CheckStoreS.EnumEnable.盘点开始 }); break;
                case DataTableText.非采购入库单: objItems = Table.StoreOrderS.GetList(null, null, false, Table.StoreOrderS.EnumFlag.入库单, Table.StoreOrderS.EnumEnable.审核); break;
                case DataTableText.非销售出库单: objItems = Table.StoreOrderS.GetList(null, null, false, Table.StoreOrderS.EnumFlag.出库单, Table.StoreOrderS.EnumEnable.审核); break;
                case DataTableText.仓库单据入库单: objItems = Table.StoreOrderS.GetList(null, null, true, Table.StoreOrderS.EnumFlag.入库单, Table.StoreOrderS.EnumEnable.评估); break;
                case DataTableText.仓库单据出库单: objItems = Table.StoreOrderS.GetList(null, null, true, Table.StoreOrderS.EnumFlag.出库单, Table.StoreOrderS.EnumEnable.评估); break;
                case DataTableText.仓库商品入库单: objItems = Table.ProductSIO.GetList(false, Table.ProductSIO.EnumEnable.评估); break;
                case DataTableText.仓库商品出库单: objItems = Table.ProductSIO.GetList(true, Table.ProductSIO.EnumEnable.评估); break;
                case DataTableText.仓库: objItems = Table.StoreS.GetList(false,Table.StoreS.EnumEnable.启用); break;
                #endregion

                #region 档案
                case DataTableText.仓库商品: objItems = Table.ProductS.GetList(null, new int[] { 0, 1, 2 }, null, false); break;
                case DataTableText.物料清单: objItems = Table.ProductS.GetWuLiaoList(null, null); break;
                case DataTableText.可销售商品: objItems = Table.ProductS.GetList(null, new int[] { 0, 1, 3, 4 }, null, false); break;
                case DataTableText.可销售物料清单: objItems = Table.ProductS.GetWuLiaoList(null, true); break;
                case DataTableText.可采购商品: objItems = Table.ProductS.GetList(null, new int[] { 0, 2 }, null, false); break;
                case DataTableText.可采购物料清单: objItems = Table.ProductS.GetWuLiaoList(null, false); break;

                case DataTableText.供应商: objItems = Table.CorS.GetList(null, new int[] { (int)Table.CorS.EnumEnable.启用 }, new int[] { (int)Table.CorS.EnumFlag.供应商, (int)Table.CorS.EnumFlag.客户和供应商 }); break;
                case DataTableText.客户: objItems = Table.CorS.GetList(null, new int[] { (int)Table.CorS.EnumEnable.启用 }, new int[] { (int)Table.CorS.EnumFlag.客户, (int)Table.CorS.EnumFlag.客户和供应商 }); break;
                case DataTableText.会员资料: objItems = Table.CorS.GetVipList(); break;

                case DataTableText.设备分类:
                case DataTableText.客商分类:
                case DataTableText.商品分类: objItems = Table.GroupS.GetList((Table.GroupS.EnumFlag)Enum.Parse(typeof(Table.GroupS.EnumFlag), selectedMenu.TableText.ToString()), Table.GroupS.EnumEnable.启用); break;

                case DataTableText.标记:
                case DataTableText.会计科目:
                case DataTableText.会员分类:
                case DataTableText.服务级别定义:
                case DataTableText.计量单位:
                case DataTableText.产地:
                case DataTableText.品牌:
                case DataTableText.维修对象:
                case DataTableText.维修类型:
                case DataTableText.支付方式:
                case DataTableText.客户性质:
                case DataTableText.客户提供资料:
                case DataTableText.服务评价:
                case DataTableText.发票定义:
                case DataTableText.任务类型:
                case DataTableText.区域:
                case DataTableText.公寓仪表费用:
                case DataTableText.公寓固定费用:
                case DataTableText.房间类型:
                case DataTableText.货架:
                case DataTableText.行:
                    objItems = Table.UniqueS.GetList((Table.UniqueS.EnumFlag)Enum.Parse(typeof(Table.UniqueS.EnumFlag), selectedMenu.TableText.ToString()), Table.UniqueS.EnumEnable.启用);
                    break;
                #endregion

                #region 采购批发
                case DataTableText.采购订单: objItems = Table.PurchaseOrderS.GetList(null, null, new Table.PurchaseOrderS.EnumFlag[] { Table.PurchaseOrderS.EnumFlag.采购订单 }, new Table.PurchaseOrderS.EnumEnable[] { Table.PurchaseOrderS.EnumEnable.下单, Table.PurchaseOrderS.EnumEnable.预付款, Table.PurchaseOrderS.EnumEnable.审核 }); break;
                //case DataTableText.采购询价单:
                //    obj = Table.PurchaseOrderS.GetList(null, null, new Table.PurchaseOrderS.EnumFlag[] { Table.PurchaseOrderS.EnumFlag.采购订单 }, new Table.PurchaseOrderS.EnumEnable[] { Table.PurchaseOrderS.EnumEnable.询价 });
                //    break;
                case DataTableText.采购退货单: objItems = Table.PurchaseOrderS.GetList(null, null, new Table.PurchaseOrderS.EnumFlag[] { Table.PurchaseOrderS.EnumFlag.采购退货单 }, new Table.PurchaseOrderS.EnumEnable[] { Table.PurchaseOrderS.EnumEnable.下单, Table.PurchaseOrderS.EnumEnable.审核 }); break;
                case DataTableText.赠品入库单: objItems = Table.PurchaseOrderS.GetList(null, null, new Table.PurchaseOrderS.EnumFlag[] { Table.PurchaseOrderS.EnumFlag.赠品入库单 }, new Table.PurchaseOrderS.EnumEnable[] { Table.PurchaseOrderS.EnumEnable.下单, Table.PurchaseOrderS.EnumEnable.审核 }); break;

                case DataTableText.销售订单: objItems = Table.SalesOrderS.GetList(null, null, new Table.SalesOrderS.EnumFlag[] { Table.SalesOrderS.EnumFlag.销售订单 }, new Table.SalesOrderS.EnumEnable[] { Table.SalesOrderS.EnumEnable.下单, Table.SalesOrderS.EnumEnable.预付款, Table.SalesOrderS.EnumEnable.审核 }); break;
                case DataTableText.销售询价单: objItems = Table.SalesOrderS.GetList(null, null, new Table.SalesOrderS.EnumFlag[] { Table.SalesOrderS.EnumFlag.销售询价单 }, null); break;
                case DataTableText.销售退货单: objItems = Table.SalesOrderS.GetList(null, null, new Table.SalesOrderS.EnumFlag[] { Table.SalesOrderS.EnumFlag.销售退货单 }, new Table.SalesOrderS.EnumEnable[] { Table.SalesOrderS.EnumEnable.下单, Table.SalesOrderS.EnumEnable.审核 }); break;
                case DataTableText.赠品出库单: objItems = Table.SalesOrderS.GetList(null, null, new Table.SalesOrderS.EnumFlag[] { Table.SalesOrderS.EnumFlag.赠品出库单 }, new Table.SalesOrderS.EnumEnable[] { Table.SalesOrderS.EnumEnable.下单, Table.SalesOrderS.EnumEnable.审核 }); break;
                #endregion

                #region 售后返修
                case DataTableText.客户报修:
                case DataTableText.售后派单:
                case DataTableText.维护完工:
                case DataTableText.回访反馈: objItems = Table.AfterSaleServiceS.GetList(null, new int[] { Convert.ToInt32(Enum.Parse(typeof(Table.AfterSaleServiceS.EnumEnable), selectedMenu.TableText.ToString())) }); break;

                case DataTableText.外包合同: objItems = Table.WeiBaoS.GetList(Table.WeiBaoS.EnumFlag.外包维保); break;

                case DataTableText.客户坏件送修:
                case DataTableText.客户坏件取回:
                case DataTableText.供应商坏件送修:
                case DataTableText.供应商坏件返回: objItems = Table.RepairS.GetList(null, null, (Table.RepairS.EnumEnable)Enum.Parse(typeof(Table.RepairS.EnumEnable), selectedMenu.TableText.ToString())); break;
                #endregion
            }
            if (Table.SysConfig.SysConfigParams._Temp_uint == 0)
            {
                uCPagePanel1.Dispatcher.Invoke(new Action(() =>
                   {
                       uCPagePanel1.LoadData(GetGroupItems(), objItems);
                   }));
            }
            else
            {
                ArrayList al = new ArrayList(uCPagePanel1.Items);
                al.AddRange(objItems);
                uCPagePanel1.Dispatcher.Invoke(new Action(() =>
                    {
                        uCPagePanel1.LoadData(null, al);
                    }));
            }
        }

        private IList<FCNS.Data.Table.GroupS> GetGroupItems()
        {
            if (Table.SysConfig.SysConfigParams._Temp_uint > 0)
                return null;

            IList<FCNS.Data.Table.GroupS> objGroups = null;
            switch (selectedMenu.TableText)
            {
                case DataTableText.仓库商品:
                case DataTableText.物料清单:
                case DataTableText.可销售商品:
                case DataTableText.可销售物料清单:
                case DataTableText.可采购商品:
                case DataTableText.可采购物料清单:
                    objGroups = Table.GroupS.GetList(Table.GroupS.EnumFlag.商品分类, Table.GroupS.EnumEnable.启用);
                    break;

                case DataTableText.客商分类:
                case DataTableText.供应商:
                case DataTableText.客户:
                case DataTableText.会员资料:
                    objGroups = Table.GroupS.GetList(Table.GroupS.EnumFlag.客商分类, Table.GroupS.EnumEnable.启用);
                    break;

                case DataTableText.设备分类: objGroups = Table.GroupS.GetList(Table.GroupS.EnumFlag.设备分类, Table.GroupS.EnumEnable.启用); break;
                case DataTableText.商品分类: objGroups = Table.GroupS.GetList(Table.GroupS.EnumFlag.商品分类, Table.GroupS.EnumEnable.启用); break;
            }
            return objGroups;
        }

        //private object GetControlValue(ContentControl control)
        //{
        //    return control.Content;
        //}

        private IList GetMessageS()
        {
            if (selectedMenu == null || selectedMenu.TableText != DataTableText.我的消息)//预警循环检测中，如果当前界面不是消息中心就不要刷新数据了。
                return null;

            ObservableCollection<Table.MessageS> allMessageS = Table.MessageS.GetList(Table.UserS.LoginUser.Id);
            ////仓库
            //if (sysConfig.UseStoreS)
            //{
            //    Table.StoreOrderS order = new Table.StoreOrderS(Table.StoreOrderS.EnumFlag.出入库单);
            //    if (Table.UserS.LoginUser.GetAuthority(DataTableText.仓库单据出库单) > Table.UserS.EnumAuthority.无权限)
            //        order.GetList(true, true).ToList().ForEach(f => allMessageS.Add(new Table.MessageS() { _Tag = "StoreS.dll", _Table = f, OrderDateTime = DateTime.Now, Name = f._CorSName + " 超期未出库" }));

            //    if (Table.UserS.LoginUser.GetAuthority(DataTableText.仓库单据入库单) > Table.UserS.EnumAuthority.无权限)
            //        order.GetList(false, true).ToList().ForEach(f => allMessageS.Add(new Table.MessageS() { _Tag = "StoreS.dll", _Table = f, OrderDateTime = DateTime.Now, Name = f._CorSName + " 超期未入库" }));
            //}
            //售后
            if (sysConfig.UseAfterSaleServiceS)
            {
                double hour;
                foreach (Table.AfterSaleServiceS ass in Table.AfterSaleServiceS.GetList(null, null, null, Table.AfterSaleServiceS.EnumEnable.客户报修))
                {
                    //检测维保用户
                    Table.UniqueS us = Table.WeiBaoS.WeiBaoFlag(ass.CorSId);
                    if (us != null)
                    {
                        hour = us.DoubleValue;
                        ass._WeiBaoFlag = us.Name;
                    }
                    else
                        hour = sysConfig.RequestTime;

                    if (ass.BaoXiuDateTime.AddHours(hour) < DateTime.Now)
                        allMessageS.Add(new Table.MessageS() { _Tag = "AfterSaleServiceS.dll", _Table = ass, OrderDateTime = ass.BaoXiuDateTime, Name = ass._CorSName + " 售后超期未派单" });
                }


                if (sysConfig.WeiBaoAlertDay != 0)
                    foreach (Table.WeiBaoS wbs in Table.WeiBaoS.GetListWhereWillEnd(Table.WeiBaoS.EnumFlag.外包维保, sysConfig.WeiBaoAlertDay))
                        allMessageS.Add(new Table.MessageS() { _Tag = "AfterSaleServiceS.dll", _Table = wbs, OrderDateTime = wbs.EndDateTime, Name = wbs._CorSName + " 维保即将到期" });
            }
            //财务
            if (sysConfig.UsePayS)
            {
                Table.PayS order = new Table.PayS();
                order.GetList(true, true).ToList().ForEach(f => allMessageS.Add(new Table.MessageS() { _Tag = "PayS.dll", _Table = f, OrderDateTime = DateTime.Now, Name = f._CorSName + " 超期未付款" }));
                order.GetList(false, true).ToList().ForEach(f => allMessageS.Add(new Table.MessageS() { _Tag = "PayS.dll", _Table = f, OrderDateTime = DateTime.Now, Name = f._CorSName + " 超期未收款" }));
            }

            return allMessageS;
        }
    }
}