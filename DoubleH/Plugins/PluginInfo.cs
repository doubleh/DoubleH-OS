using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace DoubleH.Plugins
{
    /// <summary>
    /// 插件的描述信息，一个实例对应一个插件。
    /// </summary>
    public sealed class PluginInfo 
	{
		private string m_strFilePath;
		private Plugin m_pluginInterface = null;
		private string m_strFileVersion = string.Empty;
		private string m_strName = string.Empty;
		private string m_strDescription = string.Empty;
		private string m_strAuthor = string.Empty;
        private string note = string.Empty;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }

		public string FilePath
		{
			get { return m_strFilePath; }
		}

		public string DisplayFilePath
		{
			get { return  m_strFilePath; }
		}
        /// <summary>
        /// 获取或设置插件实例
        /// </summary>
        public Plugin Interface
        {
            get { return m_pluginInterface; }
            set
            {
                m_pluginInterface = value;

                //if (value != null)
                //{
                //    Func<string, string, bool> func = DoubleH.Utility.IO.FileTools.CheckVerByAssembly;
                //    if (func(Interface.UpdateUrl, FileVersion))
                //    {
                //        Note = "新版本已发布,请及时更新";
                //    }
                //}
            }
        }
        /// <summary>
        /// 获取文件版本
        /// </summary>
		public string FileVersion
		{
			get { return m_strFileVersion; }
		}
        /// <summary>
        /// 获取插件名称
        /// </summary>
		public string Name
		{
			get { return m_strName; }
		}
        /// <summary>
        /// 获取插件描述
        /// </summary>
		public string Description
		{
			get { return m_strDescription; }
		}
       /// <summary>
       /// 获取插件作者
       /// </summary>
		public string Author
		{
			get { return m_strAuthor; }
		}
      
       public PluginInfo()
       {
       }
        /// <summary>
        /// 构造函数
        /// </summary>
       public PluginInfo(string strFilePath, FileVersionInfo fvi)
       {
           Debug.Assert(strFilePath != null);
           Debug.Assert(fvi != null);

                      m_strFilePath = strFilePath;

           if (fvi.FileVersion != null)
               m_strFileVersion = fvi.FileVersion;
           if (fvi.FileDescription != null)
               m_strName = fvi.FileDescription;
           if (fvi.Comments != null) 
               m_strDescription = fvi.Comments;
           if (fvi.CompanyName != null) 
               m_strAuthor = fvi.CompanyName;
       }
	}
}
