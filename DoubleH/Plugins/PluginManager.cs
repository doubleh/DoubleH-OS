using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Runtime.Remoting;
using System.Diagnostics;
using System.IO;
using DoubleH.Utility;
using DoubleH.Utility.Configuration;
using DoubleH.Utility.IO;
using FCNS.Data;

namespace DoubleH.Plugins
{
    /// <summary>
    /// 插件管理器
    /// </summary>
    public sealed class PluginManager
    {
        private IPluginHost m_host = null;
        public  delegate void IsLoadPlugin(FCNS.Data.Table.DataTableS table);
        public static event IsLoadPlugin LoadPlugined;

        /// <summary>
        /// 初始化
        /// </summary>
        public void Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            m_host = host;
        }

        private List<PluginInfo> m_vPlugins = new List<PluginInfo>();
        public List<PluginInfo> Plugins
        {
            get { return m_vPlugins; }
        }

        /// <summary>
        /// 加载所有插件
        /// </summary>
        public void LoadAllExtension()
        {
            Debug.Assert(m_host != null);

            try
            {
                DirectoryInfo di = new DirectoryInfo(DbDefine.extensionDir);

                FileInfo[] vFiles = di.GetFiles("*.dll", SearchOption.AllDirectories);
                foreach (FileInfo fi in vFiles)
                    LoadDllFile(DbDefine.extensionProductName, fi.FullName, null, DataTableText.Null, null, null);
            }
            catch (Exception ee) { MessageWindow.Show("LoadAllExtension()  " + ee.Message); }
        }

        public void UnloadAllExtension()
        {
            foreach (PluginInfo plugin in m_vPlugins)
            {
                Debug.Assert(plugin.Interface != null);
                if (plugin.Interface != null)
                {
                    try { plugin.Interface.Terminate(); }
                    catch (Exception) { Debug.Assert(false); }
                }
            }

            m_vPlugins.Clear();
        }

        public bool LoadPlugin(string file, string strTypeName, DataTableText tableText, FCNS.Data.Table.DataTableS dataTable, object tag)
        {
            //MessageWindow.Show(tableText.ToString() + " " + dataTable.GetType().ToString()+" "+(dataTable==null).ToString());
            bool result= LoadDllFile(DbDefine.pluginProductName, file, strTypeName, tableText, dataTable,  tag);
            if (result && LoadPlugined != null)
                LoadPlugined(dataTable);

            return result;
        }


        private static Plugin CreatePluginInstance(string strFilePath, string strTypeName)
        {
                Debug.Assert(strFilePath != null);
                if (strFilePath == null)
                    throw new ArgumentNullException("尝试建立扩展实例失败 - " + strFilePath);

                string strType;
                if (string.IsNullOrEmpty(strTypeName))
                {
                    strType = UrlUtil.GetFileName(strFilePath);
                    strType = UrlUtil.StripExtension(strType) + "." +
                        UrlUtil.StripExtension(strType) + "Ext";
                }
                else strType = strTypeName + "." + strTypeName + "Ext";
                try
                {
                    ObjectHandle oh = Activator.CreateInstanceFrom(strFilePath, strType);
                
                Plugin plugin = (oh.Unwrap() as Plugin);
                if (plugin == null) throw new FileLoadException();

                return plugin;

                }
                catch { return null; }
            }

        private bool LoadDllFile(string productName, string file, string strTypeName, DataTableText tableText, FCNS.Data.Table.DataTableS dataTable, object tag)
        {
            if (!File.Exists(file))
                return false;

            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(file);
            if ((fvi == null) || (fvi.ProductName == null) ||
                (fvi.ProductName != productName))
            {
                return false;
            }
            //try
            //{
                PluginInfo pi = new PluginInfo(file, fvi);
                pi.Interface = CreatePluginInstance(pi.FilePath, strTypeName);

                pi.Interface.TableText = tableText;
                pi.Interface.Tag = tag;
                pi.Interface.DataTable = dataTable;
                bool b = pi.Interface.Initialize(m_host);
                if (b && productName == DbDefine.extensionProductName)
                    m_vPlugins.Add(pi);

                return b;
            //}
            //catch (Exception ee)
            //{
            //    MessageWindow.Show(file + " - 模板加载错误,可能是(?)Ext前缀错误\r\n" + ee.Message);
            //    return false;
            //}
        }
    }
}
