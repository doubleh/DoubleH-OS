﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoubleH
{
    /// <summary>
    /// MessageForm.xaml 的交互逻辑
    /// </summary>
    public partial class MessageForm : Window
    {
        public MessageForm()
        {
            InitializeComponent();

            InitEvent();

        }

        private void InitEvent()
        {
            this.Activated += (ss, ee) =>
            {
                this.ResizeMode = ResizeMode.CanResize;
                textBox1.Focus();
            };

            this.Deactivated += (ss, ee) => this.ResizeMode = ResizeMode.NoResize;
            textBox1.KeyDown += (ss, ee) => SendMessage(ee);
        }

        private void SendMessage(KeyEventArgs ee)
        {
            if (ee.Key != Key.Enter)
                return;

            if (string.IsNullOrEmpty(textBox1.Text))
                return;

            foreach (FSLib.IPMessager.Entity.Host host in DoubleH.Utility.Net.NetUtility.AllHostButMe)
                DoubleH.Utility.Net.NetUtility.LanClient.Commander.SendTextMessage(host, textBox1.Text, false, false, false, false);

            textBox1.Clear();
            textBox1.Focus();
        }

        public void AddMessage(string text)
        {
            richTextBox1.AppendText(text + "\r");
            richTextBox1.ScrollToEnd();

            textBox1.Focus();
        }
    }
}
