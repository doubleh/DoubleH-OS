﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoubleH.Plugins;
using System.Diagnostics;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace UniqueS
{
    public class UniqueSExt : Plugin
    {
        MainWindow form = null;
        WindowVip vip = null;
        WindowKeMu kemu = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            Table.UniqueS.EnumFlag flag = Table.UniqueS.EnumFlag.客户性质;
            Enum.TryParse(TableText.ToString(), true, out flag);
            switch (flag)
            {
                case Table.UniqueS.EnumFlag.会员分类:
                    vip = new WindowVip();
                    vip.Init(flag, DataTable as Table.UniqueS);
                    vip.ShowDialog();
                    break;

                case Table.UniqueS.EnumFlag.会计科目:
                    kemu = new WindowKeMu();
                    kemu.Init(DataTable as Table.UniqueS);
                    kemu.ShowDialog();
                    break;

                default:
                    form = new MainWindow();
                    form.Init(flag, DataTable as Table.UniqueS);
                    form.ShowDialog();
                    break;
            }
            Terminate();
            return true;
        }

        public override void Terminate()
        {
            if (form != null)
                form.Close();

            if (vip != null)
                vip.Close();

            if (kemu != null)
                kemu.Close();
        }
    }
}
