﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Table = FCNS.Data.Table;

namespace ReportS
{
    public partial class MainWindow
    {
        private static IList GetProductS(ProductS.GetProductS.EnumProductS productSType = ProductS.GetProductS.EnumProductS.询价单商品)
        {
            ProductS.GetProductS gp = new ProductS.GetProductS();
            gp.Init(productSType, null);
            gp.ShowDialog();
            return gp.Selected;
        }

       public static Int64[] GetProductSId(ProductS.GetProductS.EnumProductS productSType = ProductS.GetProductS.EnumProductS.询价单商品)
        {
            List<Int64> li = new List<long>();
            var vr = GetProductS(productSType);
            if (vr != null)
                foreach (Table.ProductS ps in vr)
                    li.Add(ps.Id);

            return li.ToArray();
        }

        internal  static  string GetOrderTypeName(string orderNO)
        {
            if (string.IsNullOrEmpty(orderNO))
                return string.Empty;

            switch (orderNO.Substring(0,2))
            {
                case "CG": return "采购款";
                case "PF": return "批发款";
                case "SH": return "售后款";
                case "XM": return "项目款";
                case "PS": return "零售款";
                //case Table.CarOilS.tableName:
                //case Table.CarHealthS.tableName:
                //    return "车辆成本";
                //case Table.WuLiuS.tableName: return "物流费用";

                default: return string.Empty;
            }
        }
    }
}
