﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Data;
using FCNS.Data;
using System.Diagnostics;
using System.Collections.ObjectModel;
/*
 * 因为‘服务评价’是动态可变的，所以使用【】模式
 */
namespace ReportS
{
    /// <summary>
    /// 售后统计表.xaml 的交互逻辑
    /// </summary>
    public partial class 售后统计表 : Window
    {
        public 售后统计表()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        DataGridColumn gcUser = null;
        DataGridColumn gcCorS = null;
        ObservableCollection<Table.UniqueS> uniqueList = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.服务评价, Table.UniqueS.EnumEnable.启用);

        public void InitVar()
        {
            uCdataGrid1.Init(DataTableText.售后统计表, null, new UC.CharProperty
            {
                Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(),
                ValueTitle = "金额",
                ValueName = "_Money",
            }, new UC.CharProperty
            {
                Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(),
                ValueTitle = "单据数量",
                ValueName = "_Quantity",
            });

            gcUser = uCdataGrid1.dataGridExt1.GetColumn("_UserSName");
            gcCorS = uCdataGrid1.dataGridExt1.GetColumn("_CorSName");
            Debug.Assert(gcUser != null);
            Debug.Assert(gcCorS != null);
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) =>
            {
                this.Cursor = Cursors.Wait;
                FillData();
                this.Cursor = Cursors.Arrow;
            };
        }

        bool? byUser = null;
        private void FillData()
        {
            if (radioButtonCorS.IsChecked.Value)
            {
                if (byUser != true)
                {
                    byUser = true;
                    gcUser.Visibility = Visibility.Collapsed;
                    gcCorS.Visibility = Visibility.Visible;
                }
                uCdataGrid1.Init(DataTableText.售后统计表, FilterByCorS());
            }
            else
            {
                if (byUser != false)
                {
                    byUser = false;
                    gcUser.Visibility = Visibility.Visible;
                    gcCorS.Visibility = Visibility.Collapsed;
                }
                uCdataGrid1.Init(DataTableText.售后统计表,FilterByUser());
            }
        }

        private List<TempClass> FilterByUser()
        {
            List<TempClass> returnObj = new List<TempClass>();
            ObservableCollection<Table.AfterSaleServiceS> obj = Table.AfterSaleServiceS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, null, Table.AfterSaleServiceS.EnumEnable.维护完工, Table.AfterSaleServiceS.EnumEnable.回访反馈, Table.AfterSaleServiceS.EnumEnable.员工评价, Table.AfterSaleServiceS.EnumEnable.作废);
            if (obj == null)
                return returnObj;

            foreach (Table.UserS user in Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.经办人))
            {
                var vr = from f in obj where f.ServiceUserS.Split(';').Contains(user.Id.ToString()) select f;
                if (vr.Count() == 0)
                    continue;

                TempClass tc = new TempClass();
                tc._UserSName=  user.Name;
                foreach (Table.AfterSaleServiceS ass in vr)
                {
                    tc._Quantity += 1;
                    if (ass.Enable == Table.AfterSaleServiceS.EnumEnable.作废)
                    {
                        tc._QuantityZuoFei+= 1;
                        continue;
                    }
                    double money = ass.FuWuFei + ass.JiaoTongFei;
                    if (money > 0)
                    {
                       tc._Money+=money;
                       tc._QuantityHaveMoney+= 1;
                    }
                    else
                        tc._QuantityNoMoney+= 1;

                    if (money != ass.FuWuFeiHeShi)
                       tc._MoneyError+= 1;

                    //Table.UniqueS us = uniqueList.FirstOrDefault(f =>f.Id == ass.FuWuPingJia);
                    //if (us != null)
                    //    ot[us.Id.ToString()] = (int)ot[us.Id.ToString()] + 1;
                }
                returnObj.Add(tc);
            }

            return returnObj;
        }

        private List<TempClass> FilterByCorS()
        {
            List<TempClass> returnObj = new List<TempClass>();
            ObservableCollection<Table.AfterSaleServiceS> obj = Table.AfterSaleServiceS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, null, Table.AfterSaleServiceS.EnumEnable.维护完工, Table.AfterSaleServiceS.EnumEnable.回访反馈, Table.AfterSaleServiceS.EnumEnable.员工评价, Table.AfterSaleServiceS.EnumEnable.作废);
            if (obj == null)
                return returnObj;

            Dictionary<Int64, string> cors = new Dictionary<long, string>();
            obj.ToList().ForEach(f => { if (!cors.Keys.Contains(f.CorSId))cors.Add(f.CorSId, f._CorSName); });

            foreach (Int64 id in cors.Keys)
            {
                var vr = from f in obj where f.CorSId == id select f;
                if (vr.Count() == 0)
                    continue;

                TempClass tc = new TempClass();
                tc._CorSName = cors[id];
                foreach (Table.AfterSaleServiceS ass in vr)
                {
                    tc._Quantity+= 1;
                    if (ass.Enable == Table.AfterSaleServiceS.EnumEnable.作废)
                    {
                        tc._QuantityZuoFei+= 1;
                        continue;
                    }
                    double money = ass.FuWuFei + ass.JiaoTongFei;
                    if (money > 0)
                    {
                        tc._Money+= money;
                        tc._QuantityHaveMoney+= 1;
                    }
                    else
                       tc._QuantityNoMoney+= 1;

                    if (money != ass.FuWuFeiHeShi)
                        tc._MoneyError+= 1;

                    //ot[ass.FuWuPingJia.ToString()] = (int)ot[ass.FuWuPingJia.ToString()] + 1;
                }

                returnObj.Add(tc);
            }

            return returnObj;
        }


        public class TempClass
        {
            public string _UserSName { get; set; }
            public string _CorSName { get; set; }
            double quantity = 0;
            public double _Quantity { get { return quantity; } set { quantity = value; } }

            double quantityZuoFei = 0;
            public double _QuantityZuoFei { get { return quantityZuoFei; } set { quantityZuoFei = value; } }

            double money=0;
            public double _Money { get { return money; } set { money = value; } }

            double quantityHaveMoney = 0;
            public double _QuantityHaveMoney { get { return quantityHaveMoney; } set { quantityHaveMoney = value; } }

            double moneyNoMoney=0;
            public double _QuantityNoMoney { get{return moneyNoMoney;}set{moneyNoMoney=value;}}

            double moneyError=0;
            public double _MoneyError { get{return moneyError;}set{moneyError=value;}}
        }
    }
}