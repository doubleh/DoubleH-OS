﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 商品赠送明细表.xaml 的交互逻辑
    /// </summary>
    public partial class 商品赠送明细表 : Window
    {
        public 商品赠送明细表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            uCdataGrid1.HideChart = true;

            checkComboBoxUserS.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.操作员, Table.UserS.EnumFlag.POS操作员, Table.UserS.EnumFlag.经办人);
            checkComboBoxUserS.DisplayMemberPath = "Name";
            checkComboBoxUserS.ValueMemberPath = "Id";
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) =>
                {
                    var pos = Table.PosOrderS.GetListForZengSong(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, NormalUtility.FormatStringToInt64(checkComboBoxUserS.SelectedValue));
                    List<TempClass> result = new List<TempClass>();
                    foreach (Table.PosOrderS order in pos)
                    {
                        var vr = Table.ProductSInZengSongS.GetList(order.OrderNO);
                        foreach (Table.ProductSInZengSongS io in vr)
                        {
                            TempClass tc = new TempClass();
                            tc._ProductSName = io._ProductSName;
                            tc._StoreSName = order._StoreSName;
                            tc.AveragePrice = io.AveragePrice;
                            tc.OrderDateTime = order.OrderDateTime;
                            tc.OrderNO = order.OrderNO;
                            tc.Quantity = io.Quantity;
                            tc._ZengSongUserSName = order._ZengSongUserSName;
                            result.Add(tc);
                        }
                        //var vr = Table.ProductSIO.GetList(order.OrderNO).Where(f => f.PurchasePrice == 0 && f.WholesalePrice == 0);
                        //foreach (Table.ProductSIO io in vr)
                        //{
                        //    TempClass tc = new TempClass();
                        //    tc._ProductSName = io._ProductSName;
                        //    tc._StoreSName = io._StoreSName;
                        //    tc.AveragePrice = io.AveragePrice;
                        //    tc.OrderDateTime = order.OrderDateTime;
                        //    tc.OrderNO = order.OrderNO;
                        //    tc.Quantity = io.Quantity;
                        //    tc._ZengSongUserSName = order._ZengSongUserSName;
                        //    result.Add(tc);
                        //}
                    }
                    uCdataGrid1.Init(DataTableText.商品赠送明细表, result, false);
                };
        }

        public class TempClass
        {
            public double AveragePrice { get; set; }
            public double Quantity { get; set; }
            public string OrderNO { get; set; }
            public DateTime OrderDateTime { get; set; }
            public string _ProductSName { get; set; }
            public string _StoreSName { get; set; }
            public string _ZengSongUserSName { get; set; }
        }
    }
}