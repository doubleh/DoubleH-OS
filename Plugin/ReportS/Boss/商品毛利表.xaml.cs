﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using FCNS.Data;
using System.Data;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace ReportS
{
    /// <summary>
    /// 商品毛利表.xaml 的交互逻辑
    /// </summary>
    public partial class 商品毛利表 : Window
    {
        public 商品毛利表()
        {
            InitializeComponent();
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        Int64[] productS = null;
        public void InitVar()
        {
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;

            uCdataGrid1.HideChart = true;
            uCdataGrid1.dataGridExt1.LastRowBlod = true;
        }

        private void InitEvent()
        {
            buttonProductS.Click += (ss, ee) => productS = MainWindow.GetProductSId(); ;
            buttonOK.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            List<TempClass> obj = new List<TempClass>();

            foreach (Table.ProductSIO io in Table.ProductSIO.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, true, NormalUtility.FormatStringToInt64(checkComboBoxStoreS.SelectedValue), productS, Table.ProductSIO.EnumEnable.入账))
            {
                TempClass tc = new TempClass();
                tc._GrossProfit = io._GrossProfit;
                //tc._OrderTypeName = io._OrderTypeName;
                tc._ProductSName = io._ProductSName;
                tc._StoreSName = io._StoreSName;
                tc.AveragePrice = io.AveragePrice;
                tc.OrderDateTime = io.OrderDateTime;
                tc.OrderNO = io.RelatedOrderNO;
                //tc.PurchasePrice = io.PurchasePrice;
                tc.Quantity = io.Quantity;
                //tc.WholesalePrice = io.Price;
                obj.Add(tc);
            }

            TempClass ps = new TempClass()
         {
             _ProductSName = "合计",
             WholesalePrice = Math.Round(obj.Sum(w => w.WholesalePrice), Table.SysConfig.SysConfigParams.DecimalPlaces),
             Quantity = Math.Round(obj.Sum(q => q.Quantity), Table.SysConfig.SysConfigParams.DecimalPlaces),
             AveragePrice = Math.Round(obj.Sum(w => w.AveragePrice), Table.SysConfig.SysConfigParams.DecimalPlaces)
         };
            ps._GrossProfit = Math.Round(ps.WholesalePrice - ps.AveragePrice, Table.SysConfig.SysConfigParams.DecimalPlaces);
            obj.Add(ps);

            uCdataGrid1.Init(DataTableText.商品毛利表, obj, false);
            uCdataGrid1.OtherObjectForPrint = new OtherClass() { DateTimeStartToEnd = uCDateTime1.StartDateTime.ToString("yyyy-MM-dd 至 ") + uCDateTime1.EndDateTime.ToString("yyyy-MM-dd") };
        }


        public class OtherClass
        {
            public string DateTimeStartToEnd { get; set; }
        }

        public class TempClass
        {
            public double AveragePrice { get; set; }
            public double PurchasePrice { get; set; }
            public double WholesalePrice { get; set; }
            public double Quantity { get; set; }
            public string OrderNO { get; set; }
            public DateTime? OrderDateTime { get; set; }
            public string _ProductSName { get; set; }
            public string _StoreSName { get; set; }
            public string _OrderTypeName { get; set; }
            public double _GrossProfit { get; set; }
        }
    }
}