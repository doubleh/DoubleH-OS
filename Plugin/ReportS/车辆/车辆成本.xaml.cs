﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 车辆成本.xaml 的交互逻辑
    /// </summary>
    public partial class 车辆成本 : Window
    {
        public 车辆成本()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxCar.ItemsSource = Table.CarS.GetList();
            checkComboBoxCar.DisplayMemberPath = "Name";
            checkComboBoxCar.ValueMemberPath = "Id";

            foreach (string str in Enum.GetNames(typeof(Table.CarHealthS.EnumFlag)))
                checkComboBoxFlag.Items.Add(str);

            checkComboBoxFlag.Items.Add("油票记录"); 
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>FillData();
        }

        private void FillData()
        {
            List<TempClass> tc = new List<TempClass>();
            tc.AddRange(GetOil());
            tc.AddRange(GetHealth());
            tc.Add(new TempClass()
            {
                CarName = "合计:",
                Money = tc.Sum(f => f.Money)
            });

            uCdataGrid1.Init(DataTableText.车辆成本, tc, false,
                new UC.CharProperty() { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueName = "Money", ValueTitle = "金额" });

            uCdataGrid1.OtherObjectForPrint = new OtherClass() { DateTimeStartToEnd = uCDateTime1.StartDateTime.ToString("yyyy-MM-dd 至 ") + uCDateTime1.EndDateTime.ToString("yyyy-MM-dd") };
        }

        private List<TempClass> GetOil()
        {
            List<TempClass> tc = new List<TempClass>();
            if (string.IsNullOrEmpty(checkComboBoxFlag.Text) || checkComboBoxFlag.Text.Contains("油票记录"))
            {
                foreach (Table.CarOilS co in Table.CarOilS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, NormalUtility.FormatStringToInt64(checkComboBoxCar.SelectedValue)))
                {
                    TempClass t = new TempClass();
                    t.CarName = co._CarName;
                    t.Flag = "油票记录";
                    t.Money = co.Money;
                    t.Note = co.Note;
                    t.OrderDateTime = co.OrderDateTime;
                    t.OrderNO = co.OrderNO;
                    tc.Add(t);
                }
            }
            return tc;
        }

        private List<TempClass> GetHealth()
        {
            List<TempClass> tc = new List<TempClass>();
            List<Table.CarHealthS.EnumFlag> flag = new List<Table.CarHealthS.EnumFlag>();
            if (!string.IsNullOrEmpty(checkComboBoxFlag.Text))
            {
                if (checkComboBoxFlag.Text == "油票记录")
                    return tc;

                foreach (string str in checkComboBoxFlag.SelectedValue.Split(','))
                {
                    Table.CarHealthS.EnumFlag f;
                    if (Enum.TryParse(str, true, out f))
                        flag.Add(f);
                }
            }

            foreach (Table.CarHealthS cs in Table.CarHealthS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime,NormalUtility.FormatStringToInt64(checkComboBoxCar.SelectedValue), flag.ToArray()))
            {
                TempClass t = new TempClass();
                t.CarName = cs._CarName;
                t.Flag = cs._FlagText;
                t.Money = cs.Money;
                t.Note = cs.Note;
                t.OrderDateTime = cs.DateEnd.Value;
                t.OrderNO = cs.OrderNO;
                tc.Add(t);
            }
            return tc;
        }

        
        public class OtherClass
        {
            public string DateTimeStartToEnd { get; set; }
        }
        private class TempClass
        {
            public string OrderNO { get; set; }
            public string CarName { get; set; }
            public DateTime? OrderDateTime { get; set; }
            public double Money { get; set; }
            public string Flag { get; set; }
            public string Note { get; set; }
        }
    }
}
