﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Threading;
using Table = FCNS.Data.Table;

namespace ReportS
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        static MainWindow mw;
        public static MainWindow Window
        {
            get { return mw; }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            bool bRun = true;
            Mutex m = new Mutex(true, "ReportS.exe", out bRun);
            if (bRun)
            {
                m.ReleaseMutex();
                mw = new MainWindow();
                mw.Init(e.Args);
                mw.Show();
            }
            else
                Application.Current.Shutdown();
        }
    }
}
