﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace ReportS
{
    /// <summary>
    /// 账户余额表.xaml 的交互逻辑
    /// </summary>
    public partial class 账户余额表 : Window
    {
        public 账户余额表()
        {
            InitializeComponent();

            this.Loaded += (ss, ee) => InitVar();
        }

        private void InitVar()
        {
            uCdataGrid1.Init(DataTableText.账户余额表,Table.UniqueS.GetList(Table.UniqueS.EnumFlag.支付方式, Table.UniqueS.EnumEnable.启用),
                new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.ColumnSeries(), ValueTitle = "金额", ValueName = "DoubleValue" });
        }
    }
}
