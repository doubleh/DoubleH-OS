﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Collections;
using System.Diagnostics;

namespace ReportS
{
    /// <summary>
    /// 发票明细表.xaml 的交互逻辑
    /// </summary>
    public partial class 发票明细表 : Window
    {
        public 发票明细表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        { 
            checkComboBoxInvoiceType.Items.Add("已开具发票");
            checkComboBoxInvoiceType.Items.Add("已收取发票");

            checkComboBoxCorS.DisplayMemberPath = "Name";
            checkComboBoxCorS.ValueMemberPath = "Id";
            checkComboBoxCorS.ItemsSource = MainWindow.allCorS;
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>
                {
                    List<Int64> cors = new List<Int64>();
                    if (!string.IsNullOrEmpty(checkComboBoxCorS.Text))
                        foreach (string str in checkComboBoxCorS.SelectedValue.Split(','))
                            cors.Add(Int64.Parse(str));

                    List<TempClass> il = new List<TempClass>();
                    Table.PayS.EnumFlag? flag=null;
                    if (checkComboBoxInvoiceType.SelectedItems.Count == 1)
                        flag = (checkComboBoxInvoiceType.Text == "已开具发票" ? Table.PayS.EnumFlag.收款单 : Table.PayS.EnumFlag.付款单);

                    var sl = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.发票定义);
                    foreach (Table.PayS ps in Table.PayS.GetListAboutInvoice(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, flag, true, cors.ToArray()))
                    {
                        TempClass cl = new TempClass();
                        cl.CorSName = ps._CorSName;
                        cl.InvoiceDateTime = ps.InvoiceDateTime;
                        cl.InvoiceNO = ps.Invoice;
                        cl.Own = ps.Flag == Table.PayS.EnumFlag.收款单;

                        Table.UniqueS us = sl.FirstOrDefault<Table.UniqueS>(f => f.Id == ps.InvoiceType);
                        Debug.Assert(us != null);

                        cl.InvoiceType = us.Name + "（" + us.DoubleValue + "）";
                        cl.Money = Math.Round(ps.Money / (1 + us.DoubleValue), Table.SysConfig.SysConfigParams.DecimalPlaces);
                        cl.MoneyTax = ps.Money;
                        cl.Tax = cl.MoneyTax-cl.Money;
                        il.Add(cl);
                    }
                    uCdataGrid1.Init(DataTableText.发票明细表,il);
                };
        }



        private class TempClass
        {
            public DateTime InvoiceDateTime { get; set; }
            public string InvoiceType { get; set; }
            public string InvoiceNO { get; set; }
            public double Money { get; set; }
            public double Tax { get; set; }
            public double MoneyTax { get; set; }
            public string CorSName { get; set; }
            public bool Own { get; set; }
        }
    }
}
