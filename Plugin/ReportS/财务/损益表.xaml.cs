﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;

namespace ReportS
{
    /// <summary>
    /// 损益表.xaml 的交互逻辑
    /// </summary>
    public partial class 损益表 : Window
    {
        public 损益表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            uCdataGrid1.HideChart = true;
            uCdataGrid1.dataGridExt1.LastRowBlod = true;
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            //先搜索所有具有税目的收支单记录
            Dictionary<string, double> searchValue = new Dictionary<string, double>();
            foreach (Table.PayS p in Table.PayS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, new int[] { (int)Table.PayS.EnumFlag.付款单, (int)Table.PayS.EnumFlag.收款单 },
                new int[] { (int)Table.PayS.EnumEnable.评估, (int)Table.PayS.EnumEnable.审核 }).Where(f => !string.IsNullOrEmpty(f.AccountingSubjects)))
            {
                if (searchValue.ContainsKey(p.AccountingSubjects))
                    searchValue[p.AccountingSubjects] = searchValue[p.AccountingSubjects] + p.RealMoney;
                else
                    searchValue[p.AccountingSubjects] = p.RealMoney;
            }
            //其它收支单
            foreach (Table.DetailInPayS di in Table.DetailInPayS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime))
            {
                if (searchValue.ContainsKey(di.KeMu))
                    searchValue[di.KeMu] = searchValue[di.KeMu] + di.Money;
                else
                    searchValue[di.KeMu] = di.Money;
            }
            //POS单（独立核算）
            foreach(Table.PosOrderS ps in Table.PosOrderS.GetList(uCDateTime1.StartDateTime,uCDateTime1.EndDateTime,null,null,null,null))
            {
                string kemu = "300101";
                if (ps.Flag == Table.PosOrderS.EnumFlag.POS退货单)
                    kemu = "40010103";

                if (searchValue.ContainsKey(kemu))
                    searchValue[kemu] = searchValue[kemu] + ps.Money;
                else
                    searchValue[kemu] = ps.Money;

                if (ps.ZengSongList.Count <= 0)
                    continue;

                if (searchValue.ContainsKey("40010102"))
                    searchValue["40010102"] = searchValue["40010102"] + ps.ZengSongList.Sum(f=>(f.Quantity*f.AveragePrice));
                else
                    searchValue["40010102"] = ps.ZengSongList.Sum(f => (f.Quantity * f.AveragePrice));
            }

            //筛选数据
            List<TempClass> result = new List<TempClass>();
            result.Add(new TempClass() { _KeMu = "30", _Name = "收入类" });
            result.Add(new TempClass() { _KeMu = "40", _Name = "支出类" });
            double money30 = 0, money40 = 0;
            foreach (Table.UniqueS us in Table.UniqueS.GetListForKeMu(false, Table.UniqueS.EnumKeMuType.收入类, Table.UniqueS.EnumKeMuType.支出类))
            {
                var obj = from f in searchValue
                          where f.Key == us.StringValue
                          select f;

                TempClass tc = new TempClass();
                tc._KeMu = us.StringValue;
                tc._Name = us.Name;
                for (int i = 0; i < us.StringValue.Length / 2; i++)
                    tc._Name = tc._Name.Insert(0, "----");

                tc._Money = obj.Sum(f => f.Value);
                if (tc._KeMu.StartsWith("30"))
                    money30 += tc._Money;
                else
                    money40 += tc._Money;

                result.Add(tc);
            }

            //利润(亏损)总额
            result.Add(new TempClass() { _KeMu = "总额", _Name = "+利润-亏损", _Money =Math.Round( money30 - money40,Table.SysConfig.SysConfigParams.DecimalPlaces) });
            uCdataGrid1.Init(DoubleH.Utility.DataTableText.损益表, result,false);
            uCdataGrid1.OtherObjectForPrint = new OtherClass() { DateTimeStartToEnd = uCDateTime1.StartDateTime.ToString("yyyy-MM-dd 至 ") + uCDateTime1.EndDateTime.ToString("yyyy-MM-dd") };
        }



        public class OtherClass
        {
            public string DateTimeStartToEnd { get; set; }
        }

        public class TempClass
        {
            public string _KeMu { get; set; }
            public string _Name { get; set; }
            public double _Money { get; set; }
            public double _MoneyAll { get; set; }
        }
    }

}