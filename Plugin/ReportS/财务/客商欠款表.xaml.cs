﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using System.Data;

namespace ReportS
{
    /// <summary>
    /// 客商余额表.xaml 的交互逻辑
    /// </summary>
    public partial class 客商余额表 : Window
    {
        public 客商余额表()
        {
            InitializeComponent();
            this.Loaded += (ss, ee) => InitVar();
        }

        private void InitVar()
        { 
            List<Money> all = new List<Money>();
            foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows("select Name,Money from CorS where Money<>0"))
            {
                Money m = new Money() { Name = row["Name"] as string };
                if ((double)row["Money"] > 0)
                    m.MoneyZ = Math.Abs((double)row["Money"]);
                else
                    m.MoneyF =Math.Abs( (double)row["Money"]);

                all.Add(m);
            }

            uCdataGrid1.Init(DataTableText.客商余额表,all, new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.ColumnSeries(), ValueTitle = "己方欠款", ValueName = "MoneyZ" },
                new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.ColumnSeries(), ValueTitle = "客商欠款", ValueName = "MoneyF" });
        }
    }

    public class Money
    {
        public string Name { get; set; }
        public double? MoneyZ { get; set; }
        public double? MoneyF { get; set; }
    }
}
