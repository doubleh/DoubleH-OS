﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using FCNS.Data;
using System.Data;
using System.Diagnostics;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 商品出入库明细表.xaml 的交互逻辑
    /// </summary>
    public partial class 商品出入库明细表 : Window
    {
        public 商品出入库明细表()
        {
            InitializeComponent();
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        Int64[] productS = null;
        public void InitVar()
        {
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;

            uCdataGrid1.HideChart = true;
        }

        private void InitEvent()
        {
            buttonProductS.Click += (ss, ee) => productS = MainWindow.GetProductSId();  
            buttonOK.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            IList obj = Table.ProductSIO.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, null, NormalUtility.FormatStringToInt64(checkComboBoxStoreS.SelectedValue), productS, Table.ProductSIO.EnumEnable.入账);
            uCdataGrid1.Init(DataTableText.商品出入库明细表,obj, false);
        }
    }
}