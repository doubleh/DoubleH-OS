﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace CorS
{
    /// <summary>
    /// WindowVip.xaml 的交互逻辑
    /// </summary>
    public partial class WindowVip : Window
    {
        public WindowVip()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.CorS order;
        public void Init(Table.CorS obj)
        {
            order = obj;

            uCUniqueS1.Init(Table.UniqueS.EnumFlag.会员分类);
           datePickerVipStart.SelectedDate= DateTime.Now;
            datePickerVipEnd.SelectedDate = DateTime.Now.AddYears(1);

            InitOrder();
            customerSinfo1.Init(true);
        }

        private void InitOrder()
        {
            if (order == null)
                return;

            customerSinfo1.SelectedObjectId = order.Id;
            uCUniqueS1.SelectedObjectId = order.VipUniqueSId;
            textBoxVipNO.Text = order.VipNO;
            datePickerVipStart.SelectedDate = order.VipDateStart;
            datePickerVipEnd.SelectedDate = order.VipDateEnd;
            checkBoxVipEnable.IsChecked = order.VipUsed;
            textBlockRemainIntegral.Text = order.VipRemainIntegral.ToString();

            buttonSave.IsEnabled = order.Id == -1;
        }

        private void InitEvent()
        {
            customerSinfo1.CorSChanged += (ee) =>   order = ee; InitOrder(); 
            buttonSave.Click += (ss, ee) =>  Save();  
        }

        private void Save()
        {
            if (order == null)
                return;

            order.VipDateStart = datePickerVipStart.SelectedDate.Value;
            order.VipDateEnd = datePickerVipEnd.SelectedDate.Value;
            order.VipNO = textBoxVipNO.Text;
            order.VipUniqueSId = uCUniqueS1.SelectedObjectId;

            order.VipUsed = checkBoxVipEnable.IsChecked == true;
            Table.DataTableS.EnumDatabaseStatus result = order.Update();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}