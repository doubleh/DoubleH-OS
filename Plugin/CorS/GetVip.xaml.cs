﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace CorS
{
    /// <summary>
    /// GetVip.xaml 的交互逻辑
    /// </summary>
    public partial class GetVip : Window
    {
        public GetVip()
        {
            InitializeComponent();
        }

        Table.CorS selectedCorS = null;
        public Table.CorS SelectedObject
        {
            get { return selectedCorS; }
        }

        public Int64 SelectedObjectId 
        {
            get {if(selectedCorS==null)
                return -1;
                else
                return selectedCorS.Id;
            }
        }

        public void Init()
        {
            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            textBoxSearchText.Focus();

            listBoxSearchResult.DisplayMemberPath = "Name";
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) => SearchVip();
            listBoxSearchResult.SelectionChanged += (ss, ee) => ChangedVip();
            buttonOK.Click += (ss, ee) => this.Close();
        }

        private void SearchVip()
        {
            if (string.IsNullOrEmpty(textBoxSearchText.Text))
                return;

          listBoxSearchResult.ItemsSource=  Table.CorS.GetVipList(textBoxSearchText.Text, false);
          if (listBoxSearchResult.Items.Count == 1)
              listBoxSearchResult.SelectedIndex = 0;
          else if (listBoxSearchResult.Items.Count == 0)
              MessageWindow.Show("会员不存在");

          textBoxSearchText.Clear();
          textBoxSearchText.Focus();
        }

        private void ChangedVip()
        {
            selectedCorS = listBoxSearchResult.SelectedItem as Table.CorS;
            if (selectedCorS == null)
            {
                label1.Content = string.Empty;
                label2.Content = string.Empty;
                label3.Content = string.Empty;
                return;
            }

            label1.Content = selectedCorS.Name + " / " + selectedCorS._VipUniqueSName + "(" + selectedCorS.VipNO+")";
            label2.Content = "有效期:" + selectedCorS.VipDateStart.ToShortDateString() + " 到 " + selectedCorS.VipDateEnd.ToShortDateString();
            label3.Content = "剩余积分:" + selectedCorS.VipRemainIntegral;
        }
    }
}
