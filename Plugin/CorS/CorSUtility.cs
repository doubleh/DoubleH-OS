﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CorS
{
    class CustomerSUtility
    {
        public class CustomerSBinding
        {
            public Int64 Id { get; set; }
            public string Name { get; set; }
            public Int64 GroupSId { get; set; }
        }
    }
}
