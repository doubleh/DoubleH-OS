﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;
using System.Diagnostics;
using DoubleH.Utility;
using System.Data;

namespace PayS
{
    /// <summary>
    /// BankMoney.xaml 的交互逻辑
    /// </summary>
    public partial class BankMoney : Window
    {
        public BankMoney()
        {
            InitializeComponent();
        }

        Table.BankMoneyS order = null;
        public void Init(Table.BankMoneyS obj)
        {
            order = obj ?? new Table.BankMoneyS();

            InitVar();
            InitOrder();
            InitEvent();
        }

        ObservableCollection<Table.UniqueS> allBank;
        private void InitVar()
        {
            allBank = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.支付方式, Table.UniqueS.EnumEnable.启用);

            bankIdOut.DisplayMemberPath = "Name";
            bankIdOut.SelectedValuePath = "Id";
            bankIdOut.SelectedValueBinding = new Binding("BankIdOut");
            bankIdOut.ItemsSource = allBank;

            bankIdIn.DisplayMemberPath = "Name";
            bankIdIn.SelectedValuePath = "Id";
            bankIdIn.SelectedValueBinding = new Binding("BankIdIn");
            bankIdIn.ItemsSource = allBank;
        }

        private void InitOrder()
        {
            dateTimeUpDownKaiDan.Value = order.OrderDateTime;
            labelOrderNO.Content = order.OrderNO;
            textBoxNote.Text = order.Note;
            dataGridObject.ItemsSource = order._DetailText;
            buttonOK.IsEnabled = order.Id == -1;
            dataGridObject.IsReadOnly = !buttonOK.IsEnabled;
        }

        private void InitEvent()
        {
            dataGridObject.CellEditEnding += (ss, ee) => CheckCellEditValue(ee);
            buttonOK.Click += (ss, ee) => Save();
            buttonPrint.Click += (ss, ee) => Print();
        }

        private void Print()
        {
            DataSet ds = new DataSet();
            ds.Tables.AddRange(DoubleH.Utility.Print.PrintForm.GetDataTable(order));
            ds.Tables.AddRange(DoubleH.Utility.Print.PrintForm.GetDataTable(Table.SysConfig.SysConfigParams));
            DataTable dt = new DataTable("TableList");
            ds.Tables.Add(dt);
            for (int i = 0; i < 5; i++)
            {
                DataColumn ndc = new DataColumn("TableList" + i, typeof(string));
                dt.Columns.Add(ndc);
            }
            foreach (Table.BankMoneyS.DetailTextClass item in order._DetailText)
            {
                DataRow row = dt.NewRow();
                row[0] = item.Name;
                row[1] = item._BankNameIn;
                row[2] = item._BankNameOut;
                row[3] = item.Money;
                row[4] = item.Note;
                dt.Rows.Add(row);
            }

            DoubleH.Utility.Print.PrintForm.ShowPrint(ds, DataTableText.银行存取款, "财务\\银行存取款.frx");
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            DataGridColumn textColumn = ee.Column as DataGridColumn;
            double d = 0;
            switch (textColumn.Header.ToString())
            {
                case "金额":
                    TextBox textBox = (TextBox)ee.EditingElement;
                    if (!double.TryParse(textBox.Text, out d))
                        textBox.Undo();
                    else
                        if (d < 0)
                            textBox.Undo();

                    break;
            }
        }

        private void Save()
        {
            Debug.Assert(order.Id == -1);

            order.OrderDateTime = dateTimeUpDownKaiDan.Value.Value;
            order.Note = textBoxNote.Text;
            Table.DataTableS.EnumDatabaseStatus result = order.Insert();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}