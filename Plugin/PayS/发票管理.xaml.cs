﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Diagnostics;
using DoubleH.Utility;

namespace PayS
{
    /// <summary>
    /// WindowInvoice.xaml 的交互逻辑
    /// </summary>
    public partial class WindowInvoice : Window
    {
        public WindowInvoice()
        {
            InitializeComponent();
        }

        Table.PayS order = null;
        public void Init(Table.PayS obj)
        {
            Debug.Assert(obj != null);
            order = obj;

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            uCUniqueS1.Init(Table.UniqueS.EnumFlag.发票定义);
            datePicker1.SelectedDate = DateTime.Now;
            textBox1.Focus();

            if (!string.IsNullOrEmpty(order.Invoice))
            {
                buttonSave.IsEnabled = false;
                buttonCanel.IsEnabled = false;
            }
        }

        private void InitEvent()
        {
            buttonCanel.Click += (ss, ee) => CanelInvoice();
            buttonSave.Click += (ss, ee) => SaveInvoice();
        }

        private void SaveInvoice()
        {
            order.UseInvoice = true;
            order.Invoice = textBox1.Text;
            order.InvoiceType = uCUniqueS1.SelectedObjectId;
            order.InvoiceDateTime = datePicker1.SelectedDate.Value;

            Table.DataTableS.EnumDatabaseStatus result = order.UpdateInvoice();
            if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                MessageWindow.Show(result.ToString());
            else
                this.Close();
        }

        private void CanelInvoice()
        {
            if (MessageWindow.Show("操作不可逆", "确定要取消此单据的关联发票吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            order.UseInvoice = false;
            order.UpdateInvoice();

            this.Close();
        }
    }
}