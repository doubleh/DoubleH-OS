﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;

namespace PayS
{
    class PaySExt : Plugin
    {
        private IPluginHost m_host = null;
        ShouKuan sk = null;
        OtherBill other = null;
        WindowInvoice invoice = null;
        BankMoney bm = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
            switch (TableText)
            {
                case DataTableText.POS记账明细表:
                    Table.PosOrderS pos = DataTable as Table.PosOrderS;
                    Debug.Assert(pos != null);

                    if (MessageWindow.Show("", "确定结账单号:" + pos.OrderNO + "吗?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        pos.Update();

                    break;

                case DataTableText.银行存取款:
                    bm = new BankMoney();
                    bm.Init(DataTable as Table.BankMoneyS);
                    bm.Owner = host.WorkAreaWindow;
                    bm.Show();
                    break;

                case DataTableText.收款单:
                case DataTableText.付款单:
                    sk = new ShouKuan();
                    sk.Init(TableText, DataTable as Table.PayS);
                    sk.ShowDialog();
                    break;

                case DataTableText.其它费用收入单:
                case DataTableText.其它费用支出单:
                    other = new OtherBill();
                    other.Init(TableText, DataTable as Table.PayS);
                    other.Owner = host.WorkAreaWindow;
                    other.Show();
                    break;

                case DataTableText.未开具发票:
                case DataTableText.未收取发票:
                    invoice = new WindowInvoice();
                    invoice.Init(DataTable as Table.PayS);
                    invoice.ShowDialog();
                    break;
            }

            return true;
        }

        public override void Terminate()
        {
            if (bm != null)
                bm.Close();
            if (sk != null)
                sk.Close();
            if (other != null)
                other.Close();
            if (invoice != null)
                invoice.Close();
        }
    }
}
