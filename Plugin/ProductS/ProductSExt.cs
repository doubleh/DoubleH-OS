﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using System.Windows;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace ProductS
{
    public class ProductSExt : Plugin
    {
        private IPluginHost m_host = null;
        MainWindow window = null;
        CheckProductS cps = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
            switch (TableText)
            {
               case DataTableText.商品校验:
                    if (DataTable is Table.ProductS)
                        goto default;

                    cps = new CheckProductS();
                    cps.ShowDialog();
                    host.PagePanel.LoadData(null, cps.SelectedObject);
                    break;
               
                case DataTableText.可采购物料清单:
                     window = new MainWindow();
                     window.Init(DataTable as Table.ProductS ?? new Table.ProductS() { IsWuLiao = true });
                    window.ShowDialog();
                    break;

                default:
                    window = new MainWindow();
                    window.Init( DataTable as Table.ProductS);
                    window.ShowDialog();
                    break;
            }
            Terminate();
            return true;
        }

        public override void Terminate()
        {
            if (window != null)
                window.Close();

            if (cps != null)
                cps.Close();
        }
    }
}