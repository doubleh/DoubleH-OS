﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Collections;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Windows.Controls.Primitives;
using DoubleH.Utility.Configuration;

namespace ProductS.UC
{
    public class UCProductS:ComboBox
    {
        private ObservableCollection<DataRow> filterRows = new ObservableCollection<DataRow>();
        private System.Timers.Timer keypressTimer;
        private delegate void TextChangedCallback();
        private bool insertText;
        private int delayTime;
        private int searchThreshold;

        Table.ProductS product = null;
        public Table.ProductS SelectedObject
        {
            get { return product; }
        }

        public UCProductS()
        {
            keypressTimer = new System.Timers.Timer();
            keypressTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);

            IsEditable = true;
            ItemsSource = filterRows;
            DisplayMemberPath = "[Name]";
            SelectedValuePath = "[Id]";
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            searchThreshold = DoubleHConfig.AppConfig.SearchThreshold;
            delayTime = DoubleHConfig.AppConfig.DelayTime;
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);

            if (insertText == true) insertText = false;

            else
            {
                if (delayTime > 0)
                {
                    keypressTimer.Interval = delayTime;
                    keypressTimer.Start();
                }
                else TextChanged();
            }
        }

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            if (null == SelectedItem)
                product = null;
            else
            {
                insertText = true;
                DataRow row = (DataRow)SelectedItem;
                Text = row["Name"].ToString();
                product = Table.ProductS.GetObject((Int64)row["Id"]);
            }

            base.OnSelectionChanged(e);
        }

        private void TextChanged()
        {
            try
            {
                filterRows.Clear();
                if (Text.Length >= searchThreshold)
                {
                    string sql = "select Id,Name from ProductS where Id like '%" + Text + "%' or  Name like '%" + Text + "%' or  Barcode like '%" + Text + "%'  ";
                    foreach (DataRow entry in FCNS.Data.SQLdata.GetDataRows(sql))
                        filterRows.Add(entry);

                   IsDropDownOpen = HasItems;
                }
                else
                    IsDropDownOpen = false;
            }
            catch { }
        }

        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            keypressTimer.Stop();
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                new TextChangedCallback(this.TextChanged));
        }
      
    }
}
