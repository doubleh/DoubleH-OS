﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table=FCNS.Data.Table;
using System.Collections.ObjectModel;

namespace ProductS
{
    /// <summary>
    /// CheckProductS.xaml 的交互逻辑
    /// </summary>
    public partial class CheckProductS : Window
    {
        public CheckProductS()
        {
            InitializeComponent();

            InitEvent();
        }

        public System.Collections.IList SelectedObject { get; set; }

        private void InitEvent()
        {
            buttonAll.Click += (ss, ee) => SelectAll();
            buttonSearch.Click += (ss, ee) => Search();
        }

        private void Search()
        {
            var v = from f in Table.ProductS.GetListBySearch(null, -1, null)
                    where (checkBoxEnable.IsChecked.HasValue ? (checkBoxEnable.IsChecked == true ? f.Enable == Table.ProductS.EnumEnable.停用 : f.Enable == Table.ProductS.EnumEnable.启用) : true) &&
                        (checkBoxScore.IsChecked.HasValue ? (f.IsScore = checkBoxScore.IsChecked.Value) : true) && (checkBoxDiscount.IsChecked.HasValue ? (f.IsDiscount = checkBoxDiscount.IsChecked.Value) : true)
                    select f;

            SelectedObject = new ObservableCollection<Table.ProductS>(v);
            this.Close();
        }

        private void SelectAll()
        {
            if (buttonAll.Content.ToString() == "全选")
            {
                buttonAll.Content = "全不选";
                foreach (UIElement u in grid1.Children)
                    if (u is CheckBox)
                        ((CheckBox)u).IsChecked = true;
            }
            else
            {
                buttonAll.Content = "全选";
                foreach (UIElement u in grid1.Children)
                    if (u is CheckBox)
                        ((CheckBox)u).IsChecked = false;
            }
        }
    }
}