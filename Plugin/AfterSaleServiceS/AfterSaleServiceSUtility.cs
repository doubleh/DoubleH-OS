﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Table = FCNS.Data.Table;
using System.Data;

namespace AfterSaleServiceS
{
   internal sealed class AfterSaleServiceSUtility
    {
       internal sealed class SaleRecord
       {
           /// <summary>
           /// 获取客户的销售记录
           /// </summary>
           /// <param name="corSId"></param>
           /// <returns></returns>
           internal static IEnumerable<SaleRecord> GetList(Int64 corSId)
           {
               List<SaleRecord> record = new List<SaleRecord>();
               foreach (Table.SalesOrderS order in Table.SalesOrderS.GetList(null,null,new int[]{(int)Table.SalesOrderS.EnumFlag.销售订单},new int[]{(int) Table.SalesOrderS.EnumEnable.入账},null,new int[]{(int)corSId}))
               {
                   foreach (Table.ProductSIO product in Table.ProductSIO.GetList(order.OrderNO))
                   {
                       SaleRecord sale = new SaleRecord();
                       sale.OrderNODateTime = order.OrderDateTime.ToString("yyyy 年 MM 月 dd日");
                       sale.Name = product._ProductSName;
                       sale.Price = product.Price;
                       sale.Quantity = product.Quantity;
                       sale.SaleUserName = order._OperatorerSName;
                       sale.SerialNumber = product.SerialNumber;
                       sale.OrderNO = order.OrderNO;
                       record.Add(sale);
                   }
               }
               return record;
           }
           public string OrderNO { get; set; }
           /// <summary>
           /// 销售日期
           /// </summary>
           public string OrderNODateTime
           {
               get;
               set;
           }
           /// <summary>
           /// 配件名称
           /// </summary>
           public string Name
           {
               get;
               set;
           }
           /// <summary>
           /// 单价
           /// </summary>
           public double Price
           {
               get;
               set;
           }
           /// <summary>
           /// 数量
           /// </summary>
           public double Quantity
           {
               get;
               set;
           }
           /// <summary>
           /// 销售员
           /// </summary>
           public string SaleUserName
           {
               get;
               set;
           }
           /// <summary>
           /// 保修日期
           /// </summary>
           public string SerialNumber
           {
               get;
               set;
           }
       }

       internal sealed class ServiceRecord
       {
           /// <summary>
           /// 获取客户的售后记录
           /// </summary>
           /// <returns></returns>
           internal static List<ServiceRecord> GetList(Int64 customerSId)
           {
               List<ServiceRecord> record = new List<ServiceRecord>();
               StringBuilder sb = new StringBuilder("select ass.GuZhangMiaoShu, ass.JieJueFangFa, ass.FuWuFei, ass.JiaoTongFei, ass.WanChengDateTime,  ass.ServiceUserS,ass.FuWuPingJia,us.Name from AfterSaleServiceS ass,UniqueS us ");
               sb.Append("  where ass.FuWuPingJia=us.Id and ass.CorSId=" + customerSId + " and ass.Enable>=" + (Int64)Table.AfterSaleServiceS.EnumEnable.维护完工 + " and ass.Enable<>" + (Int64)Table.AfterSaleServiceS.EnumEnable.作废 + " ORDER BY ass.Id DESC");
               foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows(sb.ToString()))
               {
                   ServiceRecord sr = new ServiceRecord();
                   sr.FuWuFei = (double)row["FuWuFei"];
                   sr.GuZhangMiaoShu = row["GuZhangMiaoShu"] as string;
                   sr.ServiceUserSName = FCNS.Data.Utility.GetUserSName(row["ServiceUserS"] as string);
                   sr.WanChengDateTime = DateTime.ParseExact(row["WanChengDateTime"] as string, FCNS.Data.DbDefine.dateTimeFormat, null);
                   sr.JiaoTongFei = (double)row["JiaoTongFei"];
                   sr.JieJueFangFa = row["JieJueFangFa"] as string;
                   sr.FuWuPingJiaName = row["Name"] as string;

                   record.Add(sr);
               }

               return record;
           }

           public DateTime WanChengDateTime
           { get; set; }
           public string GuZhangMiaoShu
           {
               get;
               set;
           }
           public string ServiceUserSName
           {
               get;
               set;
           }
           public string JieJueFangFa
           {
               get;
               set;
           }
           public double FuWuFei
           {
               get;
               set;
           }
           public double JiaoTongFei
           { get; set; }
           public string FuWuPingJiaName
           { get; set; }
       }

       internal sealed class RepairRecord
       {
           /// <summary>
           /// 获取客户的返修记录
           /// </summary>
           /// <returns></returns>
           internal static IEnumerable<RepairRecord> GetList(Int64 customerSId)
           {
               List<RepairRecord> record = new List<RepairRecord>();
               StringBuilder sb = new StringBuilder("select rs.StartDateTime,rs.EndDateTime,pirs.Quantity,pirs.GuZhangMiaoShu,pirs.JieJueFangFa,ps.Name from RepairS rs,ProductSInRepairS pirs,ProductS ps" +
                   " where rs.CorSId="+customerSId+" and pirs.OrderNO like rs.OrderNO and pirs.ProductSId=ps.Id order by rs.Id DESC ");
               foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows(sb.ToString()))
               {
                   RepairRecord sr = new RepairRecord();
                   sr.StartDateTime = DateTime.ParseExact(row["StartDateTime"] as string, FCNS.Data.DbDefine.dateTimeFormat, null);
                   sr.Name = row["Name"] as string;
                   sr.Quantity = (double)row["Quantity"];
                   sr.GuZhangMiaoShu = row["GuZhangMiaoShu"] as string;
                   sr.JieJueFangFa = row["JieJueFangFa"] as string;

                   DateTime dt;
                   if (!DateTime.TryParseExact(row["EndDateTime"] as string, FCNS.Data.DbDefine.dateTimeFormat, null, System.Globalization.DateTimeStyles.None, out dt))
                       sr.EndDateTime = null;
                   else
                       sr.EndDateTime = dt;

                   record.Add(sr);
               }

               return record;
           }
           public DateTime StartDateTime
           { get; set; }
           public DateTime? EndDateTime
           { get; set; }
           public string Name { get; set; }
           public double Quantity { get; set; }
           public string GuZhangMiaoShu { get; set; }
           public string JieJueFangFa { get; set; }
       }

       internal sealed class WeiBaoRecord
       {
           /// <summary>
           /// 获取客户的维保记录
           /// </summary>
           /// <returns></returns>
           internal static IEnumerable<WeiBaoRecord> GetList(Int64 customerSId)
           {
               List<WeiBaoRecord> record = new List<WeiBaoRecord>();
               StringBuilder sb = new StringBuilder("select wbs.StartDateTime,wbs.EndDateTime,wbs.Money,wbs.Note,us.Name,un.Name from WeiBaoS wbs,UserS us,UniqueS un where wbs.CorSId="+customerSId+
                   " and wbs.OperaterId=us.Id and wbs.UniqueSId=un.Id ORDER BY wbs.Id DESC ");
               foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows(sb.ToString()))
               {
                   WeiBaoRecord sr = new WeiBaoRecord();
                   sr.StartDateTime = DateTime.ParseExact(row["StartDateTime"] as string, FCNS.Data.DbDefine.dateTimeFormat, null);
                   sr.EndDateTime = DateTime.ParseExact(row["EndDateTime"] as string, FCNS.Data.DbDefine.dateTimeFormat, null);
                   sr.Money = (double)row["Money"];
                   sr.OperaterName = row["Name"] as string;
                   sr.Note = row["Note"] as string;
                   sr.FlagText = row["Name1"] as string;
                   record.Add(sr);
               }

               return record;
           }

           public string FlagText { get; set; }
           public DateTime StartDateTime
           { get; set; }
           public DateTime EndDateTime
           { get; set; }
           public double Money { get; set; }
           public string OperaterName
           {
               get;
               set;
           }
           public string Note
           {
               get;
               set;
           }
       }
    }
}
