﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace AfterSaleServiceS
{
    /// <summary>
    /// FanKui.xaml 的交互逻辑
    /// </summary>
    public partial class FanKui : Window
    {
        Table.AfterSaleServiceS order;
        public FanKui(Table.AfterSaleServiceS services)
        {
            InitializeComponent();

            order = services;
            System.Diagnostics.Debug.Assert(order != null);

            InitVar();
            buttonOK.Click += (ss, ee) =>
                {
                    order.HuiFangDateTime = (DateTime)dateTimePickerHuiFangDateTime.Value;
                    order.HuiFangUserSId = (Int64)uCuserSelect1.SelectedValue;
                    order.FuWuFeiHeShi = (double)doubleUpDownShiShouJinE.Value;
                    order.KeHuPingJiaHeShi = (Int64)comboBoxKeHuPingJiaHeShi.SelectedValue;
                    order.KeHuYiJian = textBoxKeHuYiJian.Text;

                    Table.DataTableS.EnumDatabaseStatus result = order.Update((int)Table.AfterSaleServiceS.EnumEnable.回访反馈);
                    if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                        this.Close();
                    else
                        MessageWindow.Show(result.ToString());
                };
        }

        private void InitVar()
        { 
            doubleUpDownShiShouJinE.FormatString = "F" + Table.SysConfig.SysConfigParams.DecimalPlaces.ToString();

            dateTimePickerHuiFangDateTime.Value = DateTime.Now;
            labelFuWuFei.Content = order.FuWuFei;
            labelJiaoTongFei.Content = order.JiaoTongFei;
            labelKeHuPingJia.Content = order._FuWuPingJiaName;

            uCuserSelect1.Init(Table.UserS.EnumFlag.经办人); 
            uCuserSelect1.Text = Table.UserS.LoginUser.Name;

            doubleUpDownShiShouJinE.Value = order.JiaoTongFei + order.FuWuFei;

            comboBoxKeHuPingJiaHeShi.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.服务评价, Table.UniqueS.EnumEnable.启用);
            comboBoxKeHuPingJiaHeShi.DisplayMemberPath = "Name";
            comboBoxKeHuPingJiaHeShi.SelectedValuePath = "Id";
            comboBoxKeHuPingJiaHeShi.Text = order._FuWuPingJiaName;
        }
    }
}
