﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH.Utility.IO;
using DoubleH.Utility.Configuration;
using System.Collections.ObjectModel;

namespace AfterSaleServiceS
{
    /// <summary>
    /// ITwaiBao.xaml 的交互逻辑
    /// </summary>
    public partial class ITweiBao : Window
    {
        public ITweiBao()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

       static Table.WeiBaoS order = null;
      static  ObservableCollection<TempClass> allUploadFile = new ObservableCollection<TempClass>();

        public void Init(Table.WeiBaoS obj)
        {
            order = obj ?? new Table.WeiBaoS(Table.WeiBaoS.EnumFlag.外包维保);

            InitOrder();
        }

        private void InitVar()
        {
            uCuserSelect1.Init(Table.UserS.EnumFlag.经办人);
            uCUniqueSLevel.Init(Table.UniqueS.EnumFlag.服务级别定义);
            customerSelect1.Init(true);

            comboBoxItType.ItemsSource = Enum.GetNames(typeof(Table.WeiBaoS.EnumFlag));

            listViewFile.ItemsSource = allUploadFile;
            listViewFile.DisplayMemberPath = "FileName";
            listViewFile.SelectedValuePath = "FullFileName";
        }

        private void InitOrder()
        {
            labelTitle.Content = order.Flag.ToString() + "合同";

            customerSelect1.SelectedObjectId = order.CorSId;
            uCuserSelect1.SelectedValue = order.OperaterId;
            uCUniqueSLevel.SelectedValue = order.UniqueSId;
            textBoxNO.Text = order.OrderNO;
            textBoxNote.Text = order.Note;
            datePickerStart.SelectedDate = order.StartDateTime;
            datePickerEnd.SelectedDate = order.EndDateTime;
            doubleUpDownMoney.Value = order.Money;
            comboBoxItType.Text = order.Flag.ToString();

            dataGridObject.ItemsSource = order.ProductInWeiBaoSList;
            switch (order.Enable)
            {
                case Table.WeiBaoS.EnumEnable.评估:
                    buttonAgain.IsEnabled = false;
                    buttonStop.IsEnabled = false;
                    buttonSave.IsEnabled = false;
                    break;

                case Table.WeiBaoS.EnumEnable.合同生效:
                    buttonShengXiao.IsEnabled = false;
                    break;

                case Table.WeiBaoS.EnumEnable.合同终止:
                    buttonStop.IsEnabled = false;
                    buttonShengXiao.IsEnabled = false;
                    break;
            }

            GetUploadFile();
        }

        private void InitEvent()
        {
            dataGridObject.CellEditEnding += (ss, ee) => CheckCellEditValue(ee);
            customerSelect1.CorSChanged += (ee) => SelectCorS(ee);
            buttonStop.Click += (ss, ee) => ZuoFei();
            buttonAgain.Click += (ss, ee) => Again();
            buttonSave.Click += (ss, ee) => Save();
            buttonShengXiao.Click += (ss, ee) => ShengXiao();
            buttonAdd.Click += (ss, ee) => AddFile();
            buttonRemove.Click += (ss, ee) => RemoveFile();
            listViewFile.MouseDoubleClick += (ss, ee) => OpenFile();

            DoubleH.Utility.Net.NetUtility.LanClient.Commander.MessageProcessed += new EventHandler<FSLib.IPMessager.Entity.MessageEventArgs>(Commander_MessageProcessed);
            this.Closed += (ss, ee) => ThisClose();
        }

        private void ThisClose()
        {
            //如果这里不注销事件，它会一直递增的。
            DoubleH.Utility.Net.NetUtility.LanClient.Commander.MessageProcessed -= new EventHandler<FSLib.IPMessager.Entity.MessageEventArgs>(Commander_MessageProcessed);
        }

        private static void Commander_MessageProcessed(object sender, FSLib.IPMessager.Entity.MessageEventArgs e)
        {
            if (e.Message.Command == FSLib.IPMessager.Define.Consts.Commands.GetDirFiles)
            {
                string[] allfile = e.Message.NormalMsg.Split(';');
                if (allfile == null)
                    return;

                string pa = FCNS.Data.Utility.GetFilePathStart(Table.WeiBaoS.tableName, order.Id.ToString());
                foreach (string str in allfile)
                {
                    if (string.IsNullOrEmpty(str)||!str.Contains('.'))
                        continue;

                    allUploadFile.Add(new TempClass() { FullFileName = pa + str, FileName = str });
                }
            }
        }

        private void OpenFile()
        {
            if (listViewFile.SelectedItem == null)
                return;

            TempClass tc = listViewFile.SelectedItem as TempClass;
            System.Diagnostics.Process.Start(tc.FullFileName);
        }

        private void AddFile()
        {
            if (order.Id == -1)
            {
                MessageWindow.Show("合同未生效");
                return;
            }

            string file = FileTools.GetFile(true);
            if (string.IsNullOrEmpty(file) || order.Id == -1)
                return;

            if (allUploadFile.FirstOrDefault(f => f.FullFileName == file) != null)
            {
                MessageWindow.Show(file + " 文件已存在");
                return;
            }

            TempClass tc = new TempClass() { FullFileName = file, FileName = System.IO.Path.GetFileName(file) };
            DoubleH.Utility.Net.NetUtility.SaveFile(file, FCNS.Data.Table.SysConfig.SysConfigParams.Id + "\\" + "WeiBaoS\\" + order.Id, tc.FileName);
            GetUploadFile();//必须要这样,否则新增文件后如果立刻选择移除,就会删除了源文件
        }

        private void RemoveFile()
        {
            if (listViewFile.SelectedItem == null)
                return;

            TempClass tc = listViewFile.SelectedItem as TempClass;
            allUploadFile.Remove(tc);
            DoubleH.Utility.Net.NetUtility.RemoveFile(tc.FullFileName);
        }

        private void SelectCorS(Table.CorS cors)
        {
            order = new Table.WeiBaoS(Table.WeiBaoS.EnumFlag.外包维保);
            order.CorSId = cors.Id;
            InitOrder();
        }

        private void ZuoFei()
        {
            if (MessageWindow.Show("", "确定要终止合同吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            Table.DataTableS.EnumDatabaseStatus result = order.UpdateTo(Table.WeiBaoS.EnumEnable.合同终止);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void Again()
        {
            order = (Table.WeiBaoS)order.CloneObject(null);
            textBoxNO.Clear();
            datePickerStart.SelectedDate = datePickerEnd.SelectedDate;
            datePickerEnd.SelectedDate = datePickerEnd.SelectedDate.Value.AddYears(1);
            order.ProductInWeiBaoSList = (ObservableCollection<Table.ProductSInWeiBaoS>)dataGridObject.ItemsSource;
            buttonShengXiao.IsEnabled = true;
            buttonStop.IsEnabled = false;
            buttonSave.IsEnabled = false;
            buttonAgain.IsEnabled = false;
        }

        private void Save()
        {
            order.ProductInWeiBaoSList = (ObservableCollection<Table.ProductSInWeiBaoS>)dataGridObject.ItemsSource;
            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            result = order.Update();
            MessageWindow.Show(result.ToString());
        }

        private void ShengXiao()
        {
            if (doubleUpDownMoney.Value.Value == 0)
                if (MessageWindow.Show("", "确定合同金额为(0)吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;

            order.Flag = (Table.WeiBaoS.EnumFlag)Enum.Parse(typeof(Table.WeiBaoS.EnumFlag), comboBoxItType.Text);
            order.CorSId = customerSelect1.SelectedObjectId;
            order.OperaterId = uCuserSelect1.SelectedObjectId;

            order.Note = textBoxNote.Text;
            order.OrderNO = textBoxNO.Text;
            order.StartDateTime = datePickerStart.SelectedDate.Value;
            order.EndDateTime = datePickerEnd.SelectedDate.Value;
            order.UniqueSId = (Int64)uCUniqueSLevel.SelectedObjectId;
            order.ProductInWeiBaoSList = (ObservableCollection<Table.ProductSInWeiBaoS>)dataGridObject.ItemsSource;

            order.Money = doubleUpDownMoney.Value.Value;
            Table.DataTableS.EnumDatabaseStatus result;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.XuFei();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            TextBox textBox = ee.EditingElement as TextBox;
            if (textBox == null)
                return;

            string binding = ((Binding)((DataGridTextColumn)ee.Column).Binding).Path.Path;
            double d = 0;
            switch (binding)
            {
                case "Quantity":
                    if (!double.TryParse(textBox.Text, out d))
                        textBox.Undo();
                    else
                        if (d <= 0)
                            textBox.Undo();
                    break;
            }
        }

        private void GetUploadFile()
        {
            if (order.Id == -1)
                return;

            allUploadFile.Clear();
            string dir = Table.SysConfig.SysConfigParams.Id + "\\" + Table.WeiBaoS.tableName + "\\" + order.Id;
            string[] allfile = new string[] { };
            DoubleH.Utility.Net.NetUtility.GetFiles(dir );
            AddFile(allfile);
        }

        private void AddFile(string[] allfile)
        {
            if (allfile == null)
                return; 

            foreach (string str in allfile)
            {
                if (string.IsNullOrEmpty(str))
                    continue;

                allUploadFile.Add(new TempClass() { FullFileName = FCNS.Data.DbDefine.MediaFileDirForClient + str, FileName = str });
            }
        }
    }

    public class TempClass
    {
        public string FullFileName { get; set; }
        public string FileName { get; set; }
    }
}