﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;

namespace PosUtility
{
    public class PosUtilityExt : Plugin
    {
        PeiSongDan psd = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            switch (TableText)
            {
                case DoubleH.Utility.DataTableText.POS机器号:
                    break;

                case DoubleH.Utility.DataTableText.POS操作员:
                    PosStopSale wss = new PosStopSale();
                    wss.Init(PosStopSale.EnumFlag.Pos操作员, DataTable as Table.PosS, null);
                    wss.ShowDialog();
                    break;

                case DoubleH.Utility.DataTableText.POS禁售商品:
                    PosStopSale pss = new PosStopSale();
                    pss.Init(PosStopSale.EnumFlag.Pos禁售商品, null, DataTable as Table.ProductS);
                    pss.ShowDialog();
                    break;

                case DoubleH.Utility.DataTableText.POS请货单:
                    psd = new PeiSongDan();
                    psd.Init(DataTable as Table.PosRequestS);
                    psd.Owner = host.WorkAreaWindow;
                    psd.Show();
                    break;

                case DoubleH.Utility.DataTableText.POS配送单:
                    psd = new PeiSongDan();
                    psd.Init(DataTable as Table.PosRequestS);
                    psd.Owner = host.WorkAreaWindow;
                    psd.Show();
                    break;
            }
            return true;
        }

        public override void Terminate()
        {
            if (psd != null)
                psd = null;
        }
    }
}
