﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Collections.ObjectModel;
using DoubleH.Utility.Configuration;
using DoubleH.Utility;

namespace PosUtility
{
    /// <summary>
    /// QingHuoDan.xaml 的交互逻辑
    /// </summary>
    public partial class PeiSongDan : Window
    {
        public PeiSongDan()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.PosRequestS order = null;

        public void Init(Table.PosRequestS order)
        {
            this.order = order ?? new Table.PosRequestS() { Flag = Table.PosRequestS.EnumFlag.配送单 };
            comboBoxShopFlag.ItemsSource = Table.StoreS.GetList(true, Table.StoreS.EnumEnable.启用);
            comboBoxShopFlag.SelectedValuePath = "ShopFlag";
            comboBoxShopFlag.DisplayMemberPath = "Name";
            productSListInfo1.Init(DoubleHConfig.UserConfig.GetUserUIparams(DoubleH.Utility.DataTableText.配送单商品编辑));
            productSListInfo1.SetProductSType = ProductS.GetProductS.EnumProductS.可销售商品;
            productSListInfo1.MustHasStoreS = false;
            dataGridExt1.CanUserAddRows = false;
            InitOrder();
        }

        private void InitOrder()
        {
            comboBoxShopFlag.SelectedValue = order.ShopFlag;
            productSListInfo1.ItemsSource = order.ProductSList;

            buttonSave.IsEnabled = order.Enable == Table.PosRequestS.EnumEnable.评估;
            buttonShenHe.IsEnabled = order.Enable == Table.PosRequestS.EnumEnable.评估;
            productSListInfo1.IsReadOnly = order.Enable != Table.PosRequestS.EnumEnable.评估;
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => BuildOrder();
            buttonNew.Click += (ss, ee) => NewOrder();
            buttonShenHe.Click += (ss, ee) => ShenHeOrder();
            dataGridExt1.MouseDoubleClick += (ss, ee) => ChangedOrder();
            comboBoxShopFlag.SelectionChanged += (ss, ee) => ChangedShopFlag();
        }

        private void ShenHeOrder()
        {
            MessageBox.Show(order.ShenHe().ToString());
        }

        private void ChangedShopFlag()
        {
            string flag = null;
            if (comboBoxShopFlag.SelectedValue != null)
                flag = comboBoxShopFlag.SelectedValue.ToString();
            else if (!string.IsNullOrEmpty(order.ShopFlag))
                flag = order.ShopFlag;

            if (!string.IsNullOrEmpty(flag))
                dataGridExt1.ItemsSource = Table.PosRequestS.GetList(Table.PosRequestS.EnumFlag.请货单, flag, Table.PosRequestS.EnumEnable.审核);
        }

        private void NewOrder()
        {
            order = new Table.PosRequestS() { Flag = Table.PosRequestS.EnumFlag.配送单 };
            InitOrder();
        }

        private void BuildOrder()
        {
            if (MessageWindow.Show("", "确定要保存配送单吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            order.OrderDateTime = DateTime.Now;
            if (comboBoxShopFlag.SelectedItem != null)
                order.ShopFlag = comboBoxShopFlag.SelectedValue.ToString();

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                MessageWindow.Show(result.ToString());
            //else
            //    orderList.Add(new TempClass(prs));
        }

        private void ChangedOrder()
        {
            if (dataGridExt1.SelectedItem == null)
                return;

            order = dataGridExt1.SelectedItem as Table.PosRequestS;
            order.Id = -1;
            order.Flag = Table.PosRequestS.EnumFlag.配送单;
            order.RelatedOrderNO = order.OrderNO;
            productSListInfo1.ItemsSource = order.ProductSList;
        }
    }
}
