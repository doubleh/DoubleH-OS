﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Collections; 
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Reflection;
using DoubleH.Utility.Configuration;
using DoubleH.Utility;
using System.Diagnostics;

namespace PosUtility
{
    /// <summary>
    /// QingHuoDan.xaml 的交互逻辑
    /// </summary>
    public partial class QingHuoDan : Window
    {
        public QingHuoDan()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.PosRequestS order = null;
        ObservableCollection<TempClass> orderList = new ObservableCollection<TempClass>();
        Table.PosS pos = null;

        public void Init(Table.PosS pos)
        {
            Debug.Assert(pos != null);
            this.pos = pos;

            InitVar();
            NewOrder();
        }

        private void InitVar()
        {
            productSListInfo1.Init(DoubleHConfig.UserConfig.GetUserUIparams(DoubleH.Utility.DataTableText.请货单商品编辑));
            productSListInfo1.SetProductSType = ProductS.GetProductS.EnumProductS.询价单商品;
            productSListInfo1.StoreS = Table.StoreS.GetObject(pos.StoreSId);

            dataGridExt1.ItemsSource = orderList;
            foreach (Table.PosRequestS ps in Table.PosRequestS.GetList(Table.PosRequestS.EnumFlag.请货单, pos._ShopFlag, Table.PosRequestS.EnumEnable.评估))
                orderList.Add(new TempClass(ps));

            foreach (Table.PosRequestS ps in Table.PosRequestS.GetList(Table.PosRequestS.EnumFlag.配送单, pos._ShopFlag, Table.PosRequestS.EnumEnable.入账))
                orderList.Add(new TempClass(ps));
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => BuildOrder();
            buttonNew.Click += (ss, ee) => NewOrder();
            dataGridExt1.MouseDoubleClick += (ss, ee) => ChangedOrder();
        }

        private void NewOrder()
        {
            order = new Table.PosRequestS() { Flag = Table.PosRequestS.EnumFlag.请货单 };
            productSListInfo1.ItemsSource = order.ProductSList;
            productSListInfo1.IsReadOnly = order.Enable != Table.PosRequestS.EnumEnable.评估;
        }

        private void BuildOrder()
        {
            if (MessageWindow.Show("", "确定要保存请货单吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            order.OrderDateTime = DateTime.Now;
            order.ShopFlag = pos._ShopFlag;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
            {
                result = order.Insert();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    orderList.Add(new TempClass(order));
            }
            else
                result = order.Update();
        }

        private void ChangedOrder()
        {
            if (dataGridExt1.SelectedItem == null)
                return;

            order = ((TempClass)dataGridExt1.SelectedItem).Order;
            productSListInfo1.ItemsSource = order.ProductSList;
            productSListInfo1.IsReadOnly = order.Enable != Table.PosRequestS.EnumEnable.评估;
        }

        private void buttonShenHe(object sender, RoutedEventArgs e)
        {
            TempClass tc = dataGridExt1.SelectedItem as TempClass;
            if (tc == null)
                return;

            Table.DataTableS.EnumDatabaseStatus result = tc.Order.ShenHe();
            if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                MessageWindow.Show(result.ToString());
            else
            {
                tc.CanShenHe = false;
                tc.CanZuoFei = false;
            }
            productSListInfo1.IsReadOnly = order.Enable != Table.PosRequestS.EnumEnable.评估;
        }

        private void buttonRuZhang(object sender, RoutedEventArgs e)
        {
            TempClass tc = dataGridExt1.SelectedItem as TempClass;
            if (tc == null)
                return;

            Debug.Assert(tc.Order.Flag == Table.PosRequestS.EnumFlag.配送单);

            Table.DataTableS.EnumDatabaseStatus result = tc.Order.ShouHuo();
            if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                MessageWindow.Show(result.ToString());
            else
                tc.CanRuZhang = false;
        }

        private void buttonZuoFei(object sender, RoutedEventArgs e)
        {
            TempClass tc = dataGridExt1.SelectedItem as TempClass;
            if (tc == null)
                return;

            Table.DataTableS.EnumDatabaseStatus result = tc.Order.ZuoFei();
            if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                MessageWindow.Show(result.ToString());
            else
            {
                tc.CanShenHe = false;
                tc.CanZuoFei = false;
            }
            productSListInfo1.IsReadOnly = order.Enable != Table.PosRequestS.EnumEnable.评估;
        }


        class TempClass : INotifyPropertyChanged
        {
            public TempClass(Table.PosRequestS order)
            {
                Order = order;
                OrderNO = order.OrderNO;
                Enable = order.Enable;
                Flag = order.Flag;
                CanShenHe = (order.Enable == Table.PosRequestS.EnumEnable.评估);
                if (order.Flag == Table.PosRequestS.EnumFlag.请货单)
                    CanRuZhang = false;
                else
                CanRuZhang = (order.Enable == Table.PosRequestS.EnumEnable.入账);

                CanZuoFei = (order.Enable == Table.PosRequestS.EnumEnable.评估);
            }

            public string OrderNO { get; set; }

            Table.PosRequestS.EnumEnable enable = Table.PosRequestS.EnumEnable.评估;
            public Table.PosRequestS.EnumEnable Enable
            {
                get { return enable; }
                set { enable = value; NotifyPropertyChanged("Enable"); }
            }

            public Table.PosRequestS.EnumFlag Flag { get; set; }

            bool canShenHe = true;
            public bool CanShenHe { get { return canShenHe; } set { canShenHe = value; NotifyPropertyChanged("CanShenHe"); } }

            bool canRuZhang = true;
            public bool CanRuZhang { get { return canRuZhang; } set { canRuZhang = value; NotifyPropertyChanged("CanRuZhang"); } }

            bool canZuoFei = true;
            public bool CanZuoFei { get { return canZuoFei; } set { canZuoFei = value; NotifyPropertyChanged("CanZuoFei"); } }

            public Table.PosRequestS Order { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
            internal void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}