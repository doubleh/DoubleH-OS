﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace PosUtility
{
    /// <summary>
    /// WindowStopSale.xaml 的交互逻辑
    /// </summary>
    public partial class PosStopSale : Window
    {
        public enum EnumFlag
        {
            Pos操作员,
            Pos禁售商品
        }

        public PosStopSale()
        {
            InitializeComponent();
        }

        EnumFlag flag;
        Table.PosS pos = null;
        Table.ProductS product = null;
        public void Init(EnumFlag flag, Table.PosS pos, Table.ProductS product)
        {
            this.flag = flag;
            this.pos = pos;
            this.product = product;

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            switch (flag)
            {
                case EnumFlag.Pos禁售商品:
                    checkListBoxPos.DisplayMemberPath = "PosNO";
                    checkListBoxPos.ValueMemberPath = "PosNO";
                    checkListBoxPos.ItemsSource = Table.PosS.GetList(Table.PosS.EnumEnable.启用);
                    break;

                case EnumFlag.Pos操作员:
                    this.Title = "收银员编辑";
                    groupBox1.Header = "收银员列表";
                    checkListBoxPos.DisplayMemberPath = "Name";
                    checkListBoxPos.ValueMemberPath = "Id";
                    checkListBoxPos.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.POS操作员);
                    if (this.pos._UserList == null)
                        return;

                    foreach (Table.UserS us in pos._UserList)
                        foreach (object oo in checkListBoxPos.Items)
                            if (((Table.UserS)oo).Id == us.Id)
                                checkListBoxPos.SelectedItems.Add(oo);

                    break;

                //case EnumFlag.Bar禁售商品:
                //    checkListBoxPos.DisplayMemberPath = "PosNO";
                //    checkListBoxPos.ValueMemberPath = "PosNO";
                //    checkListBoxPos.ItemsSource = Table.PosS.GetList(Table.PosS.EnumFlag.酒吧, Table.PosS.EnumEnable.启用);
                //    break;

                //case EnumFlag.Bar操作员:
                //    this.Title = "收银员编辑";
                //    groupBox1.Header = "收银员列表";
                //    checkListBoxPos.DisplayMemberPath = "Name";
                //    checkListBoxPos.ValueMemberPath = "Id";
                //    checkListBoxPos.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.Bar操作员);

                //    if (this.pos._UserList == null)
                //        return;

                //    foreach (Table.UserS us in pos._UserList)
                //        foreach (object oo in checkListBoxPos.Items)
                //            if (((Table.UserS)oo).Id == us.Id)
                //                checkListBoxPos.SelectedItems.Add(oo);

                //    break;

            }
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
        }

        private void Save()
        {
            switch (flag)
            {
                case EnumFlag.Pos禁售商品:
                    if (product != null)
                        product.UpdatePosCanNotUseList(checkListBoxPos.SelectedValue);
                    break;

                case EnumFlag.Pos操作员:
                    if (pos != null)
                        pos.UpdateUserList(checkListBoxPos.SelectedValue);
                    break;
            }
            this.Close();
        }
    }
}