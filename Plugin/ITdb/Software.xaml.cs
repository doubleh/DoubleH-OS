﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace ItDbS
{
    /// <summary>
    /// Software.xaml 的交互逻辑
    /// </summary>
    public partial class Software : Window
    {
        public Software()
        {
            InitializeComponent();
        }

        Table.ItDbS order = null;
        public void Init(Table.ItDbS obj)
        {
            this.order = obj ?? new Table.ItDbS() { Flag = Table.ItDbS.EnumFlag.软件 };
           
            InitVar();
            InitOrder();
            InitEvent();
        }

        private void InitVar()
        {
            uCUniqueS1.Init(Table.UniqueS.EnumFlag.品牌);

            checkComboBoxHardware.ItemsSource = Table.ItDbS.GetList(Table.ItDbS.EnumFlag.硬件, Table.ItDbS.EnumEnable.激活, Table.ItDbS.EnumEnable.维修);
            checkComboBoxHardware.DisplayMemberPath = "Name";
            checkComboBoxHardware.ValueMemberPath = "Id";
            checkComboBoxHardware.SelectedMemberPath = "Id";

            checkListBoxSign.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.标记, Table.UniqueS.EnumEnable.启用);
            checkListBoxSign.DisplayMemberPath = "Name";
            checkListBoxSign.ValueMemberPath = "Id";
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) => SaveOrder();
        }

        private void SaveOrder()
        {
            if (radioButtonFree.IsChecked.Value)
                order.License = Table.ItDbS.EnumLicense.无限制;
            else if (radioButtonCore.IsChecked.Value)
                order.License = Table.ItDbS.EnumLicense.模块;
            else
                order.License = Table.ItDbS.EnumLicense.站点;

            order.Name = textBoxName.Text;
            order.Version = textBoxVersion.Text;
            order.Quantity = integerUpDownQuantity.Value.Value;
            order.PurchaseDate = datePicker1.SelectedDate;
            order.Code = textBoxCode.Text;

            order.RelatedHardware = checkComboBoxHardware.SelectedValue;
            order.PurchaseOrderNO = textBoxRelatedOrderNO.Text;
            order.BrandId = uCUniqueS1.SelectedObjectId;

            order.Gpl = textBoxGPL.Text;
            order.Note = textBoxNote.Text;

            order.Sign = checkListBoxSign.SelectedValue;
            order.UploadFile = uCImage1.UploadFileName;

            Table.DataTableS.EnumDatabaseStatus result;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
            {
                uCImage1.UpdateImage(Table.ItDbS.tableName);
                this.Close();
            }
            else
                MessageWindow.Show(result.ToString());
        }

        private void InitOrder()
        {
            textBoxName.Text = order.Name;
            textBoxVersion.Text = order.Version;
            integerUpDownQuantity.Value = (int)order.Quantity;
            datePicker1.SelectedDate = order.PurchaseDate;
            textBoxCode.Text = order.Code;
            switch (order.License)
            {
                case Table.ItDbS.EnumLicense.无限制: radioButtonFree.IsChecked = true; break;
                case Table.ItDbS.EnumLicense.模块: radioButtonCore.IsChecked = true; break;
                case Table.ItDbS.EnumLicense.站点: radioButtonCPU.IsChecked = true; break;
            }
            checkComboBoxHardware.SelectedValue = order.RelatedHardware;
            textBoxRelatedOrderNO.Text = order.PurchaseOrderNO;
            uCUniqueS1.SelectedObjectId = order.BrandId;
        
            textBoxGPL.Text = order.Gpl;
            textBoxNote.Text = order.Note;
 
            checkListBoxSign.SelectedValue = order.Sign;
            uCImage1.ImageFile = order._ImageFile;
        }
    }
}