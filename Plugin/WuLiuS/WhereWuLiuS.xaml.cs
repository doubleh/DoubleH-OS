﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Reflection;

namespace WuLiuS
{
    /// <summary>
    /// WhereWuLiuS.xaml 的交互逻辑
    /// </summary>
    public partial class WhereWuLiuS : Window
    {
        public WhereWuLiuS()
        {
            InitializeComponent();
        }

        public void Init(string company, string orderNO)
        {
            this.Title = company + " - " + orderNO;
            if (Table.WuLiuS.WuLiuCompany.Keys.Contains(company))
            {
                frame1.Source = new Uri("http://m.kuaidi100.com/index_all.html?type=" + Table.WuLiuS.WuLiuCompany[company].Trim() + "&postid=" + orderNO.Trim());
            }
            else
            {
                MessageWindow.Show("暂不支持 " + company);
            }
        }
    }
}
