﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using System.Windows;
using DoubleH.Utility;
using Table = FCNS.Data.Table;

namespace WuLiuS
{
    class WuLiuSExt : Plugin
    {
        MainWindow window = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            Table.WuLiuS order = DataTable as Table.WuLiuS;
            switch (TableText)
            {
                case DataTableText.仓库物流跟踪:
                case DataTableText.批发物流跟踪:
                case DataTableText.采购物流跟踪:
                    //if (order!=null&&order.Id > 0)
                    //{
                    //    MessageWindow.Show("你选择的单号已经发布物流,不能重复添加.");
                    //    break;
                    //}

                    window = new MainWindow();
                    window.Init(TableText,DataTable as Table.WuLiuS);
                    window.ShowDialog();
                    break;

                //case DataTableText.物流查询:
                //    if (order == null||string.IsNullOrEmpty(order._Company))
                //        break;
                //    if (!Table.WuLiuS.WuLiuCompany.Keys.Contains(order._Company))
                //        break;

                //    search = new WhereWuLiuS();
                //    search.Init(order._Company,order.OrderNO);
                //        search.ShowDialog();
                //    break;
            }

            Terminate();
            return true;
        }

        public override void Terminate()
        {
            if (window != null)
                window.Close();

            //if (search != null)
            //    search.Close();
        }
    }
}
