﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using System.Windows;
using DoubleH.Utility;
using DoubleH;

namespace StoreS
{
    public class StoreSExt : Plugin
    {
        MainWindow mainWin = null;
        StoreSIO ioWin = null;
        DiaoBo diaoBo = null;
        KuCunTiaoJia kuCun = null;
        PanDian panDian = null;
        IPluginHost ihost = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            ihost = host;
            switch (TableText)
            {
                case DataTableText.仓库单据入库单:
                case DataTableText.仓库单据出库单:
                case DataTableText.非采购入库单:
                case DataTableText.非销售出库单:
                    ioWin = new StoreSIO(host);
                    Table.StoreOrderS s1 = DataTable as Table.StoreOrderS;
                    ioWin.Init(s1, TableText);
                    ioWin.Owner = host.WorkAreaWindow;
                    ioWin.Show();
                    break;

                case DataTableText.盘点单:
                    panDian = new PanDian();
                    panDian.Init(DataTable as Table.CheckStoreS);
                    panDian.Owner = host.WorkAreaWindow;
                    panDian.Show();
                    break;

                case DataTableText.调拨单:
                    diaoBo = new DiaoBo();
                    diaoBo.Init(DataTable as Table.AllocationS);
                    diaoBo.Owner = host.WorkAreaWindow;
                    diaoBo.Show();
                    break;

                case DataTableText.库存调价单:
                    kuCun = new KuCunTiaoJia();
                    kuCun.Init(DataTable as Table.AdjustAveragePriceS);
                    kuCun.ShowDialog();
                    break;

                case DataTableText.仓库:
                case DataTableText.连锁门店:
                    mainWin = new MainWindow();
                    mainWin.Init(DataTable as Table.StoreS, TableText);
                    mainWin.ShowDialog();
                    break;

                case DataTableText.仓库商品出库单:
                case DataTableText.仓库商品入库单:
                    ioWin = new StoreSIO(host);
                    Table.ProductSIO product = (DataTable as Table.ProductSIO);
                    Debug.Assert(product != null);
                    Table.StoreOrderS s2 = Table.StoreOrderS.GetObject(product.RelatedOrderNO);
                    ioWin.Init(s2, TableText);
                    ioWin.Owner = host.WorkAreaWindow;
                    ioWin.Show();
                    break;
            }

            return true;
        }

        public override void Terminate()
        {
            if (mainWin != null)
                mainWin.Close();

            if (ioWin != null)
                ioWin.Close();

            if (panDian != null)
                panDian.Close();

            if (kuCun != null)
                kuCun.Close();

            if (diaoBo != null)
                diaoBo.Close();
        }
    }
}