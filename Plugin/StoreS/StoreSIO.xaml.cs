﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH.Utility.Configuration;

namespace StoreS
{
    /// <summary>
    /// StoreSIO.xaml 的交互逻辑
    /// </summary>
    public partial class StoreSIO : Window
    {
        public StoreSIO(DoubleH.Plugins.IPluginHost host)
        {
            InitializeComponent();

            System.Diagnostics.Debug.Assert(host != null);
        }

        Table.StoreOrderS order = null;
        DataTableText tableText;

        public void Init(Table.StoreOrderS obj, DataTableText tableText)
        {
            order = obj;
            this.tableText = tableText;
            labelTitle.Content = this.Title = tableText.ToString();
            userSelect1.Init(Table.UserS.EnumFlag.经办人);
            productSListInfoObject.Init(DoubleHConfig.UserConfig.GetUserUIparams(DataTableText.仓库单据商品编辑));
            InitOrder();
            InitEvent();
            if (Table.UserS.LoginUser.GetAuthority(tableText) < Table.UserS.EnumAuthority.作废)
                buttonDelete.Visibility = Visibility.Hidden;
        }

        private void InitOrder()
        {
            switch (tableText)
            {
                case DataTableText.非采购入库单 :
                      order = order ?? new Table.StoreOrderS(Table.StoreOrderS.EnumFlag.入库单);
                InitOrder2();
                break;

                case DataTableText.非销售出库单:
                     order = order ?? new Table.StoreOrderS(Table.StoreOrderS.EnumFlag.出库单);
                InitOrder2();
                break;

                case DataTableText.仓库单据入库单:
                case DataTableText.仓库单据出库单:
                case DataTableText.仓库商品出库单:
                case DataTableText.仓库商品入库单:
                order = order ?? new Table.StoreOrderS(Table.StoreOrderS.EnumFlag.出库单);
                InitOrder1();
                break;
            }

            textBoxStoreAddress.Text = order.Address;

            buttonSave.IsEnabled = order.Enable == Table.StoreOrderS.EnumEnable.评估;
            buttonDelete.IsEnabled = buttonSave.IsEnabled;
        }
        //非正常出入库
        private void InitOrder2()
        {
            if (order.Enable == Table.StoreOrderS.EnumEnable.审核)
            {
                productSListInfoObject.ShowBodyContexMenu = false;
                productSListInfoObject.ShowSumMoney = false;
                productSListInfoObject.IsReadOnly = true;
                corSelect1.IsEnabled = false;
                textBoxNoteNO.IsEnabled = false;
                dateTimeUpDownDaoHuo.IsEnabled = false;
            }

            if( tableText == DataTableText.非采购入库单 )
            {
                productSListInfoObject.SetProductSType = ProductS.GetProductS.EnumProductS.可采购商品;
                productSListInfoObject.MustHasStoreS = false;
                productSListInfoObject.SetProductSType = ProductS.GetProductS.EnumProductS.可采购商品;
                corSelect1.Init(false);
            }
            else
            {
                productSListInfoObject.SetProductSType =ProductS.GetProductS.EnumProductS.可销售商品;
                productSListInfoObject.MustHasStoreS = true;
                productSListInfoObject.SetProductSType = ProductS.GetProductS.EnumProductS.库存调价单商品;

                labelDaoHuo.Content = "发货日期:";
                corSelect1.Init(true);
            }
            corSelect1.SelectedObjectId = order.CorSId;

            label1OrderNo.Content = order.OrderNO;
            dateTimeUpDownKaiDan.Value = order.OrderDateTime;
            textBoxNote.Text = order.Note;
            uCStoreS1.SelectedObjectId = order.StoreSId;
            productSListInfoObject.ItemsSource = order.ProductSList;
        }

        private void InitOrder1()
        {
            if (order.Flag!=Table.StoreOrderS.EnumFlag.入库单)
            {
                labelDaoHuo.Content = "发货日期:";
            }

            label1OrderNo.Content = order.OrderNO;
            dateTimeUpDownKaiDan.Value = order.OrderDateTime;
            dateTimeUpDownDaoHuo.Value = DateTime.Now;

            textBoxNoteNO.Text = order.NoteNO;
            textBoxNote.Text = order.Note;

            if (order.Flag != Table.StoreOrderS.EnumFlag.入库单)
                corSelect1.Init(false);
            else
                corSelect1.Init(false);
        
            corSelect1.SelectedObjectId = order.CorSId;
            uCStoreS1.SelectedObjectId = order.StoreSId;

            productSListInfoObject.ItemsSource = order.ProductSList;

            productSListInfoObject.ShowBodyContexMenu = false;
            productSListInfoObject.ShowSumMoney = false;
            productSListInfoObject.IsReadOnly = true;
            corSelect1.IsEnabled = false;
            dateTimeUpDownDaoHuo.IsEnabled = false;
            dateTimeUpDownKaiDan.IsEnabled = false;
            userSelect1.IsEnabled = false;
        }

        private void InitEvent()
        {
            buttonPre.Click += (ss, ee) =>  OrderPre(); 
            buttonNext.Click += (ss, ee) =>  OrderNext();
            buttonDelete.Click += (ss, ee) =>  OrderDelete(); 
            buttonSave.Click += (ss, ee) =>  Save();
            buttonPrint.Click += (ss, ee) => {
                if(buttonSave.IsEnabled)
                    MessageWindow.Show("单据入账后才可以打印");
                else
                    DoubleH.Utility.Print.PrintForm.ShowPrint(order, tableText,"仓库\\仓库出入单.frx");
            };
            uCStoreS1.SelectedObjectEvent += (ss) => productSListInfoObject.StoreS = ss; 
        }

        private void Save()
        {
            Table.DataTableS.EnumDatabaseStatus result;
            order.StoreSId = uCStoreS1.SelectedObjectId;
            if (tableText != DataTableText.非采购入库单 && tableText != DataTableText.非销售出库单)
            {
                order.NoteNO = textBoxNoteNO.Text;
                result = order.ShenHe();
            }
            else
            {
                order.OrderDateTime = dateTimeUpDownKaiDan.Value.Value;
                order.CorSId = corSelect1.SelectedObjectId;

                result = order.Insert();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    result = order.ShenHe();
            }
            MessageBox.Show(result.ToString());

            if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                return;

            buttonSave.IsEnabled = false;
        }

        private void OrderPre()
        {
            Table.StoreOrderS p = Table.StoreOrderS.GetPreObject(order.Id, order.Flag);
            if (p == null)
                MessageWindow.Show("没有上一条记录了");
            else
            {
                order = p;
                InitOrder();
            }
        }

        private void OrderNext()
        {
            FCNS.Data.Table.StoreOrderS p = Table.StoreOrderS.GetNextObject(order.Id, order.Flag);

            if (p == null)
                MessageWindow.Show("没有下一条记录了");
            else
            {
                order = p;
                InitOrder();
            }
        }

        private void OrderDelete()
        {
            if (MessageWindow.Show("", "确定要删除此单据吗", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Table.DataTableS.EnumDatabaseStatus result = order.ZuoFei();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    this.Close();
                else
                    MessageWindow.Show(result.ToString());
            }
        }
    }
}