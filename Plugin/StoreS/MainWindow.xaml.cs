﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH;
using System.Diagnostics;

namespace StoreS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Table.StoreS order = null;

        public void Init(Table.StoreS obj, DataTableText tableText)
        {
            switch (tableText)
            {
                case DataTableText.仓库:
                    textBoxShopFlag.IsEnabled = false;
                    order = obj ?? new Table.StoreS() { Flag = Table.StoreS.EnumFlag.仓库 };
                    break;

                case DataTableText.连锁门店:
                    this.Title = "连锁门店";
                    labelName.Content = "门店名称";
                    checkBoxIsDefault.IsEnabled = false;
                    order = obj ?? new Table.StoreS() { Flag = Table.StoreS.EnumFlag.门店 };
                    break;

                default: Debug.Assert(false); break;
            }

            textBoxName.Text = order.Name;
            textBoxAddress.Text = order.Address;
            textBoxNote.Text = order.Note;
            checkBoxEnable.IsChecked = order.Enable == Table.StoreS.EnumEnable.停用 ? true : false;
            checkBoxIsDefault.IsChecked = order.IsDefault;
            textBoxShopFlag.Text = order.ShopFlag;
            textBoxName.Focus();
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (checkBoxEnable.IsChecked.Value)
                result = order.ZuoFei();
            else
                SaveOrder(result);

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void SaveOrder(Table.DataTableS.EnumDatabaseStatus result)
        {
            order.Note = textBoxNote.Text;
            order.Name = textBoxName.Text;
            order.Address = textBoxAddress.Text;
            order.Enable = checkBoxEnable.IsChecked.Value ? Table.StoreS.EnumEnable.停用 : Table.StoreS.EnumEnable.启用;
            order.IsDefault = checkBoxIsDefault.IsChecked.Value;
            order.ShopFlag = textBoxShopFlag.Text;

            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();
        }
    }
}