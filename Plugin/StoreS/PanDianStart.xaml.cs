﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Data;

namespace StoreS
{
    /// <summary>
    /// PanDianStart.xaml 的交互逻辑
    /// </summary>
    public partial class PanDianStart : Window
    {
        public PanDianStart()
        {
            InitializeComponent();

            InitEvent();
        }

        DateTime? orderDateTime = null;
        public DateTime? OrderDateTime
        {
            get { return orderDateTime; }
            set { orderDateTime = value; }
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>   PanDian();  
        }

        private void PanDian()
        {
            if (!dateTimeUpDownKaiDan.Value.HasValue)
            {
                MessageWindow.Show("请选择日期");
                return;
            }

            orderDateTime = dateTimeUpDownKaiDan.Value;
            this.Close();
        }
    }
}
