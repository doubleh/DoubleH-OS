﻿namespace StoreS
{
    partial class MainIO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainIO));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxNoteNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelAddressValue = new System.Windows.Forms.Label();
            this.labelDateTimeValue = new System.Windows.Forms.Label();
            this.labelSupplierValue = new System.Windows.Forms.Label();
            this.labelStoreValue = new System.Windows.Forms.Label();
            this.labelRelatedNoValue = new System.Windows.Forms.Label();
            this.labelRequestNo = new System.Windows.Forms.Label();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelObject = new System.Windows.Forms.Label();
            this.labelOrderNO = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelDateTime = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonPre = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonZuoFei = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonShenHe = new System.Windows.Forms.Button();
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumnQuantity = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn8 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Controls.Add(this.textBoxNoteNo);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.labelAddressValue);
            this.panel1.Controls.Add(this.labelDateTimeValue);
            this.panel1.Controls.Add(this.labelSupplierValue);
            this.panel1.Controls.Add(this.labelStoreValue);
            this.panel1.Controls.Add(this.labelRelatedNoValue);
            this.panel1.Controls.Add(this.labelRequestNo);
            this.panel1.Controls.Add(this.textBoxNote);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.labelObject);
            this.panel1.Controls.Add(this.labelOrderNO);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.labelDateTime);
            this.panel1.Controls.Add(this.labelTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(906, 133);
            this.panel1.TabIndex = 0;
            // 
            // textBoxNoteNo
            // 
            this.textBoxNoteNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxNoteNo.Location = new System.Drawing.Point(625, 58);
            this.textBoxNoteNo.Name = "textBoxNoteNo";
            this.textBoxNoteNo.Size = new System.Drawing.Size(270, 16);
            this.textBoxNoteNo.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(549, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 25;
            this.label3.Text = "配送单据:";
            // 
            // labelAddressValue
            // 
            this.labelAddressValue.AutoSize = true;
            this.labelAddressValue.Location = new System.Drawing.Point(335, 107);
            this.labelAddressValue.Name = "labelAddressValue";
            this.labelAddressValue.Size = new System.Drawing.Size(14, 14);
            this.labelAddressValue.TabIndex = 24;
            this.labelAddressValue.Text = "?";
            // 
            // labelDateTimeValue
            // 
            this.labelDateTimeValue.AutoSize = true;
            this.labelDateTimeValue.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelDateTimeValue.Location = new System.Drawing.Point(84, 57);
            this.labelDateTimeValue.Name = "labelDateTimeValue";
            this.labelDateTimeValue.Size = new System.Drawing.Size(14, 14);
            this.labelDateTimeValue.TabIndex = 11;
            this.labelDateTimeValue.Text = "?";
            // 
            // labelSupplierValue
            // 
            this.labelSupplierValue.AutoSize = true;
            this.labelSupplierValue.Location = new System.Drawing.Point(84, 82);
            this.labelSupplierValue.Name = "labelSupplierValue";
            this.labelSupplierValue.Size = new System.Drawing.Size(14, 14);
            this.labelSupplierValue.TabIndex = 22;
            this.labelSupplierValue.Text = "?";
            // 
            // labelStoreValue
            // 
            this.labelStoreValue.AutoSize = true;
            this.labelStoreValue.Location = new System.Drawing.Point(84, 107);
            this.labelStoreValue.Name = "labelStoreValue";
            this.labelStoreValue.Size = new System.Drawing.Size(14, 14);
            this.labelStoreValue.TabIndex = 21;
            this.labelStoreValue.Text = "?";
            // 
            // labelRelatedNoValue
            // 
            this.labelRelatedNoValue.AutoSize = true;
            this.labelRelatedNoValue.Location = new System.Drawing.Point(335, 82);
            this.labelRelatedNoValue.Name = "labelRelatedNoValue";
            this.labelRelatedNoValue.Size = new System.Drawing.Size(14, 14);
            this.labelRelatedNoValue.TabIndex = 20;
            this.labelRelatedNoValue.Text = "?";
            // 
            // labelRequestNo
            // 
            this.labelRequestNo.AutoSize = true;
            this.labelRequestNo.Location = new System.Drawing.Point(259, 82);
            this.labelRequestNo.Name = "labelRequestNo";
            this.labelRequestNo.Size = new System.Drawing.Size(70, 14);
            this.labelRequestNo.TabIndex = 18;
            this.labelRequestNo.Text = "订单号码:";
            // 
            // textBoxNote
            // 
            this.textBoxNote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxNote.Location = new System.Drawing.Point(625, 82);
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.Size = new System.Drawing.Size(270, 16);
            this.textBoxNote.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(547, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 14);
            this.label9.TabIndex = 17;
            this.label9.Text = "备    注:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "仓    库:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(259, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 14);
            this.label7.TabIndex = 13;
            this.label7.Text = "交货地址:";
            // 
            // labelObject
            // 
            this.labelObject.AutoSize = true;
            this.labelObject.Location = new System.Drawing.Point(11, 82);
            this.labelObject.Name = "labelObject";
            this.labelObject.Size = new System.Drawing.Size(70, 14);
            this.labelObject.TabIndex = 11;
            this.labelObject.Text = "供 应 商:";
            // 
            // labelOrderNO
            // 
            this.labelOrderNO.AutoSize = true;
            this.labelOrderNO.Location = new System.Drawing.Point(335, 58);
            this.labelOrderNO.Name = "labelOrderNO";
            this.labelOrderNO.Size = new System.Drawing.Size(14, 14);
            this.labelOrderNO.TabIndex = 8;
            this.labelOrderNO.Text = "?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(259, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "单    号:";
            // 
            // labelDateTime
            // 
            this.labelDateTime.AutoSize = true;
            this.labelDateTime.Location = new System.Drawing.Point(11, 58);
            this.labelDateTime.Name = "labelDateTime";
            this.labelDateTime.Size = new System.Drawing.Size(70, 14);
            this.labelDateTime.TabIndex = 1;
            this.labelDateTime.Text = "开单日期:";
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ControlDark;
            this.labelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTitle.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.labelTitle.Size = new System.Drawing.Size(906, 51);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "标题";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.buttonPre);
            this.panel2.Controls.Add(this.buttonNext);
            this.panel2.Controls.Add(this.buttonZuoFei);
            this.panel2.Controls.Add(this.buttonPrint);
            this.panel2.Controls.Add(this.buttonShenHe);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 406);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(906, 51);
            this.panel2.TabIndex = 1;
            // 
            // buttonPre
            // 
            this.buttonPre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPre.Location = new System.Drawing.Point(12, 14);
            this.buttonPre.Name = "buttonPre";
            this.buttonPre.Size = new System.Drawing.Size(69, 25);
            this.buttonPre.TabIndex = 15;
            this.buttonPre.Text = "前一单";
            this.buttonPre.UseVisualStyleBackColor = true;
            this.buttonPre.Click += new System.EventHandler(this.buttonPre_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNext.Location = new System.Drawing.Point(87, 14);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(69, 25);
            this.buttonNext.TabIndex = 16;
            this.buttonNext.Text = "后一单";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonZuoFei
            // 
            this.buttonZuoFei.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonZuoFei.Location = new System.Drawing.Point(675, 14);
            this.buttonZuoFei.Name = "buttonZuoFei";
            this.buttonZuoFei.Size = new System.Drawing.Size(69, 25);
            this.buttonZuoFei.TabIndex = 4;
            this.buttonZuoFei.Text = "作废";
            this.buttonZuoFei.UseVisualStyleBackColor = true;
            this.buttonZuoFei.Click += new System.EventHandler(this.buttonZuoFei_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrint.Location = new System.Drawing.Point(825, 13);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(69, 25);
            this.buttonPrint.TabIndex = 5;
            this.buttonPrint.Text = "打印";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonShenHe
            // 
            this.buttonShenHe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShenHe.Location = new System.Drawing.Point(750, 13);
            this.buttonShenHe.Name = "buttonShenHe";
            this.buttonShenHe.Size = new System.Drawing.Size(69, 25);
            this.buttonShenHe.TabIndex = 3;
            this.buttonShenHe.Text = "过账";
            this.buttonShenHe.UseVisualStyleBackColor = true;
            this.buttonShenHe.Click += new System.EventHandler(this.buttonShenHe_Click);
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn1);
            this.objectListView1.AllColumns.Add(this.olvColumn5);
            this.objectListView1.AllColumns.Add(this.olvColumn2);
            this.objectListView1.AllColumns.Add(this.olvColumn3);
            this.objectListView1.AllColumns.Add(this.olvColumn4);
            this.objectListView1.AllColumns.Add(this.olvColumnQuantity);
            this.objectListView1.AllColumns.Add(this.olvColumn8);
            this.objectListView1.BackColor = System.Drawing.Color.White;
            this.objectListView1.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.SingleClick;
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn5,
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumnQuantity,
            this.olvColumn8});
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(0, 133);
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(906, 273);
            this.objectListView1.TabIndex = 9;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Name";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn1.IsEditable = false;
            this.olvColumn1.Text = "名称";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn1.Width = 182;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "Code";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.IsEditable = false;
            this.olvColumn5.Text = "编码";
            this.olvColumn5.Width = 81;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Barcode";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn2.IsEditable = false;
            this.olvColumn2.Text = "条码";
            this.olvColumn2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn2.Width = 183;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "UnitIdText";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn3.IsEditable = false;
            this.olvColumn3.Text = "单位";
            this.olvColumn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn3.Width = 65;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "Standard";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn4.IsEditable = false;
            this.olvColumn4.Text = "规格";
            this.olvColumn4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn4.Width = 82;
            // 
            // olvColumnQuantity
            // 
            this.olvColumnQuantity.AspectName = "TempQuantity";
            this.olvColumnQuantity.CellPadding = null;
            this.olvColumnQuantity.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumnQuantity.IsEditable = false;
            this.olvColumnQuantity.Text = "数量";
            this.olvColumnQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumnQuantity.Width = 69;
            // 
            // olvColumn8
            // 
            this.olvColumn8.AspectName = "Note";
            this.olvColumn8.CellPadding = null;
            this.olvColumn8.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn8.IsEditable = false;
            this.olvColumn8.Text = "备注";
            this.olvColumn8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olvColumn8.Width = 147;
            // 
            // MainIO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(906, 457);
            this.Controls.Add(this.objectListView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainIO";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PluginForm2";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        protected internal System.Windows.Forms.Label labelOrderNO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelDateTime;
        private System.Windows.Forms.Panel panel2;
        protected internal System.Windows.Forms.Label labelTitle;
        protected internal System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        protected internal System.Windows.Forms.Button buttonPre;
        protected internal System.Windows.Forms.Button buttonNext;
        protected internal System.Windows.Forms.Button buttonPrint;
        protected internal System.Windows.Forms.Button buttonShenHe;
        protected internal BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn8;
        protected internal System.Windows.Forms.Button buttonZuoFei;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        protected internal BrightIdeasSoftware.OLVColumn olvColumnQuantity;
        protected internal System.Windows.Forms.Label labelRequestNo;
        protected internal System.Windows.Forms.TextBox textBoxNoteNo;
        protected internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelAddressValue;
        private System.Windows.Forms.Label labelDateTimeValue;
        private System.Windows.Forms.Label labelSupplierValue;
        private System.Windows.Forms.Label labelStoreValue;
        protected internal System.Windows.Forms.Label labelRelatedNoValue;
        protected internal System.Windows.Forms.Label labelObject;

    }
}