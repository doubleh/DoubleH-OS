﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;

namespace ProjectS
{
    public class ProjectSExt : Plugin
    {
        MainWindow mw = null;
        ScheduleWindow sw = null;
        FeedbackWindow fw = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            switch (TableText)
            {
                case DoubleH.Utility.DataTableText.项目列表:
                    mw = new MainWindow();
                    mw.Init(DataTable as Table.ProjectS);
                    mw.Owner = host.WorkAreaWindow;
                    mw.Show();
                    break;

                case DoubleH.Utility.DataTableText.任务跟踪:
                    sw = new ScheduleWindow();
                    sw.Init(DataTable as Table.ScheduleInProjectS);
                    sw.Owner = host.WorkAreaWindow;
                    sw.Show();
                    break;

                case DoubleH.Utility.DataTableText.问题反馈:
                    fw = new FeedbackWindow();
                    fw.Init(DataTable as Table.FeedbackInProjectS);
                    fw.Owner = host.WorkAreaWindow;
                    fw.Show();
                    break;
            }

            return true;
        }

        public override void Terminate()
        {
            if (mw != null)
                mw.Close();
            if (sw != null)
                sw.Close();
            if (fw != null)
                fw.Close();
        }
    }
}
