﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace CarS
{
    public class CarSExt : Plugin
    {
        MainWindow window = null;
        OilWindow oil = null;
        DrivePlan drive = null;
        HealthWindow health = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            switch (TableText)
            {
                case DataTableText.车辆档案:
                    window = new MainWindow();
                    window.Init(DataTable as Table.CarS);
                    window.ShowDialog();
                    break;

                case DataTableText.油票记录:
                    oil = new OilWindow();
                    oil.Init(DataTable as Table.CarOilS);
                    oil.ShowDialog();
                    break;

                case DataTableText.预订车辆:
                    drive = new DrivePlan();
                    Table.CarDriveS c1=DataTable as Table.CarDriveS;
                    if (c1 == null)
                        c1 = new Table.CarDriveS() { Enable = Table.CarDriveS.EnumEnable.预订};
                    
                    drive.Init( c1);
                    drive.ShowDialog();
                    break;

                case DataTableText.行车记录:
                    drive = new DrivePlan();
                     Table.CarDriveS c2=DataTable as Table.CarDriveS;
                    if(c2==null)
                        c2 = new Table.CarDriveS() { Enable = Table.CarDriveS.EnumEnable.登记受理 };

                    drive.Init( c2);
                    drive.ShowDialog();
                    break;

                default:
                    health = new HealthWindow();
                    Table.CarHealthS chs = DataTable as Table.CarHealthS;
                    if(chs==null)
                        switch (TableText)
                        {
                            case DataTableText.违章处罚:
                                chs = new Table.CarHealthS(Table.CarHealthS.EnumFlag.违章处罚) ;
                                break;
                        case DataTableText.事故赔偿:
                                chs = new Table.CarHealthS(Table.CarHealthS.EnumFlag.事故赔偿) ;
                                break;
                        case DataTableText.维修保养:
                                chs = new Table.CarHealthS(Table.CarHealthS.EnumFlag.维修保养);
                                break;
                        case DataTableText.年审记录:
                                chs = new Table.CarHealthS(Table.CarHealthS.EnumFlag.年审记录);
                                break;
                        case DataTableText.投保记录:
                                chs = new Table.CarHealthS(Table.CarHealthS.EnumFlag.投保记录);
                                break;
                        }
                    health.Init(chs);
                    health.Title = TableText.ToString();
                    health.ShowDialog();
                    break;
            }

            Terminate();
            return true;
        }

        public override void Terminate()
        {
            if (window != null)
                window.Close();

            if (oil != null)
                oil.Close();

            if (drive != null)
                drive.Close();
        }
    }
}
