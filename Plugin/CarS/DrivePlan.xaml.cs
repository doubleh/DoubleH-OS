﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Diagnostics;
using DoubleH.Utility;

namespace CarS
{
    /// <summary>
    /// Drive.xaml 的交互逻辑
    /// </summary>
    public partial class DrivePlan : Window
    {
        public DrivePlan()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.CarDriveS orderS = null;

        public void Init(Table.CarDriveS drive)
        {
            Debug.Assert(drive != null);

            orderS = drive;
            InitOrderIsZuLin();

            doubleUpDownDeposit.FormatString = doubleUpDownMileageEnd.FormatString = doubleUpDownMileageStart.FormatString = doubleUpDownMoney.FormatString = "F" + Table.SysConfig.SysConfigParams.DecimalPlaces.ToString();
           
            comboBoxCar.DisplayMemberPath = "Name";
            comboBoxCar.SelectedValuePath = "Id";
            comboBoxOperator.DisplayMemberPath = "Name";
            comboBoxOperator.SelectedValuePath = "Id";
            if (orderS.Id == -1)
            {
                if (orderS.Enable == Table.CarDriveS.EnumEnable.预订)
                    comboBoxCar.ItemsSource = Table.CarS.GetList(Table.CarS.EnumEnable.待用, Table.CarS.EnumEnable.年审, Table.CarS.EnumEnable.维修, Table.CarS.EnumEnable.行车);
                else
                    comboBoxCar.ItemsSource = Table.CarS.GetList(Table.CarS.EnumEnable.待用);

                comboBoxOperator.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.操作员, Table.UserS.EnumFlag.经办人);
            }
            else
            {
                comboBoxCar.Items.Add(Table.CarS.GetObject(orderS.CarSId));
                comboBoxOperator.Items.Add(Table.UserS.GetObject(orderS.OperatorerSId));
            }
            comboBoxCar.Text = orderS._CarName;
            comboBoxOperator.Text = orderS._OperatorerSName;

            textBoxCustomerSCode.Text = orderS._CorSName;
            textBoxNote.Text = orderS.Note;
            dateTimePickerStart.Value = orderS.DateStart;
            dateTimePickerPlan.Value = orderS.DatePlan;
            dateTimePickerEnd.Value = orderS.DateEnd ?? dateTimePickerPlan.Value;
            doubleUpDownMileageStart.Value = orderS.MileageStart;
            doubleUpDownMileageEnd.Value = orderS._MileageEnd;
            doubleUpDownMoney.Value = orderS.Money;
            comboBoxZuLinType.SelectedIndex = 0;
            comboBoxZuLinType.SelectedItem = orderS.Flag;
            doubleUpDownMoney.Value = orderS.Money;
            if (!Table.SysConfig.SysConfigParams.UseCarRentS)
                doubleUpDownDeposit.Value = orderS.Money;
            else
                doubleUpDownDeposit.Value = orderS.Deposit;

            if (orderS.Enable == Table.CarDriveS.EnumEnable.处理完毕)
                buttonOK.Visibility = Visibility.Hidden;
        }

        private void InitOrderIsZuLin()
        {
                dateTimePickerEnd.IsReadOnly = true;
                doubleUpDownMileageEnd.IsReadOnly = true;
            if (orderS.Id != -1)
            {
                if (orderS.Enable == Table.CarDriveS.EnumEnable.处理完毕)
                {
                    comboBoxCar.IsReadOnly = true;
                    comboBoxOperator.IsReadOnly = true;
                    dateTimePickerStart.IsEnabled = false;
                    dateTimePickerPlan.IsEnabled = false;
                    doubleUpDownMileageStart.IsEnabled = false;


                    buttonOK.Visibility = Visibility.Hidden;
                    buttonCustomerS.IsEnabled = false;
                    textBoxNote.IsReadOnly = true;
                    dateTimePickerEnd.IsReadOnly = true;
                    doubleUpDownDeposit.IsReadOnly = true;
                    doubleUpDownMileageEnd.IsReadOnly = true;
                    doubleUpDownMoney.IsReadOnly = true;
                    comboBoxZuLinType.IsReadOnly = true;
                }
                else if (orderS.Enable == Table.CarDriveS.EnumEnable.登记受理)
                {
                    comboBoxCar.IsReadOnly = true;
                    comboBoxOperator.IsReadOnly = true;
                    dateTimePickerStart.IsEnabled = false;
                    dateTimePickerPlan.IsEnabled = false;
                    doubleUpDownMileageStart.IsEnabled = false;


                    dateTimePickerEnd.IsReadOnly = false;
                    doubleUpDownMileageEnd.IsReadOnly = false;
                    buttonOK.Content = "还车";
                }
            }
            if (Table.SysConfig.SysConfigParams.UseCarRentS)
            {
                 labelDeposit.Content = "定      金:";
                 comboBoxZuLinType.Items.Add(Table.CarDriveS.EnumFlag.日租);
                 comboBoxZuLinType.Items.Add(Table.CarDriveS.EnumFlag.时租);
                 comboBoxZuLinType.Items.Add(Table.CarDriveS.EnumFlag.月租);

                 if (orderS.Id==-1)
                 {
                     this.Title = "预定车辆";
                     buttonOK.Content = "预定";
                     doubleUpDownMileageStart.IsReadOnly = true;
                 }
            }
            else
            {
                comboBoxZuLinType.Items.Add(Table.CarDriveS.EnumFlag.非车辆租赁);
                buttonCustomerS.IsEnabled = false;
                textBoxCustomerSCode.IsReadOnly=true;
                comboBoxZuLinType.Visibility = Visibility.Hidden;
                labelZuLinType.Visibility = Visibility.Hidden;
                doubleUpDownMoney.Visibility = Visibility.Hidden;
                labelMoney.Visibility = Visibility.Hidden;
            }
        }

        private void InitEvent()
        {
            buttonCustomerS.Click += (ss, ee) =>
                {
                    CorS.GetCorS gcs = new CorS.GetCorS(true,false);
                    Point p = PointToScreen(Mouse.GetPosition(ee.Source as FrameworkElement));
                    gcs.Left = p.X;
                    gcs.Top = p.Y;
                    gcs.ShowDialog();
                    Table.CorS customer = gcs.Selected;
                    if (customer != null)
                    {
                        orderS.CorSId = customer.Id;
                        textBoxCustomerSCode.Text = customer.Name;
                    }
                };

            buttonOK.Click += (ss, ee) => {                    Save();  };

            comboBoxZuLinType.SelectionChanged += (ss, ee) => { ChangRentValue(); };
            comboBoxCar.SelectionChanged += (ss, ee) => { ChangRentValue(); };
        }

        private void ChangRentValue()
        {
            if (comboBoxCar.SelectedItem == null)
                return;
            if (comboBoxZuLinType.SelectedItem == null)
                return;

            Int64 id = -1;
            Int64.TryParse(comboBoxCar.SelectedValue.ToString(), out id);
            Table.CarS car = Table.CarS.GetObject(id);
            if (car == null)
                return;

            switch ((Table.CarDriveS.EnumFlag)comboBoxZuLinType.SelectedItem)
            {
                case Table.CarDriveS.EnumFlag.日租:
                    doubleUpDownMoney.Value = car.RentDay;
                    break;
                case Table.CarDriveS.EnumFlag.时租:
                    doubleUpDownMoney.Value = car.RentHour;
                    break;
                case Table.CarDriveS.EnumFlag.月租:
                    doubleUpDownMoney.Value = car.RentMonth;
                    break;
            }
        }

        private bool Verify()
        {
            bool b = true;
            if (comboBoxOperator.SelectedItem == null || comboBoxCar.SelectedItem == null )
            {
                MessageWindow.Show("经办人不能为空\r\n车牌不能为空");
                b = false;
            }
            if (orderS.Flag != Table.CarDriveS.EnumFlag.非车辆租赁)
            {
                if (string.IsNullOrEmpty(textBoxCustomerSCode.Text))
                {
                    MessageWindow.Show("客户名称不能为空");
                    b = false;
                }
            }

            return b;
        }

        private void Save()
        {
            if (!Verify())
                return;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (!Table.SysConfig.SysConfigParams.UseCarRentS)
                orderS.Money = doubleUpDownDeposit.Value.Value;
            else
            {
                orderS.Deposit = doubleUpDownDeposit.Value.Value;
                orderS.Money = doubleUpDownMoney.Value.Value;
            }
            orderS.CarSId = (Int64)comboBoxCar.SelectedValue;
            orderS.OperatorerSId = (Int64)comboBoxOperator.SelectedValue;
            orderS.Note = textBoxNote.Text;
            orderS.DateStart = dateTimePickerStart.Value.Value;
            orderS.DatePlan = dateTimePickerPlan.Value.Value;
            orderS.MileageStart = doubleUpDownMileageStart.Value.Value;
            orderS.Flag = (Table.CarDriveS.EnumFlag)comboBoxZuLinType.SelectedItem;
            if (orderS.Id == -1)
                result = orderS.Insert();
            else if (orderS.Enable == Table.CarDriveS.EnumEnable.预订)
                result = orderS.Update();
            else
            {
                if (doubleUpDownMileageEnd.Value.Value < doubleUpDownMileageStart.Value.Value)
                {
                    MessageWindow.Show("还车里程数_必须大于_出车里程数");
                    return;
                }
                result = RuZhang();
            }

            if (result != Table.DataTableS.EnumDatabaseStatus.操作成功)
                MessageWindow.Show(result.ToString());
            else
                this.Close();
        }

        private Table.DataTableS.EnumDatabaseStatus RuZhang()
        {
            orderS.DateEnd = dateTimePickerEnd.Value.Value;
            orderS._MileageEnd = doubleUpDownMileageEnd.Value.Value;

           return orderS.Update((int)Table.CarDriveS.EnumEnable.处理完毕);
        }
    }
}
