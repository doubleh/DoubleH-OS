﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;

namespace HouseS
{
    class HouseSExt : Plugin
    {
        private IPluginHost m_host = null;
        MainWindow window = null;
        RentWindow rent = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
            switch (TableText)
            {
                default:
                    window = new MainWindow();
                    window.Owner = host.WorkAreaWindow;
                    window.Init();
                    window.Show();
                    break;

                case DataTableText.公寓://注意这里和上面是互换的哦,由于 DoubleH中Menu配置的问题.
                case DataTableText.楼层:
                    rent = new RentWindow();
                    rent.Owner = host.WorkAreaWindow;
                    rent.Init(DataTable as Table.RoomS);
                    rent.Show();
                    break;
            }

            return true;
        }

        public override void Terminate()
        {
            if (window != null)
            {
                window.Close();
                window = null;
            }
            if (rent != null)
            {
                rent.Close();
                rent = null;
            }
        }
    }
}