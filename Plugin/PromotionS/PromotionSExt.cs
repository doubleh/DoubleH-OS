﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using DoubleH.Plugins;
using System.Windows;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace PromotionS
{
    public class PromotionSExt : Plugin
    {
        private IPluginHost m_host = null;
        WindowCoupons coupons = null;
        WindowPromotion promotion = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
            switch (TableText)
            {
                case DataTableText.优惠券:
                    coupons = new WindowCoupons();
                    coupons.Init(DataTable as Table.CouponsS);
                    coupons.Owner = host.WorkAreaWindow;
                    coupons.Show();
                    break;

                case DataTableText.促销方案:
                    promotion = new WindowPromotion();
                    promotion.Init(DataTable as Table.PromotionS);
                    promotion.Owner = host.WorkAreaWindow;
                    promotion.Show ();
                    break;
            }

            return true;
        }
     
        public override void Terminate()
        {
            if (coupons != null)
                coupons.Close();
            if (promotion != null)
                promotion.Close();
        }
    }
}
