﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace SysConfig
{
    /// <summary>
    /// WindowUser.xaml 的交互逻辑
    /// </summary>
    public partial class WindowUser : Window
    {
        public WindowUser()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.UserS order = null;
        public void Init(Table.UserS obj)
        {
            order = obj ?? new Table.UserS();

            uCGroupS1.Init(Table.GroupS.EnumFlag.操作员分类);
            uCGroupS1.SelectedObjectId = order.GroupSId;

            comboBoxOperatorer.Items.Add(Table.UserS.EnumFlag.操作员);
            comboBoxOperatorer.Items.Add(Table.UserS.EnumFlag.经办人);
            comboBoxOperatorer.Items.Add(Table.UserS.EnumFlag.POS操作员);
            comboBoxOperatorer.SelectedItem = order.Flag;

            textBoxUserNo.Text = order.UserNO;
            textBoxPwd.Password = order.Password;
            textBoxName.Text = order.Name;
            textBoxPhone.Text = order.Phone;
            textBoxAddress.Text = order.Address;
            textBoxNote.Text = order.Note;
            checkBoxEnable.IsChecked = (order.Enable == Table.UserS.EnumEnable.停用);
            ChangedUserState();

            if (order.UserNO=="root")
            {
                comboBoxOperatorer.IsEnabled = false;
                uCGroupS1.IsEnabled = false;
                textBoxUserNo.IsEnabled = false;
                checkBoxEnable.IsEnabled = false;
            }
        }

        private void InitEvent()
        {
            comboBoxOperatorer.SelectionChanged += (ss, ee) => ChangedUserState(); 
            //buttonImport.Click += (ss, ee) => { IandO io = new IandO(); io.Import(new Table.UserS()); };

            buttonOk.Click += (ss, ee) => Save();
        }

        private void ChangedUserState()
        {
            textBoxUserNo.IsEnabled = ((Table.UserS.EnumFlag)comboBoxOperatorer.SelectedItem != Table.UserS.EnumFlag.经办人);
            textBoxPwd.IsEnabled = ((Table.UserS.EnumFlag)comboBoxOperatorer.SelectedItem != Table.UserS.EnumFlag.经办人);
        }

        private void Save()
        {
            order.GroupSId = uCGroupS1.SelectedObjectId;
            order.UserNO = textBoxUserNo.Text;
            order.Password = textBoxPwd.Password;
            order.Name = textBoxName.Text;
            order.Address = textBoxAddress.Text;
            order.Note = textBoxNote.Text;
            order.Phone = textBoxPhone.Text;
            order.Flag = (Table.UserS.EnumFlag)comboBoxOperatorer.SelectedItem;
            order.Enable = (checkBoxEnable.IsChecked == false ? Table.UserS.EnumEnable.启用 : Table.UserS.EnumEnable.停用);
            Table.DataTableS.EnumDatabaseStatus result;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}