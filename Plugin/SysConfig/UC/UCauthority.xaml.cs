﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SysConfig.UC
{
    /// <summary>
    /// UCauthority.xaml 的交互逻辑
    /// </summary>
    public partial class UCauthority : UserControl
    {
        public UCauthority()
        {
            InitializeComponent();
        }

        ObservableCollection<Authority> la = new ObservableCollection<Authority>();
        public ObservableCollection<Authority> Items
        {
            get { return la; }
        }


        public void Init(Table.UserS user, bool onlyLook, params DataTableText[] tableText)
        {
            List<string> l = new List<string>();
            foreach (DataTableText dt in tableText)
                l.Add(dt.ToString());

            Init(user, onlyLook, l.ToArray());
        }
        public void Init(Table.UserS user, bool onlyLook, params string[] tableText)
        {
            Debug.Assert(user != null);
            foreach (string table in tableText)
            {
                Table.UserS.EnumAuthority au = user.GetAuthority(table);
                Authority auty = new Authority() { DataTableText = table };
                auty.ChaKan = au > Table.UserS.EnumAuthority.无权限;
                auty.BianJi = au > Table.UserS.EnumAuthority.查看;
                auty.ShenHe = au > Table.UserS.EnumAuthority.编辑;
                auty.ShanChu = au > Table.UserS.EnumAuthority.审核;
                la.Add(auty);
            }
            listView1.ItemsSource = la;

            if (!onlyLook)
                return;

            bianji.Width = 0;
            shenhe.Width = 0;
            shanchu.Width = 0;
        }


        public class Authority : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;
            internal void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

            public string DataTableText { get; set; }

            bool chakan = false;
            public bool ChaKan
            {
                get { return chakan; }
                set
                {
                    chakan = value;
                    if (value)
                    {
                        bianji = false;
                        shenhe = false;
                        shanchu = false;
                        NotifyPropertyChanged("BianJi");
                        NotifyPropertyChanged("ShenHe");
                        NotifyPropertyChanged("ShanChu");
                    }
                    NotifyPropertyChanged("ChaKan");
                }
            }

            bool bianji = false;
            public bool BianJi
            {
                get { return bianji; }
                set
                {
                   bianji  = value;
                    if (value)
                    {
                        chakan = false;
                        shenhe = false;
                        shanchu = false;
                        NotifyPropertyChanged("ChaKan");
                        NotifyPropertyChanged("ShenHe");
                        NotifyPropertyChanged("ShanChu");
                    }
                    NotifyPropertyChanged("BianJi");
                }
            }

            bool shenhe = false;
            public bool ShenHe
            {
                get { return shenhe; }
                set
                {
                   shenhe= value;
                    if (value)
                    {
                        chakan = false;
                        bianji = false;
                        shanchu = false;
                        NotifyPropertyChanged("ChaKan");
                        NotifyPropertyChanged("BianJi");
                        NotifyPropertyChanged("ShanChu");
                    }
                    NotifyPropertyChanged("ShenHe");
                }
            }

            bool shanchu = false;
            public bool ShanChu
            {
                get { return shanchu; }
                set
                {
                    shanchu = value;
                    if (value)
                    {
                        chakan = false;
                        bianji = false;
                        shenhe = false;
                        NotifyPropertyChanged("ChaKan");
                        NotifyPropertyChanged("BianJi");
                        NotifyPropertyChanged("ShenHe");
                    }
                    NotifyPropertyChanged("ShanChu");
                }
            }

            public Table.UserS.EnumAuthority EA
            {
                get
                {
                    if (ShanChu)
                        return Table.UserS.EnumAuthority.作废;
                    else if (ShenHe)
                        return Table.UserS.EnumAuthority.审核;
                    else if (BianJi)
                        return Table.UserS.EnumAuthority.编辑;
                    else if (ChaKan)
                        return Table.UserS.EnumAuthority.查看;
                    else return Table.UserS.EnumAuthority.无权限;
                }
            }
        }
    }
}
