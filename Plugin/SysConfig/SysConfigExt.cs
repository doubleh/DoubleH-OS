﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using System.Windows;

namespace SysConfig
{
    class SysConfigExt : Plugin
    {
        MainWindow window = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            window = new MainWindow();
            window.Init(host.ExtentionManager.Plugins);
            window.ShowDialog();
            
            Terminate();
            return true;
        }
        public override void Terminate()
        {
            if (window != null)
                window.Close();
            }
    }
}
