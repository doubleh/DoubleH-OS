﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using System.Windows;
using Table = FCNS.Data.Table;

namespace SalesOnWeb
{
  public  class SalesOnWebExt:Plugin
    {
        private IPluginHost m_host = null;
        MainWindow window = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
          
            Terminate();
            return true;
        }

        public override void Terminate()
        {
            if (window != null)
                window.Close();
        }
    }
}
