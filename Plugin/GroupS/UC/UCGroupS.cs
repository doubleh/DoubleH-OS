﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Collections;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace GroupS
{
    public class UCGroupS : ComboBox
    {
        public UCGroupS()
        {
            InitVar();
        }

        private void InitVar()
        {
            IsEditable = true;
            this.DisplayMemberPath = "[Name]";
            this.SelectedValuePath = "[Id]";
        }

        Table.GroupS.EnumFlag flag;
        public void Init(Table.GroupS.EnumFlag obj)
        {
            flag = obj;
            this.ItemsSource = FCNS.Data.SQLdata.GetDataRows("select Id,Name from GroupS where Flag=" + (Int64)obj + " and Enable=" + (Int64)Table.UniqueS.EnumEnable.启用);
        }

        bool autoInsert = true;
        /// <summary>
        /// 在控件可编辑状态下,如果数据库中不存在对应的文本,是否自动增加?
        /// </summary>
        public bool AutoInsert
        {
            get { return autoInsert; }
            set { autoInsert = value; }
        }

        public Int64 SelectedObjectId
        {
            get
            {
                if (SelectedItem == null)
                {
                    if (!string.IsNullOrEmpty(Text)&&autoInsert)
                    {
                        Table.GroupS us = new Table.GroupS(flag);
                        us.Name = Text;
                        if (us.Insert() == Table.DataTableS.EnumDatabaseStatus.操作成功)
                            return us.Id;
                        else
                            return -1;
                    }
                    return -1;
                }
                else
                    return (Int64)SelectedValue;
            }
            set
            {
                SelectedValue = value;
            }
        }
    }
}
