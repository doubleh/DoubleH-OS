﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;
using System.Data;
using DoubleH.Utility.IO;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace GroupS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.GroupS order = null;

        public Table.GroupS NewGroupS
        {
            get { return order; }
        }
        List<Table.GroupS> gs;

        public void Init(Table.GroupS.EnumFlag flag, Table.GroupS group)
        {
            order = group ?? new Table.GroupS(flag);
            gs = new List<Table.GroupS>(Table.GroupS.GetList(flag, Table.GroupS.EnumEnable.启用));
            gs.Remove(gs.Find(f => { return f.Id == order.Id; }));
            gs.Insert(0, new Table.GroupS(flag) { Name = "顶层分类" });//添加父类

            if (flag == Table.GroupS.EnumFlag.操作员分类)
            {
                comboBoxGroup.IsEnabled = false;
                checkBox1.IsEnabled = false;
            }
            else
            {
                comboBoxGroup.ItemsSource = gs;
                comboBoxGroup.DisplayMemberPath = "Name";
                comboBoxGroup.SelectedValuePath = "Id";
                comboBoxGroup.SelectedValue = order.GroupSId;
                checkBox1.IsChecked = order.Enable == Table.GroupS.EnumEnable.停用 ? true : false;
            }

            textBoxName.Text = order.Name;
            buttonHeBin.IsEnabled = order.Id != -1;
            textBoxName.Focus();
            uCImage1.ImageFile = order._ImageFile;
        }

        private void InitEvent()
        {
            buttonOk.Click += (ss, ee) =>  Save(); 
            buttonHeBin.Click += (ss, ee) =>  HeBing();
        }

        private void HeBing()
        {
            if (comboBoxGroup.SelectedItem == null)
                return;

            if (MessageWindow.Show("", "确定要合并类别吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            Table.DataTableS.EnumDatabaseStatus result = order.HeBing(((Table.GroupS)comboBoxGroup.SelectedItem).Id);
            MessageWindow.Show(result.ToString());
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                buttonHeBin.IsEnabled = false;
        }

        private void Save()
        {
            order.Name = textBoxName.Text.Trim();
            order.Enable = checkBox1.IsChecked == true ? Table.GroupS.EnumEnable.停用 : Table.GroupS.EnumEnable.启用;
            order.GroupSId = comboBoxGroup.SelectedItem == null ? -1 : (Int64)comboBoxGroup.SelectedValue;
            order.UploadFile = uCImage1.UploadFileName;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
            {
                uCImage1.UpdateImage(Table.GroupS.tableName);
                this.Close();
            }
            else
                MessageWindow.Show(result.ToString());
        }
    }
}
