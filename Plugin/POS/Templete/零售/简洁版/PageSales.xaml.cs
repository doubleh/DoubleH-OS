﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Windows.Interop;
using System.Collections;
using DoubleH.Utility.Configuration;
using System.Collections.ObjectModel;

namespace POS.Templete.PithyTemplete
{
    /// <summary>
    /// StoreSales.xaml 的交互逻辑
    /// </summary>
    public partial class PageSale : Page, ISaleTemplete
    {
        public PageSale()
        {
            InitializeComponent();

            InitUI();
            InitVar();
            InitEvent();
        }

        public event dSaleToPayEvent Saled;
        public event dEditProduct EditProduct;
        //这3个事件在热键中触发
        public event dNoParams JiaoBan;
        public event dGuaDan GuaDan;
        public event dZengSong ZengSong;
        public event dNoParams TuiHuo;

        private void InitUI()
        {
            this.Background = App.BackgroundBrush;
            this.Foreground = App.ForegroundBrush;
            foreach (UIElement ui in mainGrid.Children)
            {
                Control c = ui as Control;
                if (c != null)
                {
                    c.Background = App.BackgroundBrush;
                    c.Foreground = App.ForegroundBrush;
                }
            }
            foreach (UIElement ui in gridRight.Children)
            {
                Control c = ui as Control;
                if (c != null)
                {
                    c.Background = App.BackgroundBrush;
                    c.Foreground = App.ForegroundBrush;
                }
            }
        }

        private void InitVar()
        {
            if (!DoubleHConfig.AppConfig.ShowCashboxMoney)
            {
                labelCashboxMoney.Visibility = Visibility.Hidden;
                labelCashboxMoneyTitle.Visibility = Visibility.Hidden;
            }

            productSListInfo1.ShowBodyContexMenu = false;
            productSListInfo1.ShowAverage = false;
            productSListInfo1.EnableText = string.Empty;
            productSListInfo1.CanUserAddRows = false;
            productSListInfo1.CanSelectProduct = false;
            productSListInfo1.SelectionUnit = DataGridSelectionUnit.FullRow;
            productSListInfo1.ProductSType = ProductS.GetProductS.EnumProductS.可销售商品;

            labelCashboxMoney.Content = DoubleHConfig.AppConfig.PosCashboxMoney;

            textBoxInput.Focus();
        }

        private void InitEvent()
        {
            textBoxInput.KeyDown += (ss, ee) => PressEnter(ee.Key); 
            textBoxInput.PreviewKeyDown += (ss, ee) => TextBoxKeyPreviewDown(ee.Key);
            productSListInfo1.KeyUp += (ss, ee) => RemoveSelectedItem(ee);
        }

        private void RemoveSelectedItem(KeyEventArgs e)
        {
            if(e.Key==Key.Delete)
                EditProduct(EnumEditProduct.移除, productSListInfo1.SelectedItem, 0, 0);
        }

        private void TextBoxKeyPreviewDown(Key key)
        {
            switch (key)
            {
                case Key.Delete:
                    EditProduct(EnumEditProduct.移除, productSListInfo1.SelectedItem, 0, 0);
                    break;

                case Key.Down:
                    if (productSListInfo1.SelectedIndex + 1 < productSListInfo1.Count)
                        productSListInfo1.SelectedIndex += 1;
                    break;

                case Key.Up:
                    if (productSListInfo1.SelectedIndex > 0)
                        productSListInfo1.SelectedIndex -= 1;
                    break;
            }
        }

        private void PressEnter(Key key)
        {
            if (key != Key.Enter)
                return;

            if (string.IsNullOrEmpty(textBoxInput.Text))
            {
                if (!productSListInfo1.HasItems)
                    return;

                if (Saled != null)
                    Saled();

                return;
            }

            if (textBoxInput.Text.StartsWith("++"))
            {
                ChangedQuantity();
                textBoxInput.Clear();
                return;
            }
            if (textBoxInput.Text.StartsWith("--"))
            {
                ChangedPrice();
                textBoxInput.Clear();
                return;
            }
            if (textBoxInput.Text.StartsWith(".."))
            {
                GetCorS(textBoxInput.Text.Remove(0, 2), false);
                return;
            }
            if (textBoxInput.Text.StartsWith("//"))
            {
                if (GuaDan == null)
                    return;

                GuaDan(true, false, textBoxInput.Text.Remove(0, 2));
                return;
            }

            Table.ProductS product = App.GetProductSBySearch(textBoxInput.Text.Trim());
            if (product == null)
                MessageWindow.Show("商品不存在");
            else
            {
                EditProduct(EnumEditProduct.添加, product, 0, 0);
                productSListInfo1.SelectedIndex = 0;
            }
            if (productSListInfo1.Count > 0)
                productSListInfo1.SelectedIndex = 0;

            textBoxInput.Clear();
            textBoxInput.Focus();
        }

        private void ChangedQuantity()
        {
            if (productSListInfo1.SelectedItem == null)
                return;
            
            double d = 0; string str = string.Empty;
            if (!double.TryParse(textBoxInput.Text.Remove(0, 2), out d))
                return;

            if (d == 0)
                return;

            EditProduct(EnumEditProduct.编辑数量, productSListInfo1.SelectedItem, Math.Abs(d), 0);
        }

        private void ChangedPrice()
        {
            if (productSListInfo1.SelectedItem == null)
                return;

            double d = 0; string str = string.Empty;
            if (!double.TryParse(textBoxInput.Text.Remove(0, 2), out d))
                return;

            EditProduct(EnumEditProduct.编辑价格, productSListInfo1.SelectedItem, 0, Math.Abs(d));
        }

        private void GetCorS(string searchText, bool searchInGuaDan)
        {
            Table.CorS corS = App.GetCorSBySearch(searchText,  searchInGuaDan,true);
            textBoxInput.Clear();
            if (corS == null)
            {
                labelVipNO.Content = string.Empty;
                labelRemainIntegral.Content = string.Empty;
            }
            else
            {
                labelVipNO.Content = corS.VipNO;
                labelRemainIntegral.Content = corS.VipRemainIntegral;
                App.Order.CorSId = corS.Id;
            }
        }



        //接口
        public void Init()
        {
            productSListInfo1.Init(DoubleHConfig.UserConfig.GetUserUIparams(DataTableText.Pos零售商品编辑));
        }

        public void InitOrder()
        {
            if (App.Order == null)
                return;

            productSListInfo1.ItemsSource = App.Order.ProductSList;
            GetCorS(App.Order.CorSId.ToString(), true);
        }

        public Table.StoreS StoreS  { set { productSListInfo1.StoreS = value; } }

        public void OrderFinish(Table.PosOrderS order)
        {
            labelPreOrderNO.Content = order.OrderNO;
            labelPreMoney.Content = order.Money;
            labelCashboxMoney.Content = DoubleHConfig.AppConfig.PosCashboxMoney;
        }

        public void GuaDanFinish(System.Collections.IList guaDanList)
        {
            labelGuaDan.Content = guaDanList.Count;
        }

        public void ProductCalcFinish()
        {
            productSListInfo1.CalcQuantityAndMoneyOnBottom();
        }
    }
}