﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.IO;
using DoubleH.Utility.Configuration;

namespace POS.Templete.TouchTempleteS
{
    /// <summary>
    /// PagePay.xaml 的交互逻辑
    /// </summary>
    public partial class PagePay : Page
    {
        public PagePay()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        public event dPayToSaleEvent Payed;
        public event EventHandler Cancel;

        private void InitVar()
        {
            this.Background = App.BackgroundBrush;
            this.Foreground = App.ForegroundBrush;
            foreach (UIElement ui in mainGrid.Children)
            {
                Control c = ui as Control;
                if (c != null)
                { 
                    c.Background = App.BackgroundBrush;
                    c.Foreground = App.ForegroundBrush;
                }
            }
        }

        public void InitEvent()
        {
            doubleUpDownPay.ValueChanged += (ss, ee) => ChangeMoney(); 
            buttonPay.Click += (ss, ee) => Pay();
            this.Loaded += (ss, ee) => ThisOnLoad();
            buttonCanel.Click += (ss, ee) => CancelPay();
        }

        private void CancelPay()
        {
            if (Cancel != null)
                Cancel(null,null);
        }

        private void ThisOnLoad()
        {
            if (App.Mode == POS.EnumMode.零售模式)
            {
                labelSum.Content = App.Order.Money;
                doubleUpDownPay.Value = App.Order.Money;
            }
            else
            {
                label1.Content = "应退款";
                label2.Content = "已退款";
                labelSum.Content = App.Order.Money;
                doubleUpDownPay.Value = App.Order.Money;
            }

            doubleUpDownPay.Focus();
            App.OpenLed();
        }

        private void ChangeMoney()
        {
            double d = doubleUpDownPay.Value.HasValue ? doubleUpDownPay.Value.Value : 0;
            d = Math.Round(d - App.Order.Money, Table.SysConfig.SysConfigParams.DecimalPlaces);
            labelBack.Content = (d > 0 ? d : 0);
        }
       
        public void Pay()
        {
            if (doubleUpDownPay.Value.HasValue)
            {
                if (doubleUpDownPay.Value.Value < App.Order.Money)
                {
                    MessageWindow.Show("注意", "收款金额低于账单金额.");
                    return;
                }

                if (Payed == null)
                    return;

                App.ShowLed(doubleUpDownPay.Value.ToString(),labelBack.Content.ToString());
                Payed(doubleUpDownPay.Value.Value, true);
            }
            else
                MessageWindow.Show("请输入收款金额");
        }
    }
}