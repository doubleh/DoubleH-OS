﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoubleH.Utility;
using DoubleH.Utility.Configuration;
using Table = FCNS.Data.Table;

namespace POS.Templete.StoreTemplete
{
    /// <summary>
    /// PageLogin.xaml 的交互逻辑
    /// </summary>
    public partial class PageLogin : Page, ILoginTemplete
    {
        public event dLoginToSaleEvent Logined;

        public PageLogin()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
            InitUI();
        }

        private void InitVar()
        {
            comboBoxName.DisplayMemberPath = "_LoginName";
            comboBoxName.SelectedValuePath = "Password";
            comboBoxName.ItemsSource = Table.UserS.GetLoginUserForPos(App.Pos.PosNO);
            comboBoxName.Text = DoubleHConfig.AppConfig.LastPosName;

            textBoxPwd.Focus();
        }

        private void InitUI()
        {
            this.Background = App.BackgroundBrush;
            this.Foreground = App.ForegroundBrush;
            foreach (UIElement ui in mainGrid.Children)
            {
                Control c = ui as ContentControl;
                if (c != null)
                {
                    c.Background = App.BackgroundBrush;
                    c.Foreground = App.ForegroundBrush;
                    //c.FontSize = DoubleHConfig.AppConfig.PosFontSize;
                    //c.Height = DoubleHConfig.AppConfig.PosFontSize * 2;
                }
            }
            //comboBoxName.Margin = new Thickness(labelAccount.Margin.Right, comboBoxName.Margin.Top, comboBoxName.Margin.Right, comboBoxName.Margin.Bottom);
        }

        private void InitEvent()
        {
            buttonLogin.Click += (ss, ee) => Login(EnumMode.零售模式);
        }

        private void Login(EnumMode mode)
        {
            if (string.IsNullOrEmpty(comboBoxName.Text))
            {
                MessageWindow.Show("请选择用户");
                return;
            }

            if ((string)comboBoxName.SelectedValue != textBoxPwd.Password)
            {
                MessageWindow.Show("用户名或密码错误");
                textBoxPwd.Clear();
                textBoxPwd.Focus();
                Table.ErrorS.WriteLogFile(comboBoxName.Text + " 登陆失败");
                return;
            }
            //更改密码
            if (checkBoxPwd.IsChecked.Value)
            {
                checkBoxPwd.IsChecked = false;
                ChangePwdWindow cpw = new ChangePwdWindow();
                cpw.Init((Table.UserS)comboBoxName.SelectedItem);
                cpw.ShowDialog();
            }

            DoubleHConfig.AppConfig.LastPosName = comboBoxName.Text;
            textBoxPwd.Clear();//清空,当用户注销的时候就看不到密码了


            if (Logined != null)
                Logined((Table.UserS)comboBoxName.SelectedItem, mode);
        }
    }
}