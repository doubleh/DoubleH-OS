﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace POS
{
    /// <summary>
    /// WindowProduct.xaml 的交互逻辑
    /// </summary>
    public partial class SelectObject : Window
    {
        public enum EnumType
        {
            商品,
            会员
        }

        public SelectObject()
        {
            InitializeComponent();

            InitEvent();
        }

        EnumType eType = EnumType.商品;

        Table.ProductS ps = null;
        /// <summary>
        /// 已选择的商品(针对商品选择场景)
        /// </summary>
        public Table.ProductS Product
        {
            get { return ps; }
        }

        Table.CorS cs = null;
        /// <summary>
        /// 已选择的会员(针对会员选择场景)
        /// </summary>
        public Table.CorS CorS
        {
            get { return cs; }
        }

        public void Init(EnumType et, System.Collections.IEnumerable itemsSource)
        {
            eType = et;
            dataGridExt1.ItemsSource = itemsSource;
            if (!dataGridExt1.HasItems)
                return;

            InitVar();
        }

        private void InitVar()
        {
            dataGridExt1.SelectedIndex = 0;
            dataGridExt1.ShowBodyMenu = false;
            dataGridExt1.ShowHeaderMenu = false;

            UserUIparams ui = new UserUIparams();
            switch (eType)
            {
                case EnumType.会员:
                    this.Title = "选择会员";
                    ui.Items = new List<DataGridColumnHeaderBinding>() { 
                    new DataGridColumnHeaderBinding(){ Header="姓名", BindingName="Name",  Index=1},
                    new DataGridColumnHeaderBinding(){ Header="姓名", BindingName="Name", Index=2},
                    new DataGridColumnHeaderBinding(){ Header="电话", BindingName="Tel", Index=3},
                    new DataGridColumnHeaderBinding(){ Header="会员卡号", BindingName="VipNO",Index=4}
                    };
                    break;

                case EnumType.商品:
                    this.Title = "选择商品";
                    ui.Items = new List<DataGridColumnHeaderBinding>() { 
                   new DataGridColumnHeaderBinding(){BindingName="Name",Header="名称",Index=1},
            new DataGridColumnHeaderBinding(){BindingName="Code",Header="编码",Index=2},
         new DataGridColumnHeaderBinding(){BindingName="Barcode",Header="条码",Index=3},
         new DataGridColumnHeaderBinding(){BindingName="_UnitName",Header="单位",Index=4},
         new DataGridColumnHeaderBinding(){BindingName="Standard",Header="规格",Index=5} 
                    };
                    break;
            }
            dataGridExt1.Init(ui);
        }

        private void InitEvent()
        {
            this.KeyDown += (ss, ee) =>  KeyDowned(ee.Key); 
            dataGridExt1.MouseDoubleClick += (ss, ee) =>   GetProduct(); 
            dataGridExt1.KeyDown += (ss, ee) =>  KeyDowned(ee.Key);
        }

        private void KeyDowned(Key key)
        {
            switch (key)
            {
                case Key.Enter: GetProduct(); break;

                case Key.Down:
                    if (dataGridExt1.SelectedIndex + 1 < dataGridExt1.Items.Count)
                        dataGridExt1.SelectedIndex += 1;
                    break;

                case Key.Up:
                    if (dataGridExt1.SelectedIndex > 0)
                        dataGridExt1.SelectedIndex -= 1;
                    break;

                case Key.Escape: this.Close(); break;
            }
        }

        private void GetProduct()
        {
            if (dataGridExt1.SelectedItem == null)
                return;

            switch (eType)
            {
                case EnumType.商品: ps = dataGridExt1.SelectedItem as Table.ProductS; break;
                case EnumType.会员: cs = dataGridExt1.SelectedItem as Table.CorS; break;
            }

            this.Close();
        }
    }
}