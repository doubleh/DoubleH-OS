﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using System.Diagnostics;
using DoubleH.Utility.Configuration;
using Table = FCNS.Data.Table;

namespace POS
{
    /// <summary>
    /// JiaoBanWindow.xaml 的交互逻辑
    /// </summary>
    public partial class JiaoBanWindow : Window
    {
        public JiaoBanWindow()
        {
            InitializeComponent();

            InitEvent();
        }
        
        /// <summary>
        /// 是否已经交班成功
        /// </summary>
        public bool Ensure{ get; set; }

        bool startWork = true;//开始上班？
        bool canClose =true;//防止强制交班状态下，用快捷键关闭窗体。

        public void Init(bool start,bool force)
        {
            Ensure = false;
            this.startWork = start;
            if (force)
            {
                this.Title = "强制交班";
                canClose = false;
            }

            if (start)
            {
                this.Title = "开始收款";
                labelMoneySumTitle.Visibility = Visibility.Hidden;
                labelMoneySum.Visibility = Visibility.Hidden;
                doubleUpDownMoney.Maximum = double.PositiveInfinity;
                doubleUpDownMoney.Value = DoubleHConfig.AppConfig.PosCashboxMoney;
            }
            else
            {
                this.Title = "交班";
                labelMoneyTitle.Content = "取款金额";
                if(!DoubleHConfig.AppConfig.ShowCashboxMoney)
                    labelMoneySum.Visibility = Visibility.Hidden;

                labelMoneySum.Content = DoubleHConfig.AppConfig.PosCashboxMoney;
                doubleUpDownMoney.Maximum = DoubleHConfig.AppConfig.PosCashboxMoney;
                doubleUpDownMoney.Value = DoubleHConfig.AppConfig.PosCashboxMoney;
            }
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
            this.Closing += (ss, ee) => ee.Cancel = !canClose;
        }

        private void Save()
        {
            Ensure = true;
            if (!doubleUpDownMoney.Value.HasValue)
                doubleUpDownMoney.Value = 0;

            if (startWork)
                DoubleHConfig.AppConfig.PosCashboxMoney = doubleUpDownMoney.Value.Value;
            else
            {
                DoubleHConfig.AppConfig.PosCashboxMoney =DoubleHConfig.AppConfig.PosCashboxMoney- doubleUpDownMoney.Value.Value;
                App.CurrentShift.EndMoney = doubleUpDownMoney.Value.Value;
                App.CurrentShift.EndDateTime = DateTime.Now;
                App.CurrentShift.Update();
            }
            canClose = true;
            this.Close();
        }
    }
}
