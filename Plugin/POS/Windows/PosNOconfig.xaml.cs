﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace POS
{
    /// <summary>
    /// PosNOconfig.xaml 的交互逻辑
    /// </summary>
    public partial class PosNOconfig : Window
    {
        public PosNOconfig()
        {
            InitializeComponent();

            InitEvent();
        }

        public void Init()
        {
            comboBoxPosType.ItemsSource = Enum.GetNames(typeof(Table.PosS.EnumFlag));
            uCStoreS1.ObjectType = StoreS.UC.UCStoreS.EnumType.仓库和连锁门店;
        }

        Table.PosS order = null;
        public Table.PosS Order
        {
            get { return order; }
        }

        private void InitEvent()
        {
            buttonCanel.Click += (ss, ee) => this.Close();
            buttonSave.Click += (ss, ee) => Save();
        }

        private void SetValue()
        {
            order.PosNO = posNO.Value.Value.ToString("d3");
            order.Note = textBoxNote.Text;
            order.StoreSId = uCStoreS1.SelectedObjectId;
        }

        private void Save()
        {
            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.机器号验证失败;
            if (comboBoxPosType.SelectedItem == null)
            {
                MessageWindow.Show(result.ToString());
                return;
            }

            string NO = posNO.Value.Value.ToString("d3");
            Table.PosS obj = Table.PosS.GetObject(NO);
            if (obj == null)
            {
                order = new Table.PosS((Table.PosS.EnumFlag)Enum.Parse(typeof(Table.PosS.EnumFlag), comboBoxPosType.Text));
                SetValue();
                result = order.Insert();
            }
            else
            {
                order = obj;
                SetValue();

                //如果数据库中PosNO已存在但PosName为空则可以更新
                if (obj.Enable == Table.PosS.EnumEnable.停用)
                    result = order.Update();
                else
                    order = null;
            }

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
            {
                DoubleHConfig.AppConfig.PosNO = NO;
                DoubleHConfig.AppConfig.PosFlag = comboBoxPosType.Text;
                ConfigSerializer.SaveConfig(DoubleHConfig.AppConfig, DbDefine.appConfigFile);
                if (Table.SysConfig.SysConfigParams.PosShenHe)
                {
                    order = null;
                    MessageWindow.Show("数据已更新,请到后台审核.");
                }
                else
                    MessageWindow.Show("程序自动关闭,请重新运行.");

                System.Environment.Exit(0);
            }
            else
                MessageWindow.Show(result.ToString());
        }
    }
}