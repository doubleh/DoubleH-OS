﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table=FCNS.Data.Table;
using DoubleH.Utility.Configuration;
using System.IO.Ports;
using System.Drawing.Printing;

namespace POS
{
    /// <summary>
    /// WindowConfig.xaml 的交互逻辑
    /// </summary>
    public partial class WindowConfig : Window
    {
        DoubleH.Utility.Configuration.PrinterCountConfig selectedPrinter = null;

        public WindowConfig()
        {
            InitializeComponent();
        }

        public void Init()
        {
            InitNormal();
            InitTouch();
            InitPrinter();
            InitEvent();
        }

        private void InitNormal()
        {
            InitTemplete();
            InitLed();

            if (!string.IsNullOrEmpty(DoubleHConfig.AppConfig.PosJiaoBan))
            {
                checkBoxJiaoBan.IsChecked = true;
                timePickerJiaoBan.Value = DateTime.ParseExact(DoubleHConfig.AppConfig.PosJiaoBan, "HHmm", null);
            }

            checkBoxFullScreen.IsChecked = DoubleHConfig.AppConfig.PosFullScreen;

            colorPickerBackground.SelectedColor = (Color)ColorConverter.ConvertFromString(DoubleHConfig.AppConfig.PosBackgroundColor);
            colorPickerForeground.SelectedColor = (Color)ColorConverter.ConvertFromString(DoubleHConfig.AppConfig.PosForegroundColor);

            checkBoxPrintJiaoBan.IsChecked = DoubleHConfig.AppConfig.PosIsPrintJiaoBan;
            checkBoxShowCashboxMoney.IsChecked = DoubleHConfig.AppConfig.ShowCashboxMoney;
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
            listBoxPrinter.SelectionChanged += (ss, ee) => SelectedPrinter();
            integerUpDownPrintCount.ValueChanged += (ss, ee) => ChangedPrintCount();
        }

        private void ChangedPrintCount()
        {
            if (selectedPrinter != null)
                selectedPrinter.PrintCount = integerUpDownPrintCount.Value.Value;
        }

        private void SelectedPrinter()
        {
            if (listBoxPrinter.SelectedItem == null)
                return;

            selectedPrinter = DoubleHConfig.AppConfig.PrinterCount.FirstOrDefault(f => f.PrinterName == listBoxPrinter.SelectedItem.ToString());
            if (selectedPrinter == null)
            {
                selectedPrinter = new PrinterCountConfig() { PrinterName = listBoxPrinter.SelectedItem.ToString() };
                DoubleHConfig.AppConfig.PrinterCount.Add(selectedPrinter);
            }
            integerUpDownPrintCount.Value = selectedPrinter.PrintCount;
        }

        private void InitTouch()
        {
            if (DoubleHConfig.AppConfig.PosProductSMode == 1)
                radioButtonXiaoLiang.IsChecked = true;
            else
                radioButtonZiDingYi.IsChecked = true;

            integerUpDownXiaoLiang.Value = DoubleHConfig.AppConfig.PosProductSCount;

            integerUpDownFontSize.Value = DoubleHConfig.AppConfig.PosProductFontSize;
            checkBoxShowImgNoText.IsChecked = DoubleHConfig.AppConfig.PosProductShowMode == 1;
            checkBoxShowTextNoImg.IsChecked = DoubleHConfig.AppConfig.PosProductShowMode == 2;
        }

        private void InitPrinter()
        {
            foreach (String printerName in PrinterSettings.InstalledPrinters)
                listBoxPrinter.Items.Add(printerName);
        }

        private void InitTemplete()
        {
            labelPosFlag.Content = App.Pos.Flag.ToString();

            comboBoxTemplete.ItemsSource = App.AllTemplete.Where(f => (f.PosFlag == (Table.PosS.EnumFlag)Enum.Parse(typeof(Table.PosS.EnumFlag), App.Pos.Flag.ToString())));
            comboBoxTemplete.DisplayMemberPath = "Text";
            comboBoxTemplete.SelectedValuePath = "Name";
            comboBoxTemplete.SelectedValue = DoubleHConfig.AppConfig.PosTempleteName;
        }

        private void InitLed()
        {
            comboBoxLed.Items.Add("无");
            foreach (string s in SerialPort.GetPortNames())
                comboBoxLed.Items.Add(s);

            comboBoxLed.Text = DoubleHConfig.AppConfig.PosLed;
        }

        private void Save()
        {
            if (comboBoxTemplete.SelectedItem != null)
                DoubleHConfig.AppConfig.PosTempleteName = comboBoxTemplete.SelectedValue.ToString();

            DoubleHConfig.AppConfig.PosFullScreen = checkBoxFullScreen.IsChecked.Value;
            if (radioButtonXiaoLiang.IsChecked.Value)
                DoubleHConfig.AppConfig.PosProductSMode = 1;
            else
                DoubleHConfig.AppConfig.PosProductSMode = 0;

            DoubleHConfig.AppConfig.PosProductSCount = integerUpDownXiaoLiang.Value.Value;

            DoubleHConfig.AppConfig.PosBackgroundColor = colorPickerBackground.SelectedColorText;
            DoubleHConfig.AppConfig.PosForegroundColor = colorPickerForeground.SelectedColorText;
            DoubleHConfig.AppConfig.PosLed = comboBoxLed.Text;

            DoubleHConfig.AppConfig.PosIsPrintJiaoBan = checkBoxPrintJiaoBan.IsChecked.Value;
            DoubleHConfig.AppConfig.ShowCashboxMoney = checkBoxShowCashboxMoney.IsChecked.Value;

            DoubleHConfig.AppConfig.PosProductFontSize = integerUpDownFontSize.Value.Value;
            int i = 0;
            if (checkBoxShowImgNoText.IsChecked.Value)
                i = 1;
            if (checkBoxShowTextNoImg.IsChecked.Value)
                i += 2;

            DoubleHConfig.AppConfig.PosProductShowMode = i;

            DoubleHConfig.AppConfig.PosJiaoBan = (checkBoxJiaoBan.IsChecked.Value ? timePickerJiaoBan.Value.Value.ToString("HHmm") : string.Empty);
            this.Close();
        }
    }
}