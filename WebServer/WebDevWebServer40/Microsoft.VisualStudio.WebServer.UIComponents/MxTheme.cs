using Microsoft.Win32;
using System;
using System.Drawing;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public sealed class MxTheme
	{
		private static bool UserPreferencesChangingHandlerSet;
		private static bool AppThemedChecked;
		private static bool AppThemed;
		private static Brush menuBarBrush;
		public static bool IsAppThemed
		{
			get
			{
				if (!MxTheme.AppThemedChecked)
				{
					MxTheme.AppThemed = (Environment.OSVersion.Version.CompareTo(new Version(5, 1, 0, 0)) >= 0 && Interop.IsAppThemed() != 0);
					MxTheme.AppThemedChecked = true;
				}
				return MxTheme.AppThemed;
			}
		}
		public static Brush MenuBarBrush
		{
			get
			{
				if (MxTheme.IsAppThemed)
				{
					if (MxTheme.menuBarBrush == null)
					{
						int sysColor = Interop.GetSysColor(30);
						int red = sysColor & 255;
						int green = sysColor >> 8 & 255;
						int blue = sysColor >> 16 & 255;
						MxTheme.menuBarBrush = new SolidBrush(Color.FromArgb(255, red, green, blue));
						MxTheme.EnsureUserPreferencesListener();
					}
					return MxTheme.menuBarBrush;
				}
				return SystemBrushes.Menu;
			}
		}
		private MxTheme()
		{
		}
		private static void EnsureUserPreferencesListener()
		{
			if (!MxTheme.UserPreferencesChangingHandlerSet)
			{
				MxTheme.UserPreferencesChangingHandlerSet = true;
				SystemEvents.UserPreferenceChanged += new UserPreferenceChangedEventHandler(MxTheme.OnUserPreferenceChanged);
			}
		}
		private static void OnUserPreferenceChanged(object sender, UserPreferenceChangedEventArgs e)
		{
			if (e.Category == UserPreferenceCategory.Color && MxTheme.menuBarBrush != null)
			{
				MxTheme.menuBarBrush.Dispose();
				MxTheme.menuBarBrush = null;
			}
		}
	}
}
