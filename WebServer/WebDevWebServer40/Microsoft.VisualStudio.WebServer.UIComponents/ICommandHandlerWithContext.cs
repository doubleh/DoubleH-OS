using System;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public interface ICommandHandlerWithContext
	{
		bool HandleCommand(Command command, object context);
		bool UpdateCommand(Command command, object context);
	}
}
