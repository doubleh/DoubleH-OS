using System;
using System.Windows.Forms;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public class MxButton : Button
	{
		public MxButton()
		{
			if (MxTheme.IsAppThemed)
			{
				base.FlatStyle = FlatStyle.System;
				return;
			}
			base.FlatStyle = FlatStyle.Popup;
		}
		private bool ShouldSerializeFlatStyle()
		{
			return false;
		}
	}
}
