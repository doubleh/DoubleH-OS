using System;
using System.Drawing;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public class ShowContextMenuEventArgs : EventArgs
	{
		private Point _location;
		private bool _keyboard;
		public Point Location
		{
			get
			{
				return this._location;
			}
		}
		public bool UsingKeyboard
		{
			get
			{
				return this._keyboard;
			}
		}
		public ShowContextMenuEventArgs(Point location, bool keyboard)
		{
			this._location = location;
			this._keyboard = keyboard;
		}
	}
}
