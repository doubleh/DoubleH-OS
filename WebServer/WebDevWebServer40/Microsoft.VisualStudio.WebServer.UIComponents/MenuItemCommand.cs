using System;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public class MenuItemCommand : Command
	{
		public MenuItemCommand(Type commandGroup, int commandID, MxMenuItem menuItem) : base(commandGroup, commandID, menuItem)
		{
			this.Visible = menuItem.Visible;
			base.VisibleChanged = false;
		}
		public override void UpdateCommandUI()
		{
			MxMenuItem mxMenuItem = base.CommandUI as MxMenuItem;
			if (base.VisibleChanged)
			{
				mxMenuItem.Visible = this.Visible;
				base.VisibleChanged = false;
			}
			mxMenuItem.Checked = this.Checked;
			mxMenuItem.Enabled = this.Enabled;
			if (base.TextChanged)
			{
				base.TextChanged = false;
				mxMenuItem.Text = this.Text;
			}
			if (base.HelpTextChanged || base.GlyphChanged)
			{
				if (base.HelpTextChanged)
				{
					base.HelpTextChanged = false;
					mxMenuItem.HelpText = this.HelpText;
				}
				if (base.GlyphChanged)
				{
					base.GlyphChanged = false;
					mxMenuItem.Glyph = this.Glyph;
				}
			}
		}
	}
}
