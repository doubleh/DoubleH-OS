using System;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public class MxMenuItem : MenuItem
	{
		private const int SeparatorHeight = 8;
		private const int GlyphWidth = 18;
		private const int GlyphHeight = 18;
		private const int HorizontalPadding = 2;
		private const int VerticalPadding = 1;
		private const int HighlightOpacity1 = 120;
		private const int HighlightOpacity2 = 75;
		private const int DisabledOpacity = 120;
		private const int Image_CheckGlyph = 0;
		private const int Image_RestoreGlyph = 1;
		private const int Image_MinimizeGlyph = 2;
		private const int Image_MaximizeGlyph = 3;
		private const int Image_CloseGlyph = 4;
		private static ImageList stockGlyphs;
		private static int sharedMaxMenuTextWidth;
		private static int sharedMaxMenuShortcutWidth;
		private Image _glyph;
		private string _helpText;
		private string _shortcutText;
		internal static readonly MxMenuItem MoveMenuItem;
		internal static readonly MxMenuItem SizeMenuItem;
		internal static readonly MxMenuItem SeparatorMenuItem;
		public Image Glyph
		{
			get
			{
				return this._glyph;
			}
			set
			{
				this._glyph = value;
			}
		}
		public string HelpText
		{
			get
			{
				return this._helpText;
			}
			set
			{
				this._helpText = value;
			}
		}
		private bool IsSeparator
		{
			get
			{
				return base.Text == "-";
			}
		}
		private bool IsTopLevel
		{
			get
			{
				return base.Parent == base.GetMainMenu() && base.Parent != null;
			}
		}
		private Font MenuFont
		{
			get
			{
				Font font = null;
				MainMenu mainMenu = base.GetMainMenu();
				if (mainMenu != null)
				{
					font = mainMenu.GetForm().Font;
				}
				else
				{
					font = SystemInformation.MenuFont;
					if (font == null)
					{
						font = new Font("Tahoma", 8f);
					}
				}
				return font;
			}
		}
		static MxMenuItem()
		{
			MxMenuItem.stockGlyphs = new ImageList();
			MxMenuItem.stockGlyphs.ImageSize = new Size(16, 16);
			MxMenuItem.stockGlyphs.TransparentColor = Color.Fuchsia;
			ImageList.ImageCollection images = MxMenuItem.stockGlyphs.Images;
			images.AddStrip(new Bitmap(typeof(MxMenuItem), "StockMenuItemGlyphs.bmp"));
			MxMenuItem.MoveMenuItem = new MxMenuItem("&Move");
			MxMenuItem.SizeMenuItem = new MxMenuItem("&Size");
			MxMenuItem.SeparatorMenuItem = new MxMenuItem("-");
		}
		public MxMenuItem() : this(string.Empty)
		{
		}
		public MxMenuItem(string text):base(MenuMerge.Add, 0, Shortcut.None, text, null, null, null, null)
		{
            //this._shortcutText = string.Empty;
            //base.ctor(MenuMerge.Add, 0, Shortcut.None, text, null, null, null, null);
			this._helpText = string.Empty;
			base.OwnerDraw = true;
		}
		public MxMenuItem(string text, string helpText, EventHandler clickHandler):base(MenuMerge.Add, 0, Shortcut.None, text, clickHandler, null, null, null)
		{
            //this._shortcutText = string.Empty;
            //base.(MenuMerge.Add, 0, Shortcut.None, text, clickHandler, null, null, null);
            
			this._helpText = helpText;
			base.OwnerDraw = true;
		}
		public MxMenuItem(string text, string helpText, EventHandler clickHandler, Image glyph):base(MenuMerge.Add, 0, Shortcut.None, text, clickHandler, null, null, null)
		{
            //this._shortcutText = string.Empty;

			this._helpText = helpText;
			this._glyph = glyph;
			base.OwnerDraw = true;
		}
		protected virtual void DrawGlyph(Graphics g, Rectangle glyphBounds, bool enabled)
		{
			if (this._glyph == null && base.Checked)
			{
				Image image = MxMenuItem.stockGlyphs.Images[0];
			}
		}
		protected override void OnDrawItem(DrawItemEventArgs e)
		{
			Graphics graphics = e.Graphics;
			bool isTopLevel = this.IsTopLevel;
			bool flag = false;
			if (base.Parent is MainMenu && ((MainMenu)base.Parent).RightToLeft == RightToLeft.Yes)
			{
				flag = true;
			}
			if (base.Parent is ContextMenu && ((ContextMenu)base.Parent).RightToLeft == RightToLeft.Yes)
			{
				flag = true;
			}
			if (isTopLevel)
			{
				graphics.FillRectangle(MxTheme.MenuBarBrush, e.Bounds);
			}
			else
			{
				graphics.FillRectangle(SystemBrushes.Menu, e.Bounds);
			}
			if (this.IsSeparator)
			{
				Pen pen = new Pen(Color.FromArgb(120, SystemColors.MenuText));
				int x = e.Bounds.X + 2 + 18;
				int x2 = e.Bounds.X + e.Bounds.Width - 2;
				int num = e.Bounds.Y + e.Bounds.Height / 2;
				graphics.DrawLine(pen, x, num, x2, num);
				pen.Dispose();
				return;
			}
			bool flag2 = (e.State & DrawItemState.Selected) == DrawItemState.Selected;
			bool flag3 = (e.State & DrawItemState.HotLight) == DrawItemState.HotLight;
			bool flag4 = (e.State & (DrawItemState.Disabled | DrawItemState.Grayed | DrawItemState.Inactive)) == DrawItemState.None;
			Rectangle rect = new Rectangle(e.Bounds.X, e.Bounds.Y, e.Bounds.Width - 1, e.Bounds.Height - 1);
			if (flag4)
			{
				if (flag2 || (isTopLevel && flag3))
				{
					Brush brush = new SolidBrush(Color.FromArgb(120, SystemColors.Highlight));
					Brush brush2 = new SolidBrush(Color.FromArgb(75, Color.White));
					graphics.FillRectangle(brush, rect);
					graphics.FillRectangle(brush2, rect);
					graphics.DrawRectangle(SystemPens.Highlight, rect);
					brush.Dispose();
					brush2.Dispose();
				}
			}
			else
			{
				if (flag2)
				{
					graphics.DrawRectangle(SystemPens.Highlight, rect);
				}
			}
			Font menuFont = this.MenuFont;
			Rectangle bounds = e.Bounds;
			StringFormat stringFormat = new StringFormat(StringFormatFlags.NoWrap);
			Brush brush3;
			if (flag4)
			{
				brush3 = new SolidBrush(SystemColors.MenuText);
			}
			else
			{
				brush3 = new SolidBrush(Color.FromArgb(120, SystemColors.MenuText));
			}
			if (!isTopLevel)
			{
				int num2 = 22;
				bounds = new Rectangle(bounds.X + num2, bounds.Y + 1, bounds.Width - num2 - 4, bounds.Height - 2);
			}
			else
			{
				stringFormat.Alignment = StringAlignment.Center;
			}
			if ((e.State & DrawItemState.NoAccelerator) == DrawItemState.NoAccelerator)
			{
				stringFormat.HotkeyPrefix = HotkeyPrefix.Hide;
			}
			else
			{
				stringFormat.HotkeyPrefix = HotkeyPrefix.Show;
			}
			stringFormat.LineAlignment = StringAlignment.Center;
			if (flag)
			{
				TextRenderer.DrawText(graphics, base.Text, menuFont, bounds, SystemColors.MenuText, TextFormatFlags.RightToLeft);
			}
			else
			{
				graphics.DrawString(base.Text, menuFont, brush3, bounds, stringFormat);
			}
			if (!isTopLevel)
			{
				if (this._shortcutText != null)
				{
					stringFormat.Alignment = StringAlignment.Far;
					stringFormat.HotkeyPrefix = HotkeyPrefix.Hide;
					if (flag)
					{
						TextRenderer.DrawText(graphics, this._shortcutText, menuFont, bounds, SystemColors.MenuText, TextFormatFlags.RightToLeft);
					}
					else
					{
						graphics.DrawString(this._shortcutText, menuFont, brush3, bounds, stringFormat);
					}
				}
				Rectangle glyphBounds = new Rectangle(e.Bounds.X + 2, e.Bounds.Y + 1, 18, 18);
				glyphBounds.Inflate(-1, -1);
				this.DrawGlyph(graphics, glyphBounds, flag4);
			}
			stringFormat.Dispose();
			brush3.Dispose();
		}
		protected override void OnMeasureItem(MeasureItemEventArgs e)
		{
			if (this.IsSeparator)
			{
				e.ItemWidth = 4;
				e.ItemHeight = 8;
				return;
			}
			bool flag = false;
			if (base.Parent is MainMenu && ((MainMenu)base.Parent).RightToLeft == RightToLeft.Yes)
			{
				flag = true;
			}
			if (base.Parent is ContextMenu && ((ContextMenu)base.Parent).RightToLeft == RightToLeft.Yes)
			{
				flag = true;
			}
			if (e.Index == 0)
			{
				MxMenuItem.sharedMaxMenuTextWidth = 0;
				MxMenuItem.sharedMaxMenuShortcutWidth = 0;
			}
			Font menuFont = this.MenuFont;
			StringFormat stringFormat = new StringFormat(StringFormatFlags.NoWrap | StringFormatFlags.NoClip);
			Size size;
			if (flag)
			{
				size = TextRenderer.MeasureText(e.Graphics, base.Text, menuFont);
			}
			else
			{
				size = e.Graphics.MeasureString(base.Text, menuFont, new PointF(0f, 0f), stringFormat).ToSize();
			}
			e.ItemHeight = Math.Max(18, size.Height) + 2;
			if (!this.IsTopLevel)
			{
				MxMenuItem.sharedMaxMenuTextWidth = Math.Max(MxMenuItem.sharedMaxMenuTextWidth, size.Width);
				if (this._shortcutText != null)
				{
					if (flag)
					{
						size = TextRenderer.MeasureText(e.Graphics, this._shortcutText, menuFont);
					}
					else
					{
						size = e.Graphics.MeasureString(this._shortcutText, menuFont, new PointF(0f, 0f), stringFormat).ToSize();
					}
					MxMenuItem.sharedMaxMenuShortcutWidth = Math.Max(MxMenuItem.sharedMaxMenuShortcutWidth, size.Width);
				}
				e.ItemWidth = MxMenuItem.sharedMaxMenuTextWidth + MxMenuItem.sharedMaxMenuShortcutWidth + 18 + 10;
			}
			else
			{
				e.ItemWidth = size.Width - 10;
			}
			stringFormat.Dispose();
		}
	}
}
