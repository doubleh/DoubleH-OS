using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Windows.Forms;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public class MxTextBox : TextBox
	{
		private bool _flatAppearance;
		private bool _mouseHover;
		private bool _numeric;
		private bool _alwaysShowFocusCues;
		[DefaultValue(false)]
		public bool AlwaysShowFocusCues
		{
			get
			{
				return this._alwaysShowFocusCues;
			}
			set
			{
				if (value != this._alwaysShowFocusCues)
				{
					this._alwaysShowFocusCues = value;
					if (base.IsHandleCreated)
					{
						base.Invalidate();
					}
				}
			}
		}
		protected override CreateParams CreateParams
		{
			[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
			get
			{
				CreateParams createParams = base.CreateParams;
				if (this._numeric)
				{
					createParams.Style |= 8192;
				}
				return createParams;
			}
		}
		[DefaultValue(false)]
		public bool FlatAppearance
		{
			get
			{
				return this._flatAppearance;
			}
			set
			{
				if (value != this._flatAppearance)
				{
					this._flatAppearance = value;
					if (base.IsHandleCreated)
					{
						base.Invalidate();
					}
				}
			}
		}
		[DefaultValue(false)]
		public bool Numeric
		{
			get
			{
				return this._numeric;
			}
			set
			{
				if (value != this._numeric)
				{
					this._numeric = value;
					if (base.IsHandleCreated)
					{
						base.RecreateHandle();
					}
				}
			}
		}
		private void DrawFlatTextBox()
		{
			if (MxTheme.IsAppThemed)
			{
				return;
			}
			Interop.RECT rECT = default(Interop.RECT);
			Interop.GetWindowRect(base.Handle, ref  rECT);
			IntPtr windowDC = Interop.GetWindowDC(base.Handle);
			try
			{
				bool flag = (this._alwaysShowFocusCues || this._mouseHover || base.ContainsFocus) && base.Enabled;
				int sysColor = Interop.GetSysColor(15);
				int sysColor2 = Interop.GetSysColor(16);
				int sysColor3 = Interop.GetSysColor(20);
				rECT.bottom -= rECT.top;
				rECT.right -= rECT.left;
				rECT.left = 0;
				rECT.top = 0;
				int topLeftColor;
				int bottomRightColor;
				if (flag)
				{
					topLeftColor = sysColor2;
					bottomRightColor = sysColor3;
				}
				else
				{
					bottomRightColor = (topLeftColor = sysColor);
				}
				this.DrawRectangle(windowDC, rECT.left, rECT.top, rECT.right - rECT.left, rECT.bottom - rECT.top, topLeftColor, bottomRightColor);
				rECT.left++;
				rECT.top++;
				rECT.right--;
				rECT.bottom--;
				if (base.Enabled && (flag || !base.ReadOnly))
				{
					bottomRightColor = (topLeftColor = sysColor);
				}
				else
				{
					bottomRightColor = (topLeftColor = sysColor3);
				}
				this.DrawRectangle(windowDC, rECT.left, rECT.top, rECT.right - rECT.left, rECT.bottom - rECT.top, topLeftColor, bottomRightColor);
			}
			finally
			{
				Interop.ReleaseDC(base.Handle, windowDC);
			}
		}
		private void DrawRectangle(IntPtr hdc, int x, int y, int cx, int cy, int topLeftColor, int bottomRightColor)
		{
			Interop.RECT rECT = default(Interop.RECT);
			Interop.SetBkColor(hdc, topLeftColor);
			rECT.left = x;
			rECT.top = y;
			rECT.right = cx - 1 + x;
			rECT.bottom = 1 + y;
			Interop.ExtTextOut(hdc, 0, 0, 2, ref rECT, null, 0, null);
			rECT.left = x;
			rECT.top = y;
			rECT.right = 1 + x;
			rECT.bottom = cy - 1 + y;
			Interop.ExtTextOut(hdc, 0, 0, 2, ref rECT, null, 0, null);
			Interop.SetBkColor(hdc, bottomRightColor);
			rECT.left = x + cx - 1;
			rECT.top = y;
			rECT.right = x + cx;
			rECT.bottom = cy + y;
			Interop.ExtTextOut(hdc, 0, 0, 2, ref rECT, null, 0, null);
			rECT.left = x;
			rECT.top = y + cy - 1;
			rECT.right = x + cx;
			rECT.bottom = cy + y;
			Interop.ExtTextOut(hdc, 0, 0, 2, ref rECT, null, 0, null);
		}
		protected override void OnGotFocus(EventArgs e)
		{
			base.OnGotFocus(e);
			if (this._flatAppearance && !this._alwaysShowFocusCues)
			{
				this.DrawFlatTextBox();
			}
		}
		protected override void OnLostFocus(EventArgs e)
		{
			base.OnLostFocus(e);
			if (this._flatAppearance && !this._alwaysShowFocusCues)
			{
				this.DrawFlatTextBox();
			}
		}
		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);
			if (this._flatAppearance)
			{
				this._mouseHover = true;
				if (!this._alwaysShowFocusCues)
				{
					this.DrawFlatTextBox();
				}
			}
		}
		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave(e);
			if (this._flatAppearance)
			{
				this._mouseHover = false;
				if (!this._alwaysShowFocusCues)
				{
					this.DrawFlatTextBox();
				}
			}
		}
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		protected override void WndProc(ref Message m)
		{
			if (!MxTheme.IsAppThemed && this._flatAppearance && (m.Msg == 15 || m.Msg == 133))
			{
				this.DefWndProc(ref m);
				this.DrawFlatTextBox();
				return;
			}
			base.WndProc(ref m);
		}
	}
}
