using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public class TaskForm : MxForm
	{
		private string _taskCaption;
		private string _taskDescription;
		private Image _taskGlyph;
		private BorderStyle _taskBorderStyle;
		[DefaultValue(BorderStyle.None), Category("Appearance")]
		public BorderStyle TaskBorderStyle
		{
			set
			{
				if (value != BorderStyle.None && value != BorderStyle.FixedSingle)
				{
					throw new ArgumentOutOfRangeException("value", value, "TaskBorderStyle should be either None or FixedSingle");
				}
				this._taskBorderStyle = value;
				if (base.IsHandleCreated)
				{
					base.Invalidate();
				}
			}
		}
		[DefaultValue(""), Category("Appearance")]
		public string TaskCaption
		{
			set
			{
				this._taskCaption = value;
				if (base.IsHandleCreated)
				{
					base.Invalidate();
				}
			}
		}
		[Category("Appearance"), DefaultValue("")]
		public string TaskDescription
		{
			set
			{
				this._taskDescription = value;
				if (base.IsHandleCreated)
				{
					base.Invalidate();
				}
			}
		}
		[DefaultValue(null), Category("Appearance")]
		public Image TaskGlyph
		{
			set
			{
				this._taskGlyph = value;
				if (base.IsHandleCreated)
				{
					base.Invalidate();
				}
			}
		}
		public TaskForm()
		{
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.OptimizedDoubleBuffer, true);
		}
		public TaskForm(IServiceProvider serviceProvider) : base(serviceProvider)
		{
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.OptimizedDoubleBuffer, true);
		}
	}
}
