using System;
using System.Runtime.InteropServices;
using System.Text;
namespace Microsoft.VisualStudio
{
	internal sealed class Interop
	{
		public delegate int WNDPROC(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		public struct WINTRUST_DATA
		{
			public int cbStruct;
			private IntPtr pPolicyCallbackData;
			private IntPtr pSIPClientData;
			public int dwUIChoice;
			public int fdwRevocationChecks;
			public int dwUnionChoice;
			private IntPtr pSomething;
			public int dwStateAction;
			private IntPtr hWVTStateData;
			private IntPtr pwszURLReference;
			public int dwProvFlags;
		}
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		public struct WINTRUST_FILE_INFO
		{
			public int cbStruct;
			private IntPtr pcwszFilePath;
			private IntPtr hFile;
		}
		[StructLayout(LayoutKind.Sequential)]
		public class SCROLLINFO
		{
			public int cbSize;
			public int fMask;
			public int nMin;
			public int nMax;
			public int nPage;
			public int nPos;
			public int nTrackPos;
			public SCROLLINFO()
			{
				this.cbSize = Marshal.SizeOf(typeof(Interop.SCROLLINFO));
			}
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential)]
		public class BROWSEINFO
		{
			private IntPtr hwndOwner = IntPtr.Zero;
			private IntPtr pidlRoot = IntPtr.Zero;
			private IntPtr pszDisplayName = IntPtr.Zero;
			[MarshalAs(UnmanagedType.LPTStr)]
			public string lpszTitle;
			public int ulFlags;
			private IntPtr lpfn = IntPtr.Zero;
			private IntPtr lParam = IntPtr.Zero;
			public int iImage;
		}
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("00000002-0000-0000-c000-000000000046")]
		public interface IMalloc
		{
			IntPtr Alloc(int cb);
			void Free(IntPtr pv);
			IntPtr Realloc(IntPtr pv, int cb);
			int GetSize(IntPtr pv);
			int DidAlloc(IntPtr pv);
			void HeapMinimize();
		}
		[ComVisible(false)]
		public struct MSG
		{
			private IntPtr hwnd;
			public int message;
			private IntPtr wParam;
			private IntPtr lParam;
			public int time;
			public int pt_x;
			public int pt_y;
		}
		[ComVisible(false)]
		public struct PAINTSTRUCT
		{
			private IntPtr hdc;
			public bool fErase;
			public int rcPaint_left;
			public int rcPaint_top;
			public int rcPaint_right;
			public int rcPaint_bottom;
			public bool fRestore;
			public bool fIncUpdate;
			public int reserved1;
			public int reserved2;
			public int reserved3;
			public int reserved4;
			public int reserved5;
			public int reserved6;
			public int reserved7;
			public int reserved8;
		}
		[ComVisible(true)]
		public struct POINT
		{
			public int x;
			public int y;
			public POINT(int x, int y)
			{
				this.x = x;
				this.y = y;
			}
		}
		[ComVisible(true)]
		public struct RECT
		{
			public int left;
			public int top;
			public int right;
			public int bottom;
			public int Height
			{
				get
				{
					return this.bottom - this.top;
				}
			}
			public int Width
			{
				get
				{
					return this.right - this.left;
				}
			}
			public RECT(int left, int top, int right, int bottom)
			{
				this.left = left;
				this.top = top;
				this.right = right;
				this.bottom = bottom;
			}
			public static Interop.RECT FromXYWH(int x, int y, int width, int height)
			{
				return new Interop.RECT(x, y, x + width, y + height);
			}
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential)]
		public class MEASUREITEMSTRUCT
		{
			public int CtlType;
			public int CtlID;
			public int itemID;
			public int itemWidth;
			public int itemHeight;
			private IntPtr itemData = IntPtr.Zero;
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential)]
		public class DRAWITEMSTRUCT
		{
			public int CtlType;
			public int CtlID;
			public int itemID;
			public int itemAction;
			public int itemState;
			private IntPtr hwndItem = IntPtr.Zero;
			private IntPtr _hDC = IntPtr.Zero;
			public Interop.RECT rcItem = new Interop.RECT(0, 0, 0, 0);
			private IntPtr itemData = IntPtr.Zero;
			public IntPtr hDC
			{
				get
				{
					return this._hDC;
				}
				set
				{
					this._hDC = value;
				}
			}
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		public class NOTIFYICONDATA
		{
			public int cbSize = Marshal.SizeOf(typeof(Interop.NOTIFYICONDATA));
			private IntPtr _hWnd = IntPtr.Zero;
			public int uID;
			public int uFlags;
			public int uCallbackMessage;
			private IntPtr _hIcon = IntPtr.Zero;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
			public string szTip;
			public int dwState;
			public int dwStateMask;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string szInfo;
			public int uTimeout;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
			public string szTitle;
			public int dwInfoFlags;
			public IntPtr hWnd
			{
				set
				{
					this._hWnd = value;
				}
			}
			public IntPtr hIcon
			{
				set
				{
					this._hIcon = value;
				}
			}
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential)]
		public class INITCOMMONCONTROLSEX
		{
			public int dwSize;
			public int dwICC;
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		public class TEXTMETRIC
		{
			public int tmHeight;
			public int tmAscent;
			public int tmDescent;
			public int tmInternalLeading;
			public int tmExternalLeading;
			public int tmAveCharWidth;
			public int tmMaxCharWidth;
			public int tmWeight;
			public int tmOverhang;
			public int tmDigitizedAspectX;
			public int tmDigitizedAspectY;
			public char tmFirstChar;
			public char tmLastChar;
			public char tmDefaultChar;
			public char tmBreakChar;
			public byte tmItalic;
			public byte tmUnderlined;
			public byte tmStruckOut;
			public byte tmPitchAndFamily;
			public byte tmCharSet;
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		public class TOOLINFO
		{
			public int cbSize;
			public int uFlags;
			private IntPtr hwnd = IntPtr.Zero;
			private IntPtr uId = IntPtr.Zero;
			public Interop.RECT rect = new Interop.RECT(0, 0, 0, 0);
			private IntPtr hinst = IntPtr.Zero;
			public string lpszText;
			private IntPtr lParam = IntPtr.Zero;
			public TOOLINFO()
			{
				this.cbSize = Marshal.SizeOf(typeof(Interop.TOOLINFO));
			}
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential)]
		public class NMHDR
		{
			private IntPtr hwndFrom = IntPtr.Zero;
			public int idFrom;
			public int code;
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential)]
		public class NMCUSTOMDRAW
		{
			public Interop.NMHDR nmcd;
			public int dwDrawStage;
			private IntPtr hdc = IntPtr.Zero;
			public Interop.RECT rc = new Interop.RECT(0, 0, 0, 0);
			public int dwItemSpec;
			public int uItemState;
			private IntPtr lItemlParam = IntPtr.Zero;
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential)]
		public class NMTBCUSTOMDRAW
		{
			public Interop.NMCUSTOMDRAW nmcd;
			private IntPtr hbrMonoDither = IntPtr.Zero;
			private IntPtr hbrLines = IntPtr.Zero;
			private IntPtr hpenLines = IntPtr.Zero;
			public int clrText;
			public int clrMark;
			public int clrTextHighlight;
			public int clrBtnFace;
			public int clrBtnHighlight;
			public int clrHighlightHotTrack;
			public Interop.RECT rc = new Interop.RECT(0, 0, 0, 0);
			public int nStringBkMode;
			public int nHLStringBkMode;
		}
		[ComVisible(true)]
		public struct SIZE
		{
			public int x;
			public int y;
		}
		[ComVisible(false)]
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		public class WNDCLASS
		{
			public int style;
			public Interop.WNDPROC lpfnWndProc;
			public int cbClsExtra;
			public int cbWndExtra;
			private IntPtr hInstance = IntPtr.Zero;
			private IntPtr hIcon = IntPtr.Zero;
			private IntPtr hCursor = IntPtr.Zero;
			private IntPtr hbrBackground = IntPtr.Zero;
			public string lpszMenuName;
			public string lpszClassName;
		}
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("21b8916c-f28e-11d2-a473-00c04f8ef448")]
		public interface IAssemblyEnum
		{
			[PreserveSig]
			int GetNextAssembly(IntPtr appCtx, out Interop.IAssemblyName ppName, uint dwFlags);
			[PreserveSig]
			int Reset();
			[PreserveSig]
			int Clone(out Interop.IAssemblyEnum ppEnum);
		}
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("CD193BC0-B4BC-11d2-9833-00C04FC31D2E")]
		public interface IAssemblyName
		{
			[PreserveSig]
			int SetProperty(uint PropertyId, IntPtr pvProperty, uint cbProperty);
			[PreserveSig]
			int GetProperty(uint PropertyId, IntPtr pvProperty, ref uint pcbProperty);
			[PreserveSig]
			int Finalize();
			[PreserveSig]
			int GetDisplayName(IntPtr szDisplayName, ref uint pccDisplayName, uint dwDisplayFlags);
			[PreserveSig]
			int BindToObject(ref Guid refIID, IntPtr pAsmBindSink, IntPtr pApplicationContext, [MarshalAs(UnmanagedType.LPWStr)] string szCodeBase, long llFlags, int pvReserved, uint cbReserved, out int ppv);
			[PreserveSig]
			int GetName(out uint lpcwBuffer, out int pwzName);
			[PreserveSig]
			int GetVersion(out uint pdwVersionHi, out uint pdwVersionLow);
			[PreserveSig]
			int IsEqual(Interop.IAssemblyName pName, uint dwCmpFlags);
			[PreserveSig]
			int Clone(out Interop.IAssemblyName pName);
		}
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("e707dcde-d1cd-11d2-bab9-00c04f8eceae")]
		public interface IAssemblyCache
		{
			[PreserveSig]
			int UninstallAssembly(uint dwFlags, [MarshalAs(UnmanagedType.LPWStr)] string pszAssemblyName, IntPtr pvReserved, out uint pulDisposition);
			[PreserveSig]
			int QueryAssemblyInfo(uint dwFlags, [MarshalAs(UnmanagedType.LPWStr)] string pszAssemblyName, IntPtr pAsmInfo);
			[PreserveSig]
			int CreateAssemblyCacheItem(uint dwFlags, IntPtr pvReserved, out IntPtr ppAsmItem, [MarshalAs(UnmanagedType.LPWStr)] string pszAssemblyName);
			[PreserveSig]
			int CreateAssemblyScavenger(out object ppAsmScavenger);
			[PreserveSig]
			int InstallAssembly(uint dwFlags, [MarshalAs(UnmanagedType.LPWStr)] string pszManifestFilePath, IntPtr pvReserved);
		}
		public const int S_OK = 0;
		public const int WS_EX_LAYOUTRTL = 4194304;
		public const int WM_USER = 1024;
		public const int WM_CHAR = 258;
		public const int WM_CLOSE = 16;
		public const int WM_COMMAND = 273;
		public const int WM_CONTEXTMENU = 123;
		public const int WM_CREATE = 1;
		public const int WM_DESTROY = 2;
		public const int WM_DRAWITEM = 43;
		public const int WM_ERASEBKGND = 20;
		public const int WM_HSCROLL = 276;
		public const int WM_KEYUP = 257;
		public const int WM_KEYDOWN = 256;
		public const int WM_KILLFOCUS = 8;
		public const int WM_LBUTTONDBLCLK = 515;
		public const int WM_LBUTTONDOWN = 513;
		public const int WM_LBUTTONUP = 514;
		public const int WM_MBUTTONDBLCLK = 521;
		public const int WM_MBUTTONDOWN = 519;
		public const int WM_MBUTTONUP = 520;
		public const int WM_MDIACTIVATE = 546;
		public const int WM_MEASUREITEM = 44;
		public const int WM_MOUSEMOVE = 512;
		public const int WM_NCPAINT = 133;
		public const int WM_NOTIFY = 78;
		public const int WM_PAINT = 15;
		public const int WM_RBUTTONDBLCLK = 518;
		public const int WM_RBUTTONDOWN = 516;
		public const int WM_RBUTTONUP = 517;
		public const int WM_REFLECT = 8192;
		public const int WM_SETCURSOR = 32;
		public const int WM_SETFOCUS = 7;
		public const int WM_SETREDRAW = 11;
		public const int WM_TIMER = 275;
		public const int WM_TRAYMESSAGE = 2048;
		public const int WM_VSCROLL = 277;
		public const int LVM_FIRST = 4096;
		public const int LVM_GETEDITCONTROL = 4120;
		public const int CBN_CLOSEUP = 8;
		public const int CBN_EDITUPDATE = 6;
		public const int EM_LINEFROMCHAR = 201;
		public const int EM_LINEINDEX = 187;
		public const int HWND_NOTOPMOST = -2;
		public const int SW_SHOWNORMAL = 1;
		public const int SWP_NOACTIVATE = 16;
		public const int SWP_NOMOVE = 2;
		public const int SWP_NOSIZE = 1;
		public const int SWP_NOZORDER = 4;
		public const int SWP_SHOWWINDOW = 64;
		public const int GW_CHILD = 5;
		public const int CDDS_PREPAINT = 1;
		public const int CDDS_ITEMPREPAINT = 65537;
		public const int CDDS_ITEMPOSTPAINT = 65538;
		public const int CDIS_CHECKED = 8;
		public const int CDIS_HOT = 64;
		public const int CDIS_DISABLED = 4;
		public const int CDRF_DODEFAULT = 0;
		public const int CDRF_SKIPDEFAULT = 4;
		public const int CDRF_NOTIFYPOSTPAINT = 16;
		public const int CDRF_NOTIFYITEMDRAW = 32;
		public const int FSB_ENCARTA_MODE = 1;
		public const int ICC_TAB_CLASSES = 8;
		public const int LVM_GETTOOLTIPS = 4174;
		public const int LVM_SETEXTENDEDLISTVIEWSTYLE = 4150;
		public const int LVS_EX_FLATSB = 256;
		public const int LVS_EX_INFOTIP = 1024;
		public const int LVS_OWNERDRAWFIXED = 1024;
		public const int NM_CUSTOMDRAW = -12;
		public const int SB_GETRECT = 1034;
		public const int TB_GETITEMRECT = 1053;
		public const int TB_GETTOOLTIPS = 1059;
		public const int TTM_ADDTOOLA = 1028;
		public const int TTM_ADDTOOLW = 1074;
		public const int TTM_DELTOOLA = 1029;
		public const int TTM_DELTOOLW = 1075;
		public const int TTM_SETMAXTIPWIDTH = 1048;
		public const int TTN_NEEDTEXTA = -520;
		public const int TTN_NEEDTEXTW = -530;
		public const int TTS_ALWAYSTIP = 1;
		public const int TTF_IDISHWND = 1;
		public const int TTF_SUBCLASS = 16;
		public const int TVGN_DROPHILITE = 8;
		public const int TVM_SELECTITEM = 4363;
		public const int WSB_PROP_VSTYLE = 256;
		public const int WSB_PROP_HSTYLE = 512;
		public const string TOOLTIPS_CLASS = "tooltips_class32";
		public const int CS_DROPSHADOW = 131072;
		public const int ES_NUMBER = 8192;
		public const int WS_EX_LAYERED = 524288;
		public const int WS_EX_TOOLWINDOW = 128;
		public const int WS_EX_TOPMOST = 8;
		public const int WS_HSCROLL = 1048576;
		public const int WS_OVERLAPPEDWINDOW = 13565952;
		public const int WS_POPUP = -2147483648;
		public const int WS_VISIBLE = 268435456;
		public const int WS_VSCROLL = 2097152;
		public const int TPM_VERTICAL = 64;
		public const int TPM_LAYOUTRTL = 32768;
		public const int ETO_OPAQUE = 2;
		public const int COLOR_BTNFACE = 15;
		public const int COLOR_BTNHIGHLIGHT = 20;
		public const int COLOR_BTNSHADOW = 16;
		public const int COLOR_BTNTEXT = 18;
		public const int COLOR_MENUBAR = 30;
		public const int COLOR_WINDOW = 5;
		public const int DT_TOP = 0;
		public const int DT_LEFT = 0;
		public const int DT_CENTER = 1;
		public const int DT_RIGHT = 2;
		public const int DT_VCENTER = 4;
		public const int DT_BOTTOM = 8;
		public const int DT_WORDBREAK = 16;
		public const int DT_SINGLELINE = 32;
		public const int DT_EXPANDTABS = 64;
		public const int DT_TABSTOP = 128;
		public const int DT_NOCLIP = 256;
		public const int DT_EXTERNALLEADING = 512;
		public const int DT_CALCRECT = 1024;
		public const int DT_NOPREFIX = 2048;
		public const int DT_INTERNAL = 4096;
		public const int TRANSPARENT = 1;
		public const int OPAQUE = 2;
		public const int DC_ACTIVE = 1;
		public const int DC_SMALLCAP = 2;
		public const int DC_TEXT = 8;
		public const int DC_GRADIENT = 32;
		public const int LWA_COLORKEY = 1;
		public const int LWA_ALPHA = 2;
		public const int VK_CAPITAL = 20;
		public const int MAX_PATH = 256;
		public const int LOGPIXELSX = 88;
		public const int LOGPIXELSY = 90;
		public const int DFC_SCROLL = 3;
		public const int DFCS_SCROLLSIZEGRIP = 8;
		public const int TMPF_FIXED_PITCH = 1;
		public const int MF_BYCOMMAND = 0;
		public const int MF_BYPOSITION = 1024;
		public const int MF_OWNERDRAW = 256;
		public const int MF_SYSMENU = 8192;
		public const int MF_SEPARATOR = 2048;
		public const int SC_SIZE = 61440;
		public const int SC_MOVE = 61456;
		public const int SC_MINIMIZE = 61472;
		public const int SC_MAXIMIZE = 61488;
		public const int SC_NEXTWINDOW = 61504;
		public const int SC_PREVWINDOW = 61520;
		public const int SC_CLOSE = 61536;
		public const int SC_RESTORE = 61728;
		public const int SB_HORZ = 0;
		public const int SB_LINEUP = 0;
		public const int SB_LINEDOWN = 1;
		public const int SB_PAGEUP = 2;
		public const int SB_PAGEDOWN = 3;
		public const int SB_THUMBPOSITION = 4;
		public const int SB_THUMBTRACK = 5;
		public const int SB_VERT = 1;
		public const int SIF_RANGE = 1;
		public const int SIF_PAGE = 2;
		public const int DRIVE_FIXED = 3;
		public const int DRIVE_REMOTE = 4;
		private const int SHARED_PATHA = 2;
		private const int SHARED_PATHW = 3;
		public const int WTD_UI_ALL = 1;
		public const int WTD_CHOICE_FILE = 1;
		public const int WTD_REVOKE_WHOLECHAIN = 1;
		public const int BrowseInfos_ReturnOnlyFSDirs = 1;
		public const int BrowseInfos_DontGoBelowDomain = 2;
		public const int BrowseInfos_StatusText = 4;
		public const int BrowseInfos_ReturnFSAncestors = 8;
		public const int BrowseInfos_EditBox = 16;
		public const int BrowseInfos_Validate = 32;
		public const int BrowseInfos_NewDialogStyle = 64;
		public const int BrowseInfos_UseNewUI = 80;
		public const int BrowseInfos_AllowUrls = 128;
		public const int BrowseInfos_BrowseForComputer = 4096;
		public const int BrowseInfos_BrowseForPrinter = 8192;
		public const int BrowseInfos_BrowseForEverything = 16384;
		public const int BrowseInfos_ShowShares = 32768;
		public const int ASM_CACHE_GAC = 2;
		public const int ASM_NAME_PUBLIC_KEY = 0;
		public const int ASM_NAME_CULTURE = 8;
		public const int ASM_NAME_CODEBASE_URL = 13;
		public const int NIM_ADD = 0;
		public const int NIM_MODIFY = 1;
		public const int NIM_DELETE = 2;
		public const int NIM_SETVERSION = 4;
		public const int NIF_MESSAGE = 1;
		public const int NIF_ICON = 2;
		public const int NIF_TIP = 4;
		public const int NIF_INFO = 16;
		public const int NIIF_NONE = 0;
		public const int NIIF_INFO = 1;
		public const int NIIF_WARNING = 2;
		public const int NIIF_ERROR = 3;
		public const int NOTIFYICON_VERSION = 3;
		private const string Gdi32 = "gdi32.dll";
		private const string Kernel32 = "kernel32.dll";
		private const string User32 = "user32.dll";
		private const string ComCtl32 = "comctl32.dll";
		private const string Shell32 = "shell32.dll";
		private const string WinTrust = "wintrust.dll";
		private const string Fusion = "fusion.dll";
		private const string UxTheme = "uxtheme.dll";
		private static readonly bool _isUnicode = Marshal.SystemDefaultCharSize != 1;
		public static readonly int TTM_ADDTOOL = Interop._isUnicode ? 1074 : 1028;
		public static readonly int TTM_DELTOOL = Interop._isUnicode ? 1075 : 1029;
		public static readonly int SHARED_PATH = Interop._isUnicode ? 3 : 2;
		public static Guid WINTRUST_ACTION_GENERIC_VERIFY_V2 = new Guid("00AAC56B-CD44-11d0-8CC2-00C04FC295EE");
		private Interop()
		{
		}
		[DllImport("gdi32.dll", CharSet = CharSet.Auto)]
		public static extern bool ExtTextOut(IntPtr hdc, int x, int y, int nOptions, ref Interop.RECT lpRect, string s, int nStrLength, int[] lpDx);
		[DllImport("gdi32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		public static extern bool ExtTextOutW(IntPtr hdc, int x, int y, int nOptions, ref Interop.RECT lpRect, IntPtr text, int count, int[] lpDx);
		[DllImport("gdi32.dll", CharSet = CharSet.Auto)]
		internal static extern bool GdiFlush();
		[DllImport("gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int GetDeviceCaps(IntPtr hDC, int nIndex);
		[DllImport("gdi32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern bool GetTextExtentPoint32W(IntPtr hdc, string s, int count, ref Interop.SIZE size);
		[DllImport("gdi32.dll", CharSet = CharSet.Auto)]
		public static extern bool GetTextMetrics(IntPtr hDC, Interop.TEXTMETRIC textMetric);
		[DllImport("gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int SelectClipRgn(IntPtr hdc, IntPtr hrgn);
		[DllImport("gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
		[DllImport("gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int SetBkColor(IntPtr hdc, int color);
		[DllImport("gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		internal static extern int SetBkMode(IntPtr hdc, int nBkMode);
		[DllImport("gdi32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int SetPixel(IntPtr hdc, int x, int y, int color);
		[DllImport("gdi32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern int SetTextColor(IntPtr hdc, int color);
		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern int GetDriveType(string lpRootPathName);
		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr GetModuleHandle(string moduleName);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr BeginPaint(IntPtr hWnd, [In] [Out] ref Interop.PAINTSTRUCT lpPaint);
		[DllImport("user32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern bool CreateCaret(IntPtr hWnd, IntPtr bitmap, int width, int height);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr CreateWindowEx(int dwExStyle, string lpszClassName, string lpszWindowName, int style, int x, int y, int width, int height, IntPtr hWndParent, IntPtr hMenu, IntPtr hInst, [MarshalAs(UnmanagedType.AsAny)] object pvParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int DefWindowProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern bool DestroyCaret();
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern bool DestroyCursor(IntPtr hCursor);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool DestroyWindow(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int DispatchMessage(ref Interop.MSG msg);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool DrawCaption(IntPtr hwnd, IntPtr hdc, [In] ref Interop.RECT rect, int nFlags);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		internal static extern bool DrawFrameControl(IntPtr hdc, ref Interop.RECT r, int type, int state);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int DrawText(IntPtr hDC, string lpszString, int nCount, ref Interop.RECT lpRect, int nFormat);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool EndPaint(IntPtr hWnd, ref Interop.PAINTSTRUCT lpPaint);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int FillRect(IntPtr hdc, [In] ref Interop.RECT rect, IntPtr hbrush);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int FrameRect(IntPtr hdc, [In] ref Interop.RECT rect, IntPtr hbrush);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetActiveWindow();
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool GetClientRect(IntPtr hWnd, [In] [Out] ref Interop.RECT rect);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool GetCursorPos([In] [Out] ref Interop.POINT pt);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetDC(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern short GetKeyState(int keyCode);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetLastActivePopup(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern bool GetMessage(ref Interop.MSG msg, IntPtr hwnd, int minFilter, int maxFilter);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int GetMessagePos();
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int GetSysColor(int nIndex);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetSysColorBrush(int nIndex);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetWindow(IntPtr hWnd, int uCmd);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr GetWindowDC(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool GetWindowRect(IntPtr hWnd, [In] [Out] ref Interop.RECT rect);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
		[DllImport("user32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern bool HideCaret(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool IsWindow(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool KillTimer(IntPtr hwnd, int idEvent);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		internal static extern bool ModifyMenu(IntPtr hMenu, int uItem, int flags, int itemData, IntPtr newValue);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern bool PeekMessage([In] [Out] ref Interop.MSG msg, IntPtr hwnd, int msgMin, int msgMax, int remove);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int PostMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern void PostQuitMessage(int nExitCode);
		[DllImport("user32.dll", ExactSpelling = true)]
		public static extern bool PtInRect(ref Interop.RECT lprc, int x, int y);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr RegisterClass(Interop.WNDCLASS wc);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int RegisterWindowMessage(string msg);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool ScrollWindow(IntPtr hWnd, int nXAmount, int nYAmount, ref Interop.RECT rectScrollRegion, ref Interop.RECT rectClip);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, [In] [Out] ref Interop.RECT lParam);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, Interop.TOOLINFO lParam);
		[DllImport("user32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern bool SetCaretPos(int x, int y);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern IntPtr SetCursor(IntPtr hcursor);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetForegroundWindow(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int SetLayeredWindowAttributes(IntPtr hwnd, int color, byte alpha, int flags);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int SetScrollInfo(IntPtr hWnd, int fnBar, Interop.SCROLLINFO si, bool redraw);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, SetLastError = true)]
		public static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int SetTimer(IntPtr hWnd, int nIDEvent, int uElapse, IntPtr lpTimerFunc);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern void SetWindowText(IntPtr hWnd, string lpString);
		[DllImport("user32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern bool ShowCaret(IntPtr hWnd);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int TrackPopupMenuEx(IntPtr hmenu, int fuFlags, int x, int y, IntPtr hwnd, IntPtr tpm);
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool TranslateMessage(ref Interop.MSG msg);
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern bool UpdateWindow(IntPtr hWnd);
		[DllImport("comctl32.dll", ExactSpelling = true)]
		public static extern bool FlatSB_SetScrollProp(IntPtr hWnd, int index, IntPtr newValue, int fRedraw);
		[DllImport("comctl32.dll", ExactSpelling = true)]
		public static extern bool InitCommonControlsEx(Interop.INITCOMMONCONTROLSEX icc);
		[DllImport("shell32.dll", CharSet = CharSet.Auto)]
		public static extern int Shell_NotifyIcon(int message, Interop.NOTIFYICONDATA pnid);
		[DllImport("shell32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SHAddToRecentDocs(int nFlags, string path);
		[DllImport("shell32.dll", ExactSpelling = true)]
		public static extern int SHGetFolderLocation(IntPtr hwnd, int csidl, IntPtr token, int reserved, out IntPtr ppidl);
		[DllImport("shell32.dll", EntryPoint = "SHGetPathFromIDListW", ExactSpelling = true)]
		public static extern bool SHGetPathFromIDList(IntPtr pidl, IntPtr pszPath);
		[DllImport("shell32.dll", EntryPoint = "SHBrowseForFolderW", ExactSpelling = true)]
		public static extern IntPtr SHBrowseForFolder([In] Interop.BROWSEINFO lpbi);
		[DllImport("shell32.dll", ExactSpelling = true)]
		public static extern int SHGetMalloc([MarshalAs(UnmanagedType.LPArray)] [Out] Interop.IMalloc[] ppMalloc);
		[DllImport("wintrust.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
		internal static extern int WinVerifyTrust(IntPtr hWnd, ref Guid pgActionID, ref Interop.WINTRUST_DATA pWinTrustData);
		[DllImport("fusion.dll", ExactSpelling = true)]
		internal static extern int CreateAssemblyCache(out Interop.IAssemblyCache ppAsmCache, uint dwReserved);
		[DllImport("fusion.dll", ExactSpelling = true)]
		internal static extern int CreateAssemblyEnum(out Interop.IAssemblyEnum ppEnum, IntPtr pAppCtx, Interop.IAssemblyName pName, uint dwFlags, int pvReserved);
		[DllImport("uxtheme.dll", CharSet = CharSet.Unicode)]
		public static extern int IsAppThemed();
	}
}
