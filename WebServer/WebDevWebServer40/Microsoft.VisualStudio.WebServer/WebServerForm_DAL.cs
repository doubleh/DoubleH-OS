using Microsoft.VisualStudio.WebHost;
using Microsoft.VisualStudio.WebServer.UIComponents;
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Web.UI;
using System.Windows.Forms;
namespace Microsoft.VisualStudio.WebServer
{
	internal class WebServerForm_DAL : TaskForm
	{
		private const int WM_QUERYENDSESSION = 17;
		private TrayIcon _trayIcon;
		private MxContextMenu _trayMenu;
		private Server _server;
		private bool _exitMode;
		private MxLabel _physicalPathLabel;
		private MxLabel _appRootLabel;
		private MxLabel _portLabel;
		private MxLabel _hyperlinkLabel;
		private MxLabel _aspNetVersionLabel;
		private MxTextBox _physicalPathTextBox;
		private MxTextBox _appRootTextBox;
		private MxTextBox _portTextBox;
		private LinkLabel _hyperlinkLinkLabel;
		private MxButton _stopButton;
		private MxTextBox _aspNetVersionTextBox;
		private TableLayoutPanel tableLayoutPanel1;
		private TableLayoutPanel tableLayoutPanel2;
		private TableLayoutPanel tableLayoutPanel3;
		private Label label1;
		private PictureBox pictureBox1;
		public WebServerForm_DAL(Server server) : base(new ServiceContainer())
		{
			this.InitializeComponent();
			base.TaskCaption = SR.GetString("WEBDEV_Name");
			base.TaskDescription = SR.GetString("WEBDEV_RunAspNetLocally");
			this._hyperlinkLinkLabel.LinkClicked += new LinkLabelLinkClickedEventHandler(this.OnLinkClickedHyperlinkLinkLabel);
			this._stopButton.Click += new EventHandler(this.OnClickStopButton);
			this._trayIcon = new TrayIcon();
			this._trayIcon.Owner = this;
			this._trayIcon.DoubleClick += new EventHandler(this.OnDoubleClickTrayIcon);
			this._trayIcon.ShowContextMenu += new ShowContextMenuEventHandler(this.OnTrayIconShowContextMenu);
			base.Icon = new Icon(base.GetType(), "WebServerForm.ico");
			base.TaskGlyph = new Bitmap(typeof(WebServerForm_DAL), "WebServerForm.bmp");
			this._server = server;
			this._physicalPathTextBox.Text = this._server.PhysicalPath;
			this._appRootTextBox.Text = this._server.VirtualPath;
			this._portTextBox.Text = this._server.Port.ToString(CultureInfo.InvariantCulture);
			this._hyperlinkLinkLabel.Text = this._server.RootUrl;
			try
			{
				Type typeFromHandle = typeof(Page);
				Assembly assembly = Assembly.GetAssembly(typeFromHandle);
				object[] customAttributes = assembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), true);
				if (customAttributes != null && customAttributes.GetLength(0) > 0)
				{
					this._aspNetVersionTextBox.Text = ((AssemblyFileVersionAttribute)customAttributes[0]).Version;
				}
				else
				{
					this._aspNetVersionTextBox.Text = assembly.GetName().Version.ToString();
				}
			}
			catch
			{
			}
			this.Text = SR.GetString("WEBDEV_NameWithPort", new object[]
			{
				this._server.Port
			});
			this._trayIcon.Icon = new Icon(typeof(WebServerForm_DAL), "WebServerForm.ico");
			this._trayIcon.Text = this.Text;
			this._trayIcon.Visible = true;
			this._trayIcon.ShowBalloon(SR.GetString("WEBDEV_Name"), this._server.RootUrl, 15);
		}
		private void DoLaunch()
		{
			string text = this._server.RootUrl;
			if (!text.EndsWith("/", StringComparison.Ordinal))
			{
				text += "/";
			}
			Process.Start(text);
		}
		private void DoShow()
		{
			base.Visible = false;
			base.WindowState = FormWindowState.Minimized;
			base.Show();
			base.Focus();
			base.WindowState = FormWindowState.Normal;
		}
		private void DoStop()
		{
			this._exitMode = true;
			base.Close();
		}
		private void OnClickStopButton(object sender, EventArgs e)
		{
			this.DoStop();
		}
		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			if (!this._exitMode && !this.FormClosedByAppShutDown(e.CloseReason))
			{
				e.Cancel = true;
				base.WindowState = FormWindowState.Minimized;
				base.Hide();
				this._trayIcon.ShowBalloon(SR.GetString("WEBDEV_Name"), this._server.RootUrl, 10);
				return;
			}
			base.OnFormClosing(e);
			if (!e.Cancel)
			{
				this._trayIcon.Dispose();
				this._server.Stop();
			}
		}
		private bool FormClosedByAppShutDown(CloseReason reason)
		{
			return reason == CloseReason.WindowsShutDown || reason == CloseReason.TaskManagerClosing || reason == CloseReason.ApplicationExitCall;
		}
		private void OnCommandLaunch(object sender, EventArgs e)
		{
			this.DoLaunch();
		}
		private void OnCommandShow(object sender, EventArgs e)
		{
			this.DoShow();
		}
		private void OnCommandStop(object sender, EventArgs e)
		{
			this.DoStop();
		}
		private void OnDoubleClickTrayIcon(object sender, EventArgs e)
		{
			this.DoShow();
		}
		private void OnLinkClickedHyperlinkLinkLabel(object sender, LinkLabelLinkClickedEventArgs e)
		{
			this.DoLaunch();
		}
		private void OnTrayIconShowContextMenu(object sender, ShowContextMenuEventArgs e)
		{
			if (this._trayMenu == null)
			{
				MxMenuItem mxMenuItem = new MxMenuItem(SR.GetString("WEBDEV_ShowDetail"), string.Empty, new EventHandler(this.OnCommandShow));
				MxMenuItem mxMenuItem2 = new MxMenuItem(SR.GetString("WEBDEV_Stop"), string.Empty, new EventHandler(this.OnCommandStop));
				MxMenuItem mxMenuItem3 = new MxMenuItem(SR.GetString("WEBDEV_OpenInBrowser"), string.Empty, new EventHandler(this.OnCommandLaunch));
				this._trayMenu = new MxContextMenu(new MenuItem[]
				{
					mxMenuItem3, 
					new MxMenuItem("-"), 
					mxMenuItem2, 
					new MxMenuItem("-"), 
					mxMenuItem
				});
				if (this.RightToLeftLayout)
				{
					this._trayMenu.RightToLeft = RightToLeft.Yes;
				}
			}
			this._trayMenu.Show(this, e.Location.X, e.Location.Y);
		}
		protected override void WndProc(ref Message m)
		{
			if (m.Msg == 17)
			{
				this._exitMode = true;
			}
			base.WndProc(ref m);
		}
		public WebServerForm_DAL()
		{
			this.InitializeComponent();
		}
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(WebServerForm_DAL));
			this._appRootTextBox = new MxTextBox();
			this._portTextBox = new MxTextBox();
			this._portLabel = new MxLabel();
			this._hyperlinkLabel = new MxLabel();
			this._hyperlinkLinkLabel = new LinkLabel();
			this._physicalPathLabel = new MxLabel();
			this._aspNetVersionLabel = new MxLabel();
			this._appRootLabel = new MxLabel();
			this._stopButton = new MxButton();
			this._physicalPathTextBox = new MxTextBox();
			this._aspNetVersionTextBox = new MxTextBox();
			this.tableLayoutPanel1 = new TableLayoutPanel();
			this.tableLayoutPanel2 = new TableLayoutPanel();
			this.tableLayoutPanel3 = new TableLayoutPanel();
			this.label1 = new Label();
			this.pictureBox1 = new PictureBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			((ISupportInitialize)this.pictureBox1).BeginInit();
			base.SuspendLayout();
			componentResourceManager.ApplyResources(this._appRootTextBox, "_appRootTextBox");
			this._appRootTextBox.AlwaysShowFocusCues = true;
			this._appRootTextBox.FlatAppearance = true;
			this._appRootTextBox.Name = "_appRootTextBox";
			this._appRootTextBox.ReadOnly = true;
			this._appRootTextBox.TabStop = false;
			componentResourceManager.ApplyResources(this._portTextBox, "_portTextBox");
			this._portTextBox.AlwaysShowFocusCues = true;
			this._portTextBox.FlatAppearance = true;
			this._portTextBox.Name = "_portTextBox";
			this._portTextBox.ReadOnly = true;
			this._portTextBox.TabStop = false;
			componentResourceManager.ApplyResources(this._portLabel, "_portLabel");
			this._portLabel.FlatStyle = FlatStyle.System;
			this._portLabel.Name = "_portLabel";
			componentResourceManager.ApplyResources(this._hyperlinkLabel, "_hyperlinkLabel");
			this._hyperlinkLabel.FlatStyle = FlatStyle.System;
			this._hyperlinkLabel.Name = "_hyperlinkLabel";
			componentResourceManager.ApplyResources(this._hyperlinkLinkLabel, "_hyperlinkLinkLabel");
			this._hyperlinkLinkLabel.MinimumSize = new Size(2, 8);
			this._hyperlinkLinkLabel.Name = "_hyperlinkLinkLabel";
			componentResourceManager.ApplyResources(this._physicalPathLabel, "_physicalPathLabel");
			this._physicalPathLabel.FlatStyle = FlatStyle.System;
			this._physicalPathLabel.Name = "_physicalPathLabel";
			componentResourceManager.ApplyResources(this._aspNetVersionLabel, "_aspNetVersionLabel");
			this._aspNetVersionLabel.FlatStyle = FlatStyle.System;
			this._aspNetVersionLabel.Name = "_aspNetVersionLabel";
			componentResourceManager.ApplyResources(this._appRootLabel, "_appRootLabel");
			this._appRootLabel.FlatStyle = FlatStyle.System;
			this._appRootLabel.Name = "_appRootLabel";
			componentResourceManager.ApplyResources(this._stopButton, "_stopButton");
			this._stopButton.Name = "_stopButton";
			componentResourceManager.ApplyResources(this._physicalPathTextBox, "_physicalPathTextBox");
			this._physicalPathTextBox.AlwaysShowFocusCues = true;
			this._physicalPathTextBox.FlatAppearance = true;
			this._physicalPathTextBox.Name = "_physicalPathTextBox";
			this._physicalPathTextBox.ReadOnly = true;
			this._physicalPathTextBox.TabStop = false;
			componentResourceManager.ApplyResources(this._aspNetVersionTextBox, "_aspNetVersionTextBox");
			this._aspNetVersionTextBox.AlwaysShowFocusCues = true;
			this._aspNetVersionTextBox.FlatAppearance = true;
			this._aspNetVersionTextBox.Name = "_aspNetVersionTextBox";
			this._aspNetVersionTextBox.ReadOnly = true;
			this._aspNetVersionTextBox.TabStop = false;
			componentResourceManager.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
			this.tableLayoutPanel1.Controls.Add(this._hyperlinkLabel, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this._portLabel, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this._appRootLabel, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this._physicalPathLabel, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this._aspNetVersionLabel, 0, 4);
			this.tableLayoutPanel1.Controls.Add(this._hyperlinkLinkLabel, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this._portTextBox, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this._appRootTextBox, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this._physicalPathTextBox, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this._aspNetVersionTextBox, 1, 4);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			componentResourceManager.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this._stopButton, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel3.BackColor = Color.White;
			componentResourceManager.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
			this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.pictureBox1, 1, 0);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			componentResourceManager.ApplyResources(this.label1, "label1");
			this.label1.BackColor = Color.White;
			this.label1.ForeColor = Color.Black;
			this.label1.Name = "label1";
			componentResourceManager.ApplyResources(this.pictureBox1, "pictureBox1");
			this.pictureBox1.InitialImage = null;
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.TabStop = false;
			componentResourceManager.ApplyResources(this, "$this");
			base.AutoScaleMode = AutoScaleMode.Dpi;
			base.Controls.Add(this.tableLayoutPanel2);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "WebServerForm_DAL";
			base.ShowInTaskbar = false;
			base.TaskBorderStyle = BorderStyle.FixedSingle;
			base.TaskCaption = "Visual Web Developer Web Server";
			base.TaskDescription = "Run ASP.NET applications locally.";
			base.WindowState = FormWindowState.Minimized;
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			((ISupportInitialize)this.pictureBox1).EndInit();
			base.ResumeLayout(false);
		}
	}
}
