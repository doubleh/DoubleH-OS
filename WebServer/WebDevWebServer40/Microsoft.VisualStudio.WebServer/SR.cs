using System;
using System.Globalization;
using System.Resources;
using System.Threading;
namespace Microsoft.VisualStudio.WebServer
{
	internal sealed class SR
	{
		internal const string WEBDEV_DirNotExist = "WEBDEV_DirNotExist";
		internal const string WEBDEV_InvalidPort = "WEBDEV_InvalidPort";
		internal const string WEBDEV_ErrorListeningPort = "WEBDEV_ErrorListeningPort";
		internal const string WEBDEV_Usagestr1 = "WEBDEV_Usagestr1";
		internal const string WEBDEV_Usagestr2 = "WEBDEV_Usagestr2";
		internal const string WEBDEV_Usagestr3 = "WEBDEV_Usagestr3";
		internal const string WEBDEV_Usagestr4 = "WEBDEV_Usagestr4";
		internal const string WEBDEV_Usagestr5 = "WEBDEV_Usagestr5";
		internal const string WEBDEV_Usagestr6 = "WEBDEV_Usagestr6";
		internal const string WEBDEV_Usagestr7 = "WEBDEV_Usagestr7";
		internal const string WEBDEV_Name = "WEBDEV_Name";
		internal const string WEBDEV_NameWithPort = "WEBDEV_NameWithPort";
		internal const string WEBDEV_RunAspNetLocally = "WEBDEV_RunAspNetLocally";
		internal const string WEBDEV_ShowDetail = "WEBDEV_ShowDetail";
		internal const string WEBDEV_Stop = "WEBDEV_Stop";
		internal const string WEBDEV_OpenInBrowser = "WEBDEV_OpenInBrowser";
		internal const string WEBDEV_FailToStart = "WEBDEV_FailToStart";
		private static SR loader;
		private ResourceManager resources;
		private static CultureInfo Culture
		{
			get
			{
				return null;
			}
		}
		public static ResourceManager Resources
		{
			get
			{
				return SR.GetLoader().resources;
			}
		}
		internal SR()
		{
			this.resources = new ResourceManager("WebDevServer", base.GetType().Assembly);
		}
		private static SR GetLoader()
		{
			if (SR.loader == null)
			{
				SR value = new SR();
				Interlocked.CompareExchange<SR>(ref SR.loader, value, null);
			}
			return SR.loader;
		}
		public static string GetString(string name, params object[] args)
		{
			SR sR = SR.GetLoader();
			if (sR == null)
			{
				return null;
			}
			string @string = sR.resources.GetString(name, SR.Culture);
			if (args != null && args.Length > 0)
			{
				for (int i = 0; i < args.Length; i++)
				{
					string text = args[i] as string;
					if (text != null && text.Length > 1024)
					{
						args[i] = text.Substring(0, 1021) + "...";
					}
				}
				return string.Format(CultureInfo.CurrentCulture, @string, args);
			}
			return @string;
		}
		public static string GetString(string name)
		{
			SR sR = SR.GetLoader();
			if (sR == null)
			{
				return null;
			}
			return sR.resources.GetString(name, SR.Culture);
		}
		public static string GetString(string name, out bool usedFallback)
		{
			usedFallback = false;
			return SR.GetString(name);
		}
		public static object GetObject(string name)
		{
			SR sR = SR.GetLoader();
			if (sR == null)
			{
				return null;
			}
			return sR.resources.GetObject(name, SR.Culture);
		}
	}
}
