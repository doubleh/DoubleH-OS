using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using DoubleH.Utility;
using System.Windows.Controls;

namespace DHserver.Plugins
{
    /// <summary>
    /// 插件宿主(doubleH.exe)，包含可以调用的来自 DoubleH 的功能
    /// </summary>
    internal sealed class DefaultPluginHost : IPluginHost
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public void Initialize(DHserver.MainWindow form)
        {
            Debug.Assert(form != null);

            this.window = form;
        }

        private DHserver.MainWindow window = null;
        public DHserver.MainWindow WorkAreaWindow
        {
            get { return window; }
        }

        public PluginManager ExtentionManager { get { return window.PluginManager; } }

    }
}
